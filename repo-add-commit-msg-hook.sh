#!/usr/bin/env bash

loge()
{
    >&2 echo "$@"
}


die_with_code()
{
    # print an error message and exit with the specified errcode
    local exit_code="$1"
    shift
    
    loge "$@"
    exit $exit_code
}


die()
{
    # print error message and exit with errcode 1
    
    loge "$@"
    exit 1
}


git_root()
{
    local root=$(git rev-parse --show-toplevel 2>/dev/null)

    [ -z "$root" ] && return 1
    
    printf "$root"
}


has_trailing_slash()
{
    local str="$1"

    # don't consider "/" by itself a trailing slash

    [  "${str}" != "/" ] && [ "${str%/*}/" = "${str}" ]

    return $?
}


remove_trailing_slash()
{
    local str="$1"

    if has_trailing_slash "$str"; then
        str="${str%/*}"
    fi

    echo "$str"
}




find_ancestor_dir_relative()
{
    # search for a directory location higher up the directory heirarchy
    # and return a relative path to it
    #
    # In order to handle symlinks, each component of the parent path
    # is tested one at a time.
    #
    # $1 is the 'target' directory to find and is required
    #
    # $2 is a starting directory and is optional (current directory is
    #       the default if not specified)

    if [ "$#" -eq 0 ]; then
        die "find_dir_relative(): requires at least one parameter"
    fi

    local target="$1"
    shift

    #
    # if no directory name is passed in use $(pwd)

    local argc="$#"
    local start="$1"

    if [ "$argc" -eq 0 ] ; then
        start="."
    fi

    local RELATIVE_PATH=""
    local REALPATH_TEMP="$(realpath "$start" 2>/dev/null)"

    REALPATH_TEMP=$(remove_trailing_slash "$REALPATH_TEMP")

    while [ -n "$REALPATH_TEMP"  ] && [ "$REALPATH_TEMP" != "/" ] ; do
        last_path_component="${REALPATH_TEMP##*/}"

        if [ "$last_path_component" = "$target" ] ; then
            break
        fi

        RELATIVE_PATH="${RELATIVE_PATH}../"
        REALPATH_TEMP="${REALPATH_TEMP%/*}"

        if [ "$RELATIVE_PATH" = "../../../../../../../../../../" ] ; then exit 1 ; fi
    done

    if [ "$argc" -gt 0 ]; then
        RELATIVE_PATH="$start/$(remove_trailing_slash "$RELATIVE_PATH")"
    fi

    local REALPATH="$(realpath "$RELATIVE_PATH" 2>/dev/null)"

    # is $REALPATH really a "$target" directory?
    if [ "${REALPATH##*/}" != "$target" ] ; then
        return 1
    fi

    if [ "$(stat -c %F "$REALPATH" 2>/dev/null)" != "directory" ] ; then
        return 1
    fi

    # if we get here, $RELATIVE_PATH resolves to a directory named "$target"

    echo "$RELATIVE_PATH"
}


# check if we're even in a git repository
GIT_ROOT=$(git_root)/.git
[ "$?" -ne 0 ] && die "not a git repository"



#
# in the examples in the comments, the repo proecjt used is 
#   ./LINUX/android/kernel/msm-4.9
#
# stat -c %N .git/hooks returns something that looks like (quotes included):
#
#   # (example is from ./LINUX/android/kernel/msm-4.9 project)
#   '.git/hooks' -> '../../../../../.repo/project-objects/qcs605/kernel/msm-4.9.git/hooks'
#
# In an AOSP checkout, the .git/hooks directory entry is always a symlink to the actual
# hooks directory inside repo's actual git repository (most of the file entries in a 
# a workspace's particular repository is really a simlink to the actual git repository
# maintained by the repo script in .repo.project-objects/<PROJECT-NAME>.git)
#
# The awk and tr stuff is to get the 3rd field of the `stat` output and remove the
# single quotes that surround it.
#
#
# PROJECT_HOOKS will be:
#
#   "../../../../../.repo/project-objects/qcs605/kernel/msm-4.9.git/hooks"
# 
hook_stat=$(stat -c %N "$GIT_ROOT/hooks")
PROJECT_HOOKS=$(awk '{print $3}' <<<$hook_stat | tr -d "'")

# get the .repo directory for the AOSP workspace relative to the
# "$GIT_ROOT/hooks" directory.  `cd` to "$GIT_ROOT/hooks"  (in a
# subshell so it automatically gets undone) to get the relative
# path (so there's no "$GIT_ROOT/hooks" prefix on the relative path)

DOT_REPO="$(cd "$GIT_ROOT/hooks"; find_ancestor_dir_relative ".repo")"
DOT_REPO="$(remove_trailing_slash "$DOT_REPO")"
# DOT_REPO will be: "../../../../.."

# directory that the repo utility keeps its custom git hooks in
#
# REPO_HOOKS will be: ".git/hooks/../../../../../repo/hooks"
REPO_HOOKS="$GIT_ROOT/hooks/${DOT_REPO}/repo/hooks"

#
# The directory "$REPO_HOOKS" contains the repo utility's custom git hooks.
# Currently the repo utiliy has two custom hooks: commit-msg and pre-auto-gc
#
# This script will create a link to the repo utility's commit-msg hook (which
# creates a Change-Id in every commit mess if there isn't already one)
#

# if there's already a commit-msg hook in the current repository leave it
[ -f "$GIT_ROOT/hooks/commit-msg" ] && die_with_code 2 "commit-msg hook already exists"

# if there's no custom commit-msg hook, then bail out
[ ! -f "$REPO_HOOKS/commit-msg" ] && die_with_code 3 "repo utilty's commit-msg hook not found"


# create a symlink to the repo utility's commit-msg hook in this project's
# .git/hooks/ directory

ln -s "$DOT_REPO/repo/hooks/commit-msg" "$GIT_ROOT/hooks/commit-msg"
