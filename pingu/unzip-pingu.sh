scriptname=$(basename "$0")
zipfile="$1"


log()
{
    printf "%s\n" "$@"
}


usage()
{
log \
"$scriptname: unzip a pingu flashing_files archive into an appropriately named directory

Usage: $scriptname [-h | --help] FLASHING_FILES_ARCHIVE_NAME

    -h, --help      show this help screen and exit

The specified .zip archive will be unzipped into a directory that has the same
name but stripped of the .zip extension.  Also,the \"signed_flashing_files/\"
(or other top-level directory) in the zip archive will be removed so all of the 
contenst of the archive will be immediately under the destination directory.

NOTE: as of this writing, *all* directories inside the archive will be removed,
leaving only the actual files.  So if you're using a .zip archive that has more
than on directory level, this might not do what you want.

(the unzip utility's -j "junk paths" option is used to remove directories)
"

}


main()
{
    if [ -z "$zipfile" ] || [ "$zipfile" = "-h" ] || [ "$zipfile" = "--help" ] ; then
        usage
        exit 0
    fi

    unzip -jd $(basename -- "$zipfile" .zip) "$zipfile"
}


main "@"
exit


# a more advanced possibliity to look into
# taken from: 
#
# stackoverflow: Equivalent to tar's "--strip-components=1" in unzip?
#
#   https://superuser.com/a/573624/119817
#
# unzip featuring an enhanced version of tar's --strip-components=1
# Usage: unzip-strip ARCHIVE [DESTDIR] [EXTRA_cp_OPTIONS]
# Derive DESTDIR to current dir and archive filename or toplevel dir
unzip-strip() (
    set -eu
    local archive=$1
    local destdir=${2:-}
    shift; shift || :
    local tmpdir=$(mktemp -d)
    trap 'rm -rf -- "$tmpdir"' EXIT
    unzip -qd "$tmpdir" -- "$archive"
    shopt -s dotglob
    local files=("$tmpdir"/*) name i=1
    if (( ${#files[@]} == 1 )) && [[ -d "${files[0]}" ]]; then
        name=$(basename "${files[0]}")
        files=("$tmpdir"/*/*)
    else
        name=$(basename "$archive"); name=${archive%.*}
        files=("$tmpdir"/*)
    fi
    if [[ -z "$destdir" ]]; then
        destdir=./"$name"
    fi
    while [[ -f "$destdir" ]]; do destdir=${destdir}-$((i++)); done
    mkdir -p "$destdir"
    cp -ar "$@" -t "$destdir" -- "${files[@]}"
)


