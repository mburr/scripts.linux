rotate_logs()
{
    # two arguments are passed in:
    #
    #   $1: the name of the log to rotate
    #   $2: the number of backup logs to keep (optional, 7 is default)
    #
    local pathname="$1"
    local max_index=${2:-7}
    
    local dir=$(dirname "${pathname}")
    local name=$(basename "${pathname}")
    
    # no need to rotate anything if the original log file doesn't exist
    if [ ! -f "${pathname}" ] ; then return 1; fi


    # move files in the range 1..(max_index - 1) to the next number
    #
    # for example, if max_index == 4 this will perform the folowing steps:
    #
    #  max_index       rename performed
    #  ---------      -------------------
    #       4               3 -> 4
    #       3               2 -> 3
    #       2               1 -> 2
    #
    #  This loop does not handle the move
    #  of the original file (which has no
    #  numeric extension) to *.1
    #  
    while [ ${max_index} -gt 1 ]; do
        local target_file="$dir/$name.$max_index"
        local prev_file="$dir/$name.$((max_index - 1))"
        
        if [ -f "${prev_file}" ]; then
            mv "${prev_file}" "${target_file}"
        fi
        max_index=$((max_index - 1))
    done

    # move the most recent file to *.1
    mv "$dir/$name" "$dir/$name.1"

    return 0
}

