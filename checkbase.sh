#!/bin/bash

scriptname="${0/#$HOME/\~}"

# utility functions
# use the getopts_long() function:
. ~/devtrees/scripts/getopts_long.shlib


function die()
{
	>&2 echo "$@"
	exit 1
}


function logv()
{
	if [ "$opt_verbose" == true ]; then
		echo "$@"
	fi
}

function logd()
{
	if [ "$opt_debug" == true ]; then
		echo "$@"
	fi
}


function usage()
{
echo "Usage: $scriptname"
cat <<EOF

Compare the merge-base of two branches with the HEAD of 
what is supposed to be the 'base' branch.  If those commits
are different then it means that the base branch has evolved
since it was branched from.

That's OK in general, but for buxfix releases it might not
be desired (sine that might mean that a bugfix that has been 
released might be missing from a subsequent release).

The script expects two branch names to be provided, the 
first branch is considered the base.  If the HEAD of the 
base branch is different from the merge-base between the
two branches then a message is displayed and the script 
returns an error result of 1.

The following command line options are supported:

    -v, --verbose                  verbose messages
        --debug                    debug messages
    -h, --help                     this help message

EOF
}


function parse_options()
{
	opt_verbose=false
	opt_debug=false
	OPTLIND=1
	while getopts_long :vh opt \
	verbose no_argument \
	debug no_argument \
	help 0 "" "$@"
	do
	logd "option: " "$opt"
	case "$opt" in
		v|verbose) opt_verbose=true;;
		debug) opt_debug=true;;
		h|help) usage; exit 0;;
		:) printf >&2 '%s: %s\n' "${0##*/}" "$OPTLERR"
		usage
		exit 1;;
	esac
	done
	shift "$(($OPTLIND - 1))"
	# process the remaining arguments
	
	argv1="$1"
	argv2="$2"
}



function merge_base_match_branch_head()
{
    local base_branch="$1"
    local child_branch="$2"
    
    logd "base_branch=\"${base_branch}\""
    logd "child_branch=\"${child_branch}\""
    
    local merge_base=`git merge-base "$base_branch" "$child_branch"`
    local base_head=`git rev-parse "$base_branch"`
    
    if [ "$merge_base" == "$base_head" ]; then
        logv "    $child_branch" is a strict superset of "$base_branch" '('"${merge_base:0:8}"')'
    else
        echo "!!! $child_branch" is NOT a strict superset of "$base_branch" '('"${merge_base:0:8}"')' vs '('"${base_head:0:8}"')'
        exit 1
    fi
}



function main()
{
	parse_options "$@"
	
	logd "opt_verbose = $opt_verbose"
	
	[ -z "$argv2" ] && die "$scriptname: error - script requires two branches to be specified"

	merge_base_match_branch_head "$argv1" "$argv2"

}


main "$@"
exit 0
