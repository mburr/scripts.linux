#! /bin/bash
#
# cp-to-s3 - copy a file to Amazon S3
#
# currently this script is not much more than an alias.  It will copy a file to Precor's
#   "precor-s3-fileshare" bucket with the provided name (I think Amazon calls the name a "tag")
#   and makes the file publicly readable
#
# The following things are hardcoded at the moment.  Over time I expect to make them configurable:
#
#     - the destination bucket:      precor-s3-fileshare
#     - the AWS credentials:         the [default] credentials in ~/.aws/credentials
#     - the file gets world readable permissions
#

script=$(basename "$0")

help() 
{
    cat <<EOF
${script}: upload a file to Amazon S3 bucket

usage:  ${script} SOURCE DEST

    SOURCE:  file to upload
    DEST:    full S3 path (excluding the bucket name) & filename

example:

    ${script} foobar /some-dir/foobar

EOF
    return 0
}


debug=1

bucket="s3://precor-s3-fileshare"

source="$1"

if [[ -z "$source" || "$source" == @('-h'|'--help') ]] ; then
    help
    exit 0
fi

# remove leading slash (if any) from the destination
#
# The destination is always assumed to be from the root of the bucket
#
dest="${2#/}"

if [ debug != 0 ] ; then
    echo "source:  \"$source\""
    echo "dest:    \"$dest\""
fi

aws s3 cp             \
     "$source"        \
     "$bucket/$dest"  \
     --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers

echo -e "\nURL: https://precor-s3-fileshare.s3.amazonaws.com/$dest"

