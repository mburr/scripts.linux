#!/bin/bash
set -euo pipefail
# IFS=$'\n\t'


readonly SCRIPT_NAME=$(basename "$0")

readonly IMAGE_DEFAULT="192.168.250.41:5001/p80:trunk"
readonly CONTAINER_NAME_DEFAULT="p80-legacy-trunk-8022"

# DEVROOT is where the P80's tools and SDKs will be placed on the host system
# TODO: make this an option
readonly DEVROOT_DEFAULT="${HOME}/devroot/p80"

usage()
{
    echo "$SCRIPT_NAME: run a dockerized P80 build environment"
    echo ""
    echo "USAGE: $SCRIPT_NAME  CONTAINERNAME IMAGE"
    echo ""
    echo "    CONTAINERNAME: - docker container name to use. Defaults to $CONTAINER_NAME_DEFAULT"
    echo ""
    echo "                     NOTE: The container name **must** end with a dash followed by a port"
    echo "                     number, for example '-8022' - that will be the port you can use to ssh"
    echo "                     into the running container (ie., the container's port 22 will be mapped"
    echo "                     to the hosts port 8022)"
    echo ""
    echo "    IMAGE:         - docker image. Defaults to $IMAGE_DEFAULT"
    echo ""
    echo "    example: $SCRIPT_NAME p80-legacy-trunk-8022 192.168.250.41:5001/p80:trunk"
    exit 1
}


setup_devroot()
{
    # create the directories that the docker contain will map to for various
    # tools and SDKs

    local l_devroot
    l_devroot="$1"
    
    if [ "$l_devroot" = "" ] ; then
        warning "no DEVROOT specified"
        return 1
    fi
    
    mkdir -p ${l_devroot}/dvsdk
    mkdir -p ${l_devroot}/ext

    # I don't know if the following docker mappings are necessary, so no devroot dirs for them:
    #
    # -v /mnt/fileshare/builds:/mnt/fileshare/builds \
    # -v /mnt/fileshare/firmware:/mnt/fileshare/firmware \
    # -v /mnt/fileshare/packages/firmware:/mnt/fileshare/packages/firmware/ \
    # -v /mnt/fileshare/packages/repos:/mnt/fileshare/packages/repos \
    # -v /mnt/precorbeaker/firmware:/mnt/precorbeaker/firmware \
    # -v /mnt/precorbeaker/builds:/mnt/precorbeaker/builds \
    # -v /mnt/precorbeaker/packages/firmware:/mnt/precorbeaker/packages/firmware/ \
    # -v /mnt/precorbeaker/packages/repos:/mnt/precorbeaker/packages/repos \
    # -v /opt:/opt:ro
    # -v /opt/resources:/opt/resources

}





# call usage if there's a --help on the command line
expr "$*" : ".*--help" > /dev/null && usage


readonly LOG_FILE="/tmp/$(basename "$0").log"
info()    { echo "[INFO]    $*" | tee -a "$LOG_FILE" >&2 ; }
warning() { echo "[WARNING] $*" | tee -a "$LOG_FILE" >&2 ; }
error()   { echo "[ERROR]   $*" | tee -a "$LOG_FILE" >&2 ; }
fatal()   { echo "[FATAL]   $*" | tee -a "$LOG_FILE" >&2 ; exit 1 ; }




readonly ctr="${1:-$CONTAINER_NAME_DEFAULT}"
readonly img="${2:-$IMAGE_DEFAULT}"
readonly prt=${ctr##*-}

readonly devroot="${DEVROOT-$DEVROOT_DEFAULT}"


if [ -z $prt ] ; then
  echo "failed to get port: $prt"
  usage
fi

# ensure that $prt is an int3eger with nomore than 5 digits
if !  expr "${prt}" : "[0-9]\{1,5\}$" >/dev/null ; then
    fatal "port number ($prt) must be an integer less than 99999"
fi

info "container name:  ${ctr}"
info "image:           ${img}"
info "mapped ssh port: ${prt}"
info "devroot:         ${devroot}"


setup_devroot "$devroot"




# removed the following docker mounts from Pete's version.  Hopefully don't need them...
#
#   -v /mnt/fileshare/builds:/mnt/fileshare/builds \
#   -v /mnt/fileshare/firmware:/mnt/fileshare/firmware \
#   -v /mnt/fileshare/packages/firmware:/mnt/fileshare/packages/firmware/ \
#   -v /mnt/fileshare/packages/repos:/mnt/fileshare/packages/repos \
#   -v /mnt/precorbeaker/firmware:/mnt/precorbeaker/firmware \
#   -v /mnt/precorbeaker/builds:/mnt/precorbeaker/builds \
#   -v /mnt/precorbeaker/packages/firmware:/mnt/precorbeaker/packages/firmware/ \
#   -v /mnt/precorbeaker/packages/repos:/mnt/precorbeaker/packages/repos \


docker run -d -p $prt:22 \
-v "${devroot}/dvsdk:/home/precor/dvsdk" \
-v "${devroot}/ext:/home/precor/ext" \
-v "/opt:/opt:ro" \
-v "/opt/resources:/opt/resources" \
--dns=192.168.248.51 \
--dns-search=xdc.precor.int \
--name "$ctr" \
"$img" /usr/sbin/sshd -D
