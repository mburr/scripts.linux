# /bin/bash

#
# Enables or disables ctap by commenting/uncommenting CPA_PROXY_ADDRESS=localhost
#   as appropriate in mpaqtap's envionment.txt
#
# Will also query the current state of the ctap configuration if named `ctap-query.sh`
#
#
# The decision on what to do is based on the script's name
#
# The script will detect if it's running on the console or not, and
# will use ssh to connect to the console if necessary.  Use the
# the environment variable `dest` to specify the address to use
# with ssh if that is the case.
#

# debug can contain one or more of the following tokens to enable
# certain debugging operations:
#
#   log,  keeptmp

# set up some defaults that can be overridden by setting environment variables
opt_debug=${opt_debug:-""}

script=$(basename "$0")

# IP address or DNS name of console to disable ctap on
dest=${dest:-"burr-p62"}



function die()
{
    >&2 echo "$@"
    exit 1
}


function logv()
{
    if [ "$opt_verbose" == true ]; then
            echo "$@"
    fi
}

function logd()
{
    case "$opt_debug" in
        *log*) echo "$@"
                ;;
    esac
}


function logd_var()
{
    local var_name="$1"
    local value
    eval value=\"\$$var_name\"

    logd "$var_name:" "${value}"
}



logd_var script
logd_var dest
logd ""

usage()
{
cat <<EOF
${script}:  Enable/Disable ctap on a console

This script comments/uncomments CPA_PROXY_ADDRESS=localhost
in mpaqtapp's environment.txt as approprite depending on the name
of the script

Use the following environment variables to change the defaults:

  dest:     the IP address or DNS name of the console to install
            the ctap file to.

            Ignored if the script is run on-console

  debug:    set this variable to "log" to get debug information
EOF
}


do_ssh()
{
    local cmd="$1"

    logd ssh root@"$dest" "$cmd"
    2>/dev/null ssh -o StrictHostKeyChecking=no root@"$dest" "$cmd"
}



query_ctap()
{
    # print a message indicating ctap is enabled or not
    # also, return 0 if ctap is enabled, 1 otherwise
    local environment_txt="/data/data/com.preva.mpaqtapp/files/support_files/resources/environment.txt"
    local result

    if [ -f "$environment_txt" ]; then
        grep -q '^CPA_PROXY_ADDRESS=localhost$' ${environment_txt}
        result=$?
    else
        do_ssh "grep -q '^CPA_PROXY_ADDRESS=localhost$' ${environment_txt}"
        result=$?
    fi

    if [ "$result" == 0 ]; then
        echo "ctap is enabled"
    else
        echo "ctap is disabled"
    fi

    return $result
}



disable_ctap()
{
    # comment out CPA_PROXY_ADDRESS=localhost (if neccessary)
    local environment_txt="/data/data/com.preva.mpaqtapp/files/support_files/resources/environment.txt"

    if [ -f "$environment_txt" ]; then
        logd "Disabling ctap"
        busybox sed -i 's/^CPA_PROXY_ADDRESS=localhost$/# CPA_PROXY_ADDRESS=localhost/g' ${environment_txt}
    else
        logd "Disabling ctap via ssh"
        do_ssh "busybox sed -i 's/^CPA_PROXY_ADDRESS=localhost$/# CPA_PROXY_ADDRESS=localhost/g' ${environment_txt}"
    fi
}


enable_ctap()
{
    # uncomment CPA_PROXY_ADDRESS=localhost (if neccesary)
    local environment_txt="/data/data/com.preva.mpaqtapp/files/support_files/resources/environment.txt"

    if [ -f "$environment_txt" ]; then
        logd "Enabling ctap"
        busybox sed -i 's/^# CPA_PROXY_ADDRESS=localhost$/CPA_PROXY_ADDRESS=localhost/g' ${environment_txt}
    else
        logd "Enabling ctap via ssh"
        do_ssh "busybox sed -i 's/^# CPA_PROXY_ADDRESS=localhost$/CPA_PROXY_ADDRESS=localhost/g' ${environment_txt}"
    fi

}

if [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
    usage
    exit 0
fi


case "$script" in

    *disable*)  disable_ctap
                ;;

    *enable*)   enable_ctap
                ;;

    *query*)    query_ctap
                ;;

    *)  >&2 echo "Error: must select enable, disable, or query"
        exit 1
        ;;
esac

exit 0

