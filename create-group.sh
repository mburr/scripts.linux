# create a group with th gid in the range 4000-4999 to keep it
# outside the ranges normally used the the system (0-999) or
# normal users (1000-3999)

groupname="$1"


loge()
{
    1>&2 echo "$@"
}


if [ "$groupname" = "" ] ; then
    loge "error: must provide a user name"
    exit 1
fi

if  getent group "$groupname" >/dev/null 2>&1 ; then
    loge "error: $groupname already exists"
    exit 1
fi

# find the next GID in the range 4000-4999
#   taken from: https://www.kevinguay.com/posts/next-uid-gid/

gid=$(cat /etc/group | cut -d ":" -f 3 | grep "^4...$" | sort -n | tail -n 1 | awk '{ print $1+1 }')
if [ "$gid" = "" ] ; then
    gid=4000    # no groups in range 4000-4999 yet!
fi

# if the group already exists, just leave it unchanged (the `-f` option)
sudo groupadd -f --gid "$gid" "$groupname"
