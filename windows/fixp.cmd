@echo off
for /f "usebackq tokens=*" %%a in (`"%~d0%~p0pwd.exe"`) do cd "%%a"
goto :eof

/*
 *  pwd.c - simply gets the current directory, converts it to the LFN variant of the pathname
 *	   This can be used to fix WinNT's annoying behavior of making the current
 *              direcory displayed at the prompt the short filename version after
 *              running a 16-bit program (like debug.exe).
 *
 *              With pwd.exe and a not-so-simple cmd one-liner, the propmpt can be fixed:
 *
 *          @for /f "usebackq tokens=*" %%a in (`"%~d0%~p0pwd.exe"`) do @cd "%%a"
 *
 *              The "%~d0%~p0pwd.exe" string is simply the pwd.exe command with the drive and path
 *              of the cmd script pre-pened to it (do the script assumes that pwd.exe is in the
 *              same directory as the script).
 *
 *              Basically, what the script does is invoke pwd.exe to get the current directory, then:
 *
 *          cd "c:\Current\long name\of the directory"
 *
 *              
 */
