#!python3

# weird hack to force a "readable" syntax error when this is run under
#   python2 instead of python3
#
# see https://stackoverflow.com/a/65407535/12711
#
# This takes advantage of a particular syntax that is new to Python 3,
#   "PEP 3132 -- Extended Iterable Unpacking"
#   https://www.python.org/dev/peps/pep-3132/
#
Python_2_is_running, *Python_3_is_needed="python 3 is needed" # this script needs to be run with python 3

# use jlink.exe (JLink Commander) to flash STM32/GD32 chips
#
# This script will write a "JLink Commander Command" file we can pass to jlnk.exe
# to flash an STM32/GD32 chip.  The advantages of using jlink.exe instead of 
# jflash.exe are:
#
#   - you don't need a binary configuration file (jflash does)
#   - you don't get an ugly artifact of a GUI program starting, doing a bunch of
#       crap then disappearing (only cosmetic, but still annoying)
#   - jlink.exe provides information on the console that can be copy/pasted for
#       troubleshooting if there's a problem
#   - the JLink JTAG unti doesn't need a JFLASH license - this should work with 
#       JLink Base units as well as JLink Plus. It may also work with JLink Lite.
#
# The only drawback that I'm aware of is that to fo the flashing with jlink.exe
# you have to use "JLink Command Command" (.jlinkcmd) files which cannot be
# parameterized, so to flash using different hex/binary files you have to write
# a custom .jlinkcmd file for each one.
#
# This script is designed to handle writing a custom .jlinkcmd file so that arbitrary
# .hex/.bin firmware files can be specified on the command line
#
# Notes:
# 
# Segger has several kinds of files that can interact/drive jlink.exe and is can
# confusing which kind can do what. Here's a little glossary (the extensins I give
# here are not necessarily what Segger uses - I believe Segger uses an extension of
# ".jlink" for any of them)
# 
#   - JLink Commander Command file  (.jlinkcmd)
#   
#         A file that specifies a list of commands just as you wuld type them at
#         the jlink.exe command prompt (when it is run in interactive mode)
# 
#         A JLink Commander Command file is specified using the -CommandFile option
# 
#         These files do not support any control flow or parameterization, they
#         are just a list of commands
# 
#         It is undocumented, but you can use C++ style comments ("//") in
#         ".jlinkcmd" files 
# 
#         No parameterization: https://forum.segger.com/index.php/Thread/4027-SOLVED-Arguments-from-a-bat-file-to-a-jlink-file-with-CommanderScript/
#         
# 
#         
#   - JLink Commander Script file  (.jlinkscript)
# 
#         A file that uses a C-like language what can call APIs and have callbacks
#         to provide very low level access and control of the JLink device.
# 
#         A JLink script is often used to iniialize a microscontroller to prepare
#         it for debugging.
# 
#         A JLink Commander Script file is specified using the -CommanderScript option
# 
#         You can use C++ style comments ("//") in ".JLinkScript" files
# 
#         https://wiki.segger.com/J-Link_script_files
# 
#   - JLink Command Strings 
# 
#         Commands that are sent to the JLinkARM.dll to configure it.  From the Segger Wiki:
# 
#         The behavior of the J-Link can be customized via J-Link Command Strings passed
#         to the JLinkARM.dll which controls J-Link. Applications such as J-Link Commander,
#         but also other IDEs, allow passing one or more J-Link Command Strings. Command
#         strings can be used for passing commands to J-Link (such as switching on target
#         power supply), as well as customize the behavior (by defining memory regions and
#         other things) of J-Link. This way, J-Link Command Strings enable to set options
#         which are not configurable via dialogs or settings (e.g. in an IDE). 
# 
#         The recommend way to execute J-Link command strings is to add them to a J-Link
#         script file (using the JLINK_ExecCommand() API?)
# 
#         JLink Command Strings can also be sent va JLink Commander (or .jlinkcmd files)
#         using the `exec` command.
# 
#         https://wiki.segger.com/J-Link_Command_Strings
#         https://wiki.segger.com/J-Link_Commander#Using_J-Link_Command_Strings
#
#

import sys
# import os
# import re

import shlex
import subprocess
import argparse
import logging
import tempfile
import time
import os


# global variables
verbose = False

exe_names = {
    'stlink_exe': 'ST-LINK_CLI.exe',
    'stcube_exe': 'STM32_Programmer_CLI.exe',
    'jlink_exe':  'JLink.exe'
}



# a time formatting function from https://stackoverflow.com/a/63198084/12711
from string import Formatter
from datetime import timedelta

def strfdelta(tdelta, fmt='{D:02}d {H:02}h {M:02}m {S:02.0f}s', inputtype='timedelta'):
    """Convert a datetime.timedelta object or a regular number to a custom-
    formatted string, just like the stftime() method does for datetime.datetime
    objects.

    The fmt argument allows custom formatting to be specified.  Fields can 
    include seconds, minutes, hours, days, and weeks.  Each field is optional.

    Some examples:
        '{D:02}d {H:02}h {M:02}m {S:02.0f}s' --> '05d 08h 04m 02s' (default)
        '{W}w {D}d {H}:{M:02}:{S:02.0f}'     --> '4w 5d 8:04:02'
        '{D:2}d {H:2}:{M:02}:{S:02.0f}'      --> ' 5d  8:04:02'
        '{H}h {S:.0f}s'                       --> '72h 800s'

    The inputtype argument allows tdelta to be a regular number instead of the  
    default, which is a datetime.timedelta object.  Valid inputtype strings: 
        's', 'seconds', 
        'm', 'minutes', 
        'h', 'hours', 
        'd', 'days', 
        'w', 'weeks'
    """

    # Convert tdelta to integer seconds.
    if inputtype == 'timedelta':
        remainder = tdelta.total_seconds()
    elif inputtype in ['s', 'seconds']:
        remainder = float(tdelta)
    elif inputtype in ['m', 'minutes']:
        remainder = float(tdelta)*60
    elif inputtype in ['h', 'hours']:
        remainder = float(tdelta)*3600
    elif inputtype in ['d', 'days']:
        remainder = float(tdelta)*86400
    elif inputtype in ['w', 'weeks']:
        remainder = float(tdelta)*604800

    f = Formatter()
    desired_fields = [field_tuple[1] for field_tuple in f.parse(fmt)]
    possible_fields = ('Y','m','W', 'D', 'H', 'M', 'S', 'mS', 'uS')
    constants = {'Y':86400*365.24,'m': 86400*30.44 ,'W': 604800, 'D': 86400, 'H': 3600, 'M': 60, 'S': 1, 'mS': 1/pow(10,3) , 'uS':1/pow(10,6)}
    values = {}
    for field in possible_fields:
        if field in desired_fields and field in constants:
            Quotient, remainder = divmod(remainder, constants[field])
            values[field] = int(Quotient) if field != 'S' else Quotient + remainder
    return f.format(fmt, **values)




def logged_call_build_msg(cmd, **kwargs):
    # put cmd into the msg, joining if it's a collection of arguments
    msg = "running: \n========\n{cmd}\n"
    if type(cmd) is str:
        msg = msg.format(cmd=cmd)
    else:
        msg = msg.format(cmd = " ".join(cmd))

    # list ant keyword arguments, one per line
    if kwargs:
        # list the keyword arguments passed in
        msg += "\nkeyword args:\n"
        for arg in kwargs:
            msg += "    {arg} = {val}\n".format(arg=arg, val=kwargs[arg])
    msg += "========"
    return msg


def logged_call(cmd, **kwargs):
    # special case when called with a string - split it into a
    #   collection of args - but **only** if `shell==False`
    logging.debug( f"cmd: {cmd}")
    logging.debug( f"kwargs: {kwargs}");

    if type(cmd) is str and not kwargs.get("shell"):
        cmd = shlex.split(cmd)
        logging.debug( f"split cmd: {cmd}")

    logging.debug( logged_call_build_msg(cmd, **kwargs))

    return subprocess.call(cmd, **kwargs)


def logged_check_output(cmd, **kwargs):
    # special case when called with a string - split it into a
    #   collection of args - but **only** if `shell==False`

    if type(cmd) is str and not kwargs.get("shell"):
        cmd = shlex.split(cmd)

    logging.debug( logged_call_build_msg(cmd, **kwargs))

    return subprocess.check_output(cmd, **kwargs)




def printv(*args, **kwargs):
    # pass all arguments on to the built-in print() if we're in verbose mode
    global verbose
    
    if not verbose:
        return
        
    return print( *args, **kwargs)



def parse_command_line(argv):
    argument_parser = argparse.ArgumentParser(
                        description='XDC JLink flasher',
                        formatter_class=argparse.RawTextHelpFormatter,
                        allow_abbrev=False)
    
    argument_parser.add_argument( 'firmware_files', metavar='FILENAME', type=str, 
                                    nargs='*', action='append', default=argparse.SUPPRESS,
                                    help="a firmware hex file to flash to the STM32/GD32 device")

    argument_parser.add_argument('--device', default='GD32F303ZK',
                                 help = "specify the microcontroller that will be erased/flashed \n"
                                        "    default is GD32F303ZK \n"
                                        "    STM32 is an alias for STM32F103ZE \n"
                                        "    GD32  is an alias for GD32F303ZK")

    argument_parser.add_argument('--stlink', action='store_true',
                                 help="use ST-LINK jtag instead of JLink")

    argument_parser.add_argument('--jlink-prog', default='',
                                 help = "full path/filename to JLink.exe")

    argument_parser.add_argument('--dbgif', choices=[ 'swd', 'jtag' ], default='swd',
                                 help="debug interface to use: swd (default) or jtag")

    argument_parser.add_argument('--erase', action='store_true',
                                 help="erase the STM32/GD32 chip entirely before any flashing")

    argument_parser.add_argument('--verbose', action='store_true',
                                 help="enable verbose output")

    argument_parser.add_argument('--no-reset', action='store_false', dest='reset',
                                 help="don't reset after flashing")

    argument_parser.add_argument('--keep-temp', action='store_false', dest='delete_temp',
                                 help="keep temporary files")

    argument_parser.add_argument('--debug', action='store_true',
                                 help="enable debug logging")

    arguments = argument_parser.parse_args(argv)


    if arguments.debug:
        # print verbose output too if we're doing DEBUG
        arguments.verbose = True
        logging.getLogger().setLevel(logging.DEBUG)
        
    if arguments.verbose:
        global verbose
        verbose = True
        
    if   arguments.device.casefold() == "STM32".casefold():
        arguments.device = "STM32F103ZE"
    elif arguments.device.casefold() == "GD32".casefold():
        arguments.device = "GD32F303ZK"

    import pprint
    logging.debug("List of command line arguments:\n")
    logging.debug(pprint.PrettyPrinter().pformat(vars(arguments)))

    if not arguments.erase and not hasattr( arguments, "firmware_files"):
        # user hasn't asked for anythig to be done - print help
        argument_parser.print_help()
        sys.exit(0)
        
    return arguments



def use_stcube(arguments):
    logging.debug("use_stcube() entered")
    # newer ST-LINK flashing utility: STM32_Programmer_CLI
    #
    #   installed by default to: 
    #       "C:\Program Files\STMicroelectronics\STM32Cube\STM32CubeProgrammer\bin\"
    #
    #   examples:
    #       STM32_Programmer_CLI --connect port=SWD --erase all
    #       STM32_Programmer_CLI --connect port=SWD --download p82.precor.test.3183_4085665.partition-a.cpa.00039804.hex --verify
    #       STM32_Programmer_CLI --connect port=SWD --rst
    
    global exe_names
    
    stcube_exe = exe_names['stcube_exe']
    
    # check that STM32_Programmer_CLI flasher is installed
    import shutil
    if not shutil.which( stcube_exe):
        logging.debug( f"{stcube_exe} utility not found, bailing out")
        print( f"ERROR: {stcube_exe} utility not found")
        return 1

    if (arguments.erase):
        printv( "asking ST-LINK to erase the chip...")
        logged_call( f"{stcube_exe} --connect PORT={arguments.dbgif} --erase all")
        
    try:
        hex_files = arguments.firmware_files[0]
        for fname in hex_files:
            printv( f"asking ST-LINK to flash '{fname}'...")
            logged_call( f"{stcube_exe} --connect PORT={arguments.dbgif} --download \"{fname}\" --verify")
        
    except AttributeError:
        pass

    if arguments.reset:
        printv( "asking ST-LINK to reset the chip...")
        logged_call( f"{stcube_exe} --connect PORT={arguments.dbgif} -rst")
    else:
        logging.debug( "not asking ST-LINK to reset due to command line option")

    return 0




def use_stlink( arguments):
    logging.debug("use_stlink() entered")
    global exe_names
    
    stcube_exe = exe_names['stcube_exe']
    stlink_exe = exe_names['stlink_exe']
    
    # find out if the newer STM32CubeProgrammer flasher is installed
    import shutil
    stcube_fullpath = shutil.which( stcube_exe)
    if stcube_fullpath:
        logging.debug( f"full path to {stcube_exe}: '{stcube_fullpath}'")
        logging.debug( f"found {stcube_exe} utility, calling use_stcube()")
        printv( f"{stcube_exe} utility found, using it...")
        return use_stcube( arguments)


    # if stcube program is found, we'll never get to this point...

    printv( f"{stcube_exe} utility not found, trying {stlink_exe}...")

    # check that ST-LINK_CLI.exe flasher is installed
    stlink_fullpath = shutil.which( stlink_exe)
    logging.debug( f"full path to {stlink_exe}: '{stlink_fullpath}'")
    if not stlink_fullpath:
        logging.debug( f"{stlink_exe} utility not found, bailing out")
        print( f"ERROR: {stlink_exe} utility not found")
        return 1

    if (arguments.erase):
        printv( "asking ST-LINK to erase the chip...")
        logged_call( f"{stlink_exe} -c {arguments.dbgif} -ME")
        
    try:
        hex_files = arguments.firmware_files[0]
        for fname in hex_files:
            printv( f"asking ST-LINK to flash '{fname}'...")

            # if there's a comma separating the filename from the address, we're
            # being asked to flash a binary image instead of a hex file
            #
            # ST-LINK_CLI.exe needs a space instead of a comma

            fname, *addr = fname.split(",", 1)  # split first token to fname, remainder to a list named addr
            addr = ''.join(addr)                # convert the addr list to a string (the empty
                                                #   string if there's nothing in the list
            logged_call( f"{stlink_exe} -c {arguments.dbgif} -P \"{fname}\" {addr} -V \"after_programming\"")
        
    except AttributeError:
        pass

    if arguments.reset:
        printv( "asking ST-LINK to reset the chip...")
        logged_call( f"{stlink_exe} -c {arguments.dbgif} -Rst")
    else:
        logging.debug( "not asking ST-LINK to reset due to command line option")

    return 0



##############################
#
# Notes on commands for JLink Commander Command Files:
#
#   erase [Saddr, EAddr]
#       flash between SAddr and EAddr will be erased (it's not clear to
#       me if EAddr is erased or just past last erased address)
#
#       If addresses are not specified, all flash will be erased
#
#       Hex addresses start with 0x
#
#   LoadFile <filename>, [Addr]
#
#       load a .hex or .bin file into flash.
#
#       Addr must be specified for .bin files (and only .bin files)
#
#       The file type is determined by extension (I believe)
#
#   SaveBin <filename>, <Addr>, <numbytes>
#
#   VerifyBin <filename>, <addr>
#
#       compare specified file to location in flash
#
##############################
def use_jlink(arguments):
    logging.debug("use_jlink() entered")

    global exe_names
    
    jlink_exe = exe_names['jlink_exe']
    
    if len(arguments.jlink_prog) != 0:
        # user has specified a jlink.exe executable -
        #   use it instead of whatever is in the PATH env var
        jlink_exe = arguments.jlink_prog
        logging.debug( f"using user-specified jlink utility: '{jlink_exe}'")
        printv( f"using user-specified jlink utility: '{jlink_exe}'")
    else:
        # check that JLink.exe flasher is installed
        import shutil
        jlink_fullpath = shutil.which( jlink_exe)
        logging.debug( f"full path to {jlink_exe}: '{jlink_fullpath}'")
        if not jlink_fullpath:
            logging.debug( f"{jlink_exe} utility not found, bailing out")
            print( f"ERROR: {jlink_exe} utility not found")
            return 1
        #
        # jlink.exe is sometimes a Java utility.. check for that so we
        # don't get mysterious failures
        #
        if ("jdk" in jlink_fullpath.lower()) or ("java" in jlink_fullpath.lower()):
            logging.error( f"{jlink_exe} appears to be a Java utility ('{jlink_fullpath}')")
            logging.error( f"exiting...")
            sys.exit(1)

    jlinkcmd = tempfile.NamedTemporaryFile( 
                            mode='w',
                            prefix="flash-gd32-cmds.",
                            suffix=".jlinkcmd",
                            delete=False)

    try:
        logging.debug("created JLink Command file: " + jlinkcmd.name)
        
        jlinkcmd.write( "ExitOnError 1\n")
        jlinkcmd.write( "Connect\n")
        jlinkcmd.write( "h\n")
        jlinkcmd.write( "\n")
        
        if (arguments.erase):
            printv( "asking jlink to erase the chip...")
            jlinkcmd.write( "erase\n")
            jlinkcmd.write( "\n")
            
        try:
            hex_files = arguments.firmware_files[0]
            for fname in hex_files:
                printv( f"asking jlink to flash '{fname}'...")
                jlinkcmd.write( f"LoadFile {fname}\n")
            jlinkcmd.write( "\n")
            
        except AttributeError:
            pass
            
        if arguments.reset:
            jlinkcmd.write( "r\n")
        else:
            logging.debug( "not asking jlink to reset due to command line option")

        jlinkcmd.write( "exit\n")

        jlinkcmd.close()
        printv( "")
        printv( "running jlink to do the work...")
        printv( "==========================================")
        logged_call( f"\"{jlink_exe}\"  -device {arguments.device} -if {arguments.dbgif} -speed 4000 -JTAGConf -1,-1 -CommandFile '{jlinkcmd.name}'")
        printv( "==========================================")
        printv( "jlink processing complete...")

        
        if not arguments.reset:
            print( "\nnote: no reset due to command line option")
        else:
            print( "resetting...")

    finally:
        jlinkcmd.close()
        if arguments.delete_temp:
            logging.debug( f"deleting temporary file: '{jlinkcmd.name}'")
            os.remove( jlinkcmd.name)
        else:
            logging.debug( f"!!keeping temporary file: '{jlinkcmd.name}'")
            
    return 0


def main(arguments):
    logging.debug("main() entered")
    
    start = time.time()
    
    if arguments.stlink:
        use_stlink(arguments)
    else:
        use_jlink( arguments)

    end = time.time()
    
    duration = end - start
    print( "Done in %s" % strfdelta( duration, '{M} min {S:02.3f} sec', inputtype='s'))
    return 0



if __name__ == '__main__':
    if sys.version_info < (3,8):
        print("Python version: " + sys.version)
        print("This script requires Python 3.8 or later")
        sys.exit(1)
    sys.exit(main(parse_command_line(sys.argv[1:])))
    

