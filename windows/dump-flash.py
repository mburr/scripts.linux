#!python3

# weird hack to force a "readable" syntax error when this is run under
#   python2 instead of python3
#
# see https://stackoverflow.com/a/65407535/12711
#
# This takes advantage of a particular syntax that is new to Python 3,
#   "PEP 3132 -- Extended Iterable Unpacking"
#   https://www.python.org/dev/peps/pep-3132/
#
Python_2_is_running, *Python_3_is_needed="python 3 is needed" # this script needs to be run with python 3

# use jlink.exe (JLink Commander) to flash STM32/GD32 chips
#
# This script will write a "JLink Commander Command" file we can pass to jlnk.exe
# to flash an STM32/GD32 chip.  The advantages of using jlink.exe instead of 
# jflash.exe are:
#
#   - you don't need a binary configuration file (jflash does)
#   - you don't get an ugly artifact of a GUI program starting, doing a bunch of
#       crap then disappearing (only cosmetic, but still annoying)
#   - jlink.exe provides information on the console that can be copy/pasted for
#       troubleshooting if there's a problem
#   - the JLink JTAG unti doesn't need a JFLASH license - this should work with 
#       JLink Base units as well as JLink Plus. It may also work with JLink Lite.
#
# The only drawback that I'm aware of is that to fo the flashing with jlink.exe
# you have to use "JLink Command Command" (.jlinkcmd) files which cannot be
# parameterized, so to flash using different hex/binary files you have to write
# a custom .jlinkcmd file for each one.
#
# This script is designed to handle writing a custom .jlinkcmd file so that arbitrary
# .hex/.bin firmware files can be specified on the command line
#
# Notes:
# 
# Segger has several kinds of files that can interact/drive jlink.exe and is can
# confusing which kind can do what. Here's a little glossary (the extensins I give
# here are not necessarily what Segger uses - I believe Segger uses an extension of
# ".jlink" for any of them)
# 
#   - JLink Commander Command file  (.jlinkcmd)
#   
#         A file that specifies a list of commands just as you wuld type them at
#         the jlink.exe command prompt (when it is run in interactive mode)
# 
#         A JLink Commander Command file is specified using the -CommandFile option
# 
#         These files do not support any control flow or parameterization, they
#         are just a list of commands
# 
#         It is undocumented, but you can use C++ style comments ("//") in
#         ".jlinkcmd" files 
# 
#         No parameterization: https://forum.segger.com/index.php/Thread/4027-SOLVED-Arguments-from-a-bat-file-to-a-jlink-file-with-CommanderScript/
#         
# 
#         
#   - JLink Commander Script file  (.jlinkscript)
# 
#         A file that uses a C-like language what can call APIs and have callbacks
#         to provide very low level access and control of the JLink device.
# 
#         A JLink script is often used to iniialize a microscontroller to prepare
#         it for debugging.
# 
#         A JLink Commander Script file is specified using the -CommanderScript option
# 
#         You can use C++ style comments ("//") in ".JLinkScript" files
# 
#         https://wiki.segger.com/J-Link_script_files
# 
#   - JLink Command Strings 
# 
#         Commands that are sent to the JLinkARM.dll to configure it.  From the Segger Wiki:
# 
#         The behavior of the J-Link can be customized via J-Link Command Strings passed
#         to the JLinkARM.dll which controls J-Link. Applications such as J-Link Commander,
#         but also other IDEs, allow passing one or more J-Link Command Strings. Command
#         strings can be used for passing commands to J-Link (such as switching on target
#         power supply), as well as customize the behavior (by defining memory regions and
#         other things) of J-Link. This way, J-Link Command Strings enable to set options
#         which are not configurable via dialogs or settings (e.g. in an IDE). 
# 
#         The recommend way to execute J-Link command strings is to add them to a J-Link
#         script file (using the JLINK_ExecCommand() API?)
# 
#         JLink Command Strings can also be sent va JLink Commander (or .jlinkcmd files)
#         using the `exec` command.
# 
#         https://wiki.segger.com/J-Link_Command_Strings
#         https://wiki.segger.com/J-Link_Commander#Using_J-Link_Command_Strings
#
#

import argparse
import contextlib
import functools
import logging
import shlex
import subprocess
import sys
import tempfile
import time
import os


# mechanism to define named constants in python
#   a variable can be added to the const class
#   with a value.  But any attempt to change that
#   value will raise an exception (const.ConstError)
#
# See http://code.activestate.com/recipes/65207-constants-in-python/?in=user-97991
#
class const:
    class ConstError(TypeError): pass
    def __setattr__(self,name,value):
        if self.__dict__.has_key(name):
            raise self.ConstError( "Can't rebind const(%s)"%name)
        self.__dict__[name]=value



# global constants
const.default_filelen = 256 * 1024

# global variables
verbose = False
debug = False
using_stdout= False

exe_names = {
    'stlink_exe': 'ST-LINK_CLI.exe',
    'stcube_exe': 'STM32_Programmer_CLI.exe',
    'jlink_exe':  'JLink.exe'
}




#
# smart_open allows a `with` statement to handle
# opening a file by name or returning a handle to
# stdout if no name is passed in or the name "-" is
# used.
#
# See https://stackoverflow.com/a/17603000/12711
#     https://stackoverflow.com/a/54073813/12711
#
@contextlib.contextmanager
def smart_open(filename, *args, **kwargs):
    if filename and filename != '-':
        fh = open(filename, *args, **kwargs)
    else:
        fh = os.fdopen(sys.stdout.fileno(), *args, **kwargs)

    try:
        yield fh
    finally:
        fh.close()





def printe(*args, **kwargs):
    logging.error( *args,  **kwargs)
    return print( *args, file=sys.stderr, **kwargs)



def printv(*args, **kwargs):
    # pass all arguments on to the built-in print() if we're in verbose mode
    global verbose

    # send verbose messags to info log regardless of if
    # the user wants to see them  on stdout
    logging.info( *args, **kwargs)

    if not verbose:
        return

    global using_stdout
    outfile = sys.stdout
    if using_stdout:
        # if the generated file is being sent to stdout, don't
        # send informational messages there too
        outfile = sys.stderr

    return print( *args, file=outfile, **kwargs)


def printi(*args, **kwargs):
    # pass all arguments on to the built-in print() if we're in verbose mode

    # send info messags to info log regardless of if
    # the user wants to see them  on stdout
    logging.info( *args, **kwargs)

    global using_stdout
    outfile = sys.stdout
    if using_stdout:
        # if the generated file is being sent to stdout, don't
        # send informational messages there too
        outfile = sys.stderr

    return print( *args, file=outfile, **kwargs)





# a time formatting function from https://stackoverflow.com/a/63198084/12711
from string import Formatter
from datetime import timedelta

def strfdelta(tdelta, fmt='{D:02}d {H:02}h {M:02}m {S:02.0f}s', inputtype='timedelta'):
    """Convert a datetime.timedelta object or a regular number to a custom-
    formatted string, just like the stftime() method does for datetime.datetime
    objects.

    The fmt argument allows custom formatting to be specified.  Fields can 
    include seconds, minutes, hours, days, and weeks.  Each field is optional.

    Some examples:
        '{D:02}d {H:02}h {M:02}m {S:02.0f}s' --> '05d 08h 04m 02s' (default)
        '{W}w {D}d {H}:{M:02}:{S:02.0f}'     --> '4w 5d 8:04:02'
        '{D:2}d {H:2}:{M:02}:{S:02.0f}'      --> ' 5d  8:04:02'
        '{H}h {S:.0f}s'                       --> '72h 800s'

    The inputtype argument allows tdelta to be a regular number instead of the  
    default, which is a datetime.timedelta object.  Valid inputtype strings: 
        's', 'seconds', 
        'm', 'minutes', 
        'h', 'hours', 
        'd', 'days', 
        'w', 'weeks'
    """

    # Convert tdelta to integer seconds.
    if inputtype == 'timedelta':
        remainder = tdelta.total_seconds()
    elif inputtype in ['s', 'seconds']:
        remainder = float(tdelta)
    elif inputtype in ['m', 'minutes']:
        remainder = float(tdelta)*60
    elif inputtype in ['h', 'hours']:
        remainder = float(tdelta)*3600
    elif inputtype in ['d', 'days']:
        remainder = float(tdelta)*86400
    elif inputtype in ['w', 'weeks']:
        remainder = float(tdelta)*604800

    f = Formatter()
    desired_fields = [field_tuple[1] for field_tuple in f.parse(fmt)]
    possible_fields = ('Y','m','W', 'D', 'H', 'M', 'S', 'mS', 'uS')
    constants = {'Y':86400*365.24,'m': 86400*30.44 ,'W': 604800, 'D': 86400, 'H': 3600, 'M': 60, 'S': 1, 'mS': 1/pow(10,3) , 'uS':1/pow(10,6)}
    values = {}
    for field in possible_fields:
        if field in desired_fields and field in constants:
            Quotient, remainder = divmod(remainder, constants[field])
            values[field] = int(Quotient) if field != 'S' else Quotient + remainder
    return f.format(fmt, **values)




def logged_call_build_msg(cmd, **kwargs):
    # put cmd into the msg, joining if it's a collection of arguments
    msg = "running: \n========\n{cmd}\n"
    if type(cmd) is str:
        msg = msg.format(cmd=cmd)
    else:
        msg = msg.format(cmd = " ".join(cmd))

    # list ant keyword arguments, one per line
    if kwargs:
        # list the keyword arguments passed in
        msg += "\nkeyword args:\n"
        for arg in kwargs:
            msg += "    {arg} = {val}\n".format(arg=arg, val=kwargs[arg])
    msg += "========"
    return msg


def logged_call(cmd, **kwargs):
    # special case when called with a string - split it into a
    #   collection of args - but **only** if `shell==False`
    logging.debug( f"cmd: {cmd}")
    logging.debug( f"kwargs: {kwargs}");

    if type(cmd) is str and not kwargs.get("shell"):
        cmd = shlex.split(cmd)
        logging.debug( f"split cmd: {cmd}")

    logging.debug( logged_call_build_msg(cmd, **kwargs))

    global using_stdout
    outfile=sys.stdout
    if using_stdout:
        outfile=sys.stderr

    return subprocess.run(cmd, stdout=outfile, **kwargs)


def logged_check_output(cmd, **kwargs):
    # special case when called with a string - split it into a
    #   collection of args - but **only** if `shell==False`

    if type(cmd) is str and not kwargs.get("shell"):
        cmd = shlex.split(cmd)

    logging.debug( logged_call_build_msg(cmd, **kwargs))

    return subprocess.check_output(cmd, **kwargs)




def parse_command_line(argv):
    parser = argparse.ArgumentParser(
                        description='XDC JLink Flash Dumper',
                        formatter_class=argparse.RawTextHelpFormatter,
                        allow_abbrev=False)
    
    parser.add_argument( 'dest_file', metavar='FILENAME', type=str,
                         action='store', default=argparse.SUPPRESS,
                         help="output filename for binary data (use '-' for stdout)")

    # the type argument for length and addr would normally be int, but then hex constants
    # like "0x1000" aren't accepted. So here I'm using a funky lambda that will accept and
    # interpret constants of any base.
    #
    # See https://stackoverflow.com/a/48950906/12711 for details
    #
    parser.add_argument( '-a', '--addr', metavar="ADDR",  required=True,
                         type=functools.wraps(int)(lambda x: int(x,0)),
                         help = "address of data to save to file")

    parser.add_argument( '-l', '--length', metavar="LEN",
                         type=functools.wraps(int)(lambda x: int(x,0)),
                         default=const.default_filelen,
                         help=f"length of file (default = {const.default_filelen})")

    parser.add_argument( '--device', default='GD32F303ZK',
                         help = "specify the microcontroller that will be read\n"
                                "    default is GD32F303ZK \n"
                                "    STM32 is an alias for STM32F103ZE \n"
                                "    GD32  is an alias for GD32F303ZK")

    # st-link is not supported yet
    # parser.add_argument('--stlink', action='store_true',
    #                              help="use ST-LINK jtag instead of JLink")

    parser.add_argument( '--jlink-prog', default='',
                         help = "full path/filename to JLink.exe")

    parser.add_argument( '--dbgif', choices=[ 'swd', 'jtag' ], default='swd',
                         help="debug interface to use: swd (default) or jtag")

    parser.add_argument( '--verbose', action='store_true',
                         help="enable verbose output")

    parser.add_argument( '--keep-temp', action='store_false', dest='delete_temp',
                         help="keep temporary files")

    parser.add_argument( '--debug', action='store_true',
                         help="enable debug logging")

    arguments = parser.parse_args(argv)


    if arguments.debug:
        # print verbose output too if we're doing DEBUG
        arguments.verbose = True
        logging.getLogger().setLevel(logging.DEBUG)
        
    if arguments.verbose:
        global verbose
        verbose = True
        
    if   arguments.device.casefold() == "STM32".casefold():
        arguments.device = "STM32F103ZE"
    elif arguments.device.casefold() == "GD32".casefold():
        arguments.device = "GD32F303ZK"

    import pprint
    logging.debug("List of command line arguments:\n")
    logging.debug(pprint.PrettyPrinter().pformat(vars(arguments)))

    return arguments



def use_stcube(arguments):
    logging.debug("use_stcube() entered")
    # newer ST-LINK flashing utility: STM32_Programmer_CLI
    #
    #   installed by default to: 
    #       "C:\Program Files\STMicroelectronics\STM32Cube\STM32CubeProgrammer\bin\"
    #
    #   examples:
    #       STM32_Programmer_CLI --connect port=SWD --erase all
    #       STM32_Programmer_CLI --connect port=SWD --download p82.precor.test.3183_4085665.partition-a.cpa.00039804.hex --verify
    #       STM32_Programmer_CLI --connect port=SWD --rst
    
    global exe_names
    
    stcube_exe = exe_names['stcube_exe']
    
    # check that STM32_Programmer_CLI flasher is installed
    import shutil
    if not shutil.which( stcube_exe):
        logging.error( f"{stcube_exe} utility not found, bailing out")
        printe( f"ERROR: {stcube_exe} utility not found")
        return 1

    if (arguments.erase):
        printv( "asking ST-LINK to erase the chip...")
        logged_call( f"{stcube_exe} --connect PORT={arguments.dbgif} --erase all")
        
    try:
        hex_files = arguments.firmware_files[0]
        for fname in hex_files:
            printv( f"asking ST-LINK to flash '{fname}'...")
            logged_call( f"{stcube_exe} --connect PORT={arguments.dbgif} --download \"{fname}\" --verify")
        
    except AttributeError:
        pass

    if arguments.reset:
        printv( "asking ST-LINK to reset the chip...")
        logged_call( f"{stcube_exe} --connect PORT={arguments.dbgif} -rst")
    else:
        logging.debug( "not asking ST-LINK to reset due to command line option")

    return 0




def use_stlink( arguments):
    logging.debug("use_stlink() entered")
    global exe_names
    
    stcube_exe = exe_names['stcube_exe']
    stlink_exe = exe_names['stlink_exe']
    
    # find out if the newer STM32CubeProgrammer flasher is installed
    import shutil
    stcube_fullpath = shutil.which( stcube_exe)
    if stcube_fullpath:
        logging.debug( f"full path to {stcube_exe}: '{stcube_fullpath}'")
        logging.debug( f"found {stcube_exe} utility, calling use_stcube()")
        printv( f"{stcube_exe} utility found, using it...")
        return use_stcube( arguments)

    printv( f"{stcube_exe} utility not found, trying {stlink_exe}...")

    # check that ST-LINK_CLI.exe flasher is installed
    stlink_fullpath = shutil.which( stlink_exe)
    logging.debug( f"full path to {stlink_exe}: '{stlink_fullpath}'")
    if not stlink_fullpath:
        logging.error( f"{stlink_exe} utility not found, bailing out")
        printe( f"ERROR: {stlink_exe} utility not found")
        return 1

    if (arguments.erase):
        printv( "asking ST-LINK to erase the chip...")
        logged_call( f"{stlink_exe} -c {arguments.dbgif} -ME")
        
    try:
        hex_files = arguments.firmware_files[0]
        for fname in hex_files:
            printv( f"asking ST-LINK to flash '{fname}'...")
            logged_call( f"{stlink_exe} -c {arguments.dbgif} -P \"{fname}\" -V \"after_programming\"")
        
    except AttributeError:
        pass

    if arguments.reset:
        printv( "asking ST-LINK to reset the chip...")
        logged_call( f"{stlink_exe} -c {arguments.dbgif} -Rst")
    else:
        logging.debug( "not asking ST-LINK to reset due to command line option")

    return 0



##############################
#
# Notes on commands for JLink Commander Command Files:
#
#   erase [Saddr, EAddr]
#       flash between SAddr and EAddr will be erased (it's not clear to
#       me if EAddr is erased or just past last erased address)
#
#       If addresses are not specified, all flash will be erased
#
#       Hex addresses start with 0x
#
#   LoadFile <filename>, [Addr]
#
#       load a .hex or .bin file into flash.
#
#       Addr must be specified for .bin files (and only .bin files)
#
#       The file type is determined by extension (I believe)
#
#   SaveBin <filename>, <Addr>, <numbytes>
#
#   VerifyBin <filename>, <addr>
#
#       compare specified file to location in flash
#
##############################
def use_jlink(arguments):
    logging.debug("use_jlink() entered")

    global exe_names
    
    jlink_exe = exe_names['jlink_exe']
    
    if len(arguments.jlink_prog) != 0:
        # user has specified a jlink.exe executable -
        #   use it instead of whatever is in the PATH env var
        jlink_exe = arguments.jlink_prog
        logging.debug( f"using user-specified jlink utility: '{jlink_exe}'")
        printv( f"using user-specified jlink utility: '{jlink_exe}'")
    else:
        # check that JLink.exe flasher is installed
        import shutil
        jlink_fullpath = shutil.which( jlink_exe)
        logging.debug( f"full path to {jlink_exe}: '{jlink_fullpath}'")
        if not jlink_fullpath:
            logging.error( f"{jlink_exe} utility not found, bailing out")
            printe( f"ERROR: {jlink_exe} utility not found")
            return 1
        #
        # jlink.exe is sometimes a Java utility.. check for that so we
        # don't get mysterious failures
        #
        if ("jdk" in jlink_fullpath.lower()) or ("java" in jlink_fullpath.lower()):
            logging.error( f"{jlink_exe} appears to be a Java utility ('{jlink_fullpath}')")
            logging.error( f"exiting...")
            sys.exit(1)

    jlinkcmd = tempfile.NamedTemporaryFile( 
                            mode='w',
                            prefix="dump-flash-cmds.",
                            suffix=".jlinkcmd",
                            delete=False)

    bin_file = arguments.dest_file
    global using_stdout
    if using_stdout:
        # we need to:
        #   1) give jlink.exe a temporary filename
        #   2) let jlink.exe write to that file
        #   3) cat the file to stdout
        #   4) delete the file
        bin_file_obj = tempfile.NamedTemporaryFile(
                            mode='w+b',
                            prefix="dump-flash-output.",
                            suffix=".bin",
                            delete=False)
        bin_file = bin_file_obj.name
        bin_file_obj.close()        # allow jlink.exe to write to the file
        logging.debug("created temp file for stdout capture: " + bin_file_obj.name)

    try:
        logging.debug("created JLink Command file: " + jlinkcmd.name)
        
        jlinkcmd.write( "ExitOnError 1\n")
        jlinkcmd.write( "Connect\n")
        jlinkcmd.write( "Halt\n")
        jlinkcmd.write( "\n")
        
        try:
            addr= f"{arguments.addr:#010x}"
            length = f"{arguments.length:#010x}"

            printv( f"asking jlink to dump memory to '{bin_file}' addr: {addr}, len: {length}...")

            jlinkcmd.write( f"SaveBin {bin_file},{addr},{length}\n")
            jlinkcmd.write( "\n")
            
        except AttributeError:
            pass
            
        jlinkcmd.write( "exit\n")

        jlinkcmd.close()

        printv( "")
        printv( "running jlink to do the work...")
        printv( "==========================================")
        logged_call( f"\"{jlink_exe}\"  -device {arguments.device} -if {arguments.dbgif} -speed 4000 -JTAGConf -1,-1 -CommandFile '{jlinkcmd.name}'")
        printv( "==========================================")
        printv( "jlink processing complete...")

    finally:
        jlinkcmd.close()
        if using_stdout:
            logging.debug( f"send contents of temorary file \"{bin_file}\" to stdout")
            bin_file_obj = open( bin_file,  "rb")
            bin_data = bin_file_obj.read()
            bin_file_obj.close()
            sys.stdout.buffer.write( bin_data)

        if arguments.delete_temp:
            logging.debug( f"deleting temporary file: '{jlinkcmd.name}'")
            os.remove( jlinkcmd.name)
            if using_stdout:
                logging.debug( f"deleting stdout temporary file: '{bin_file}'")
                os.remove( bin_file)
        else:
            logging.debug( f"!!keeping temporary file: '{jlinkcmd.name}'")
            
    return 0


def main( args):
    logging.debug("main() entered")
    
    # default logging is for CRITICAL only
    logging.getLogger().setLevel(logging.CRITICAL)

    fname = args.dest_file
    global using_stdout
    if fname == "-":
        # generated filedata will be sent to stdout, tell the
        # messaging functions so they don't also send to stdout
        using_stdout = True
        logging.debug( f"binary dump being sent to stdout, all messagesto stderr")

    # error messages get printed to stderr and logged at level ERROR
    #
    # verbose messages get logged at level INFO and will also
    #   be sent to stdout if the `--verbose` option is given
    #   if the generated file data is being directed to stdout,
    #   then verbose messages that are ususlly sent to stdout are
    #   sent to stderr instead (to avoid corrupting generated data)
    #
    # info messages get logged at logging level INFO and
    #   and are normally written to stdout, but if the generated
    #   file is being sent to stdout those message are written
    #   to stderr instead so they dnt' interfere with the file data

    if args.verbose:
        verbose = True

    if args.debug:
        debug = True
        logging.getLogger().setLevel(logging.DEBUG)

    import pprint
    logging.debug("List of command line arguments:\n")
    logging.debug(pprint.PrettyPrinter().pformat(vars(args)))

    start = time.time()
    
    if getattr( args, 'stlink', None):
        use_stlink( args)
    else:
        use_jlink( args)

    end = time.time()
    
    duration = end - start
    printi( "Done in %s" % strfdelta( duration, '{M} min {S:02.3f} sec', inputtype='s'))
    return 0



if __name__ == '__main__':
    if sys.version_info < (3,8):
        printe("Python version: " + sys.version)
        printe("This script requires Python 3.8 or later")
        sys.exit(1)

    args = parse_command_line( sys.argv[1:])

    # sys.exit( 0)    # just until the argument parsing looks good

    sys.exit(main(  args))
    

