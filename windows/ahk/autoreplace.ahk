#SingleInstance force


; fix-up mistyping contractions...

#Hotstring EndChars ()[]{}:"\,.?!`n `t

; SetTitleMatchMode 1  = Window title must start with the string
;                   2  = Window title must contain the string
;                   3  = Window title must exactly match string
SetTitleMatchMode 2

; fix variations of "I" being mistyped

; but don't upper-case 'i' in code editors 

;SetTitleMatchMode 3
#IfWinActive ahk_class ConsoleWindowClass           ; this is for Console programs (ie., cmd.exe)
::i::i
#IfWinActive Microsoft Visual C                     ; Visual C++ 6
::i::i
#IfWinActive ahk_class wndclass_desked_gsk          ; VB 6, VS 2003 & VS 2005
::i::i
#IfWinActive IAR        ; IAR Embedded Workbench
::i::i
#IfWinActive UltraEdit
::i::i
#IfWinActive UEStudio
::i::i
#IfWinActive UES
::i::i
#IfWinActive ahk_class Vim
::i::i
#IfWinActive ahk_class Notepad2
::i::i
#IfWinActive PrimalScript
::i::i
#IfWinactive ahk_class #32770                       ; dialog box window class (to disable in find & replace dialogs) 
::i::i
#IfWinActive


; also don't upcase 'i' in command line parameters
:c:-i::-i
:c:/i::/i

:c:i::I
:c1:I;m::I'm
:c1:I;ll::I'll
:c1:I;d::I'd
:c:i'm::I'm
:c:i'll::I'll
:c:i'd::I'd

::teh::the
::het::the
::hte::the

::ot::to

::aslo::also
::they;re::they're
::it;s::it's
::don;t::don't
::wnat::want
::deafult::default




;some of my other common misspellings

::becuase::because
::windwos::windows
::chuni::ChuNi
::doe snot::does not
::thier::their
::infomration::information
::infromation::information
::machiens::machines
::routien::routine
::handelr::handler
::thigns::things
::thign::thing
::thanks for you::thanks for your
::calss::class

; shortcuts

::wmail::michael.burr@philips.com
::wphone::(206) 664-5811
::nullmail::mailbox.1@mailnull.com
::empno::000278584

;===========================================
; Visual Studio Helpers

; map NumPad Clear (the 5 key) to something that Visual Studio 2002 and later can deal with (Shift-Ctrl-Alt-5) 
; so I can map that key to 'scroll to center:
;
;	Edit.ScrollLineCenter  (VS 2003 & 2005 - and probably 2002) 
;	WindowScrollToCenter   (VS 6)
;

NumpadClear::!^+5 
;===========================================



;===========================================
;
; Fix up for editors that need to hit Home twice to get to column 1
;

#IfWinActive Microsoft Visual C                     ; Visual C++ 6
Home::Send {Home}{Home}
NumpadHome::Send {NumpadHome}{NumpadHome}

#IfWinActive PrimalScript                           ; PrimalScript 4.1
Home::Send {Home}{Home}
NumpadHome::Send {NumpadHome}{NumpadHome}

#IfWinActive ahk_class Notepad2
Home::Send {Home}{Home}
NumpadHome::Send {NumpadHome}{NumpadHome}

#IfWinActive
;===========================================

