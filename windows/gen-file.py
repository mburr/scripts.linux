#!python3

# weird hack to force a "readable" syntax error when this is run under
#   python2 instead of python3
#
# see https://stackoverflow.com/a/65407535/12711
#
# This takes advantage of a particular syntax that is new to Python 3,
#   "PEP 3132 -- Extended Iterable Unpacking"
#   https://www.python.org/dev/peps/pep-3132/
#
Python_2_is_running, *Python_3_is_needed="python 3 is needed" # this script needs to be run with python 3


import sys

import argparse
import codecs
import contextlib
import functools
import logging
import random
import re
import time
import os


# mechanism to define named constants in python
#   a variable can be added to the const class
#   with a value.  But any attempt to change that
#   value will raise an exception (const.ConstError)
#
# See http://code.activestate.com/recipes/65207-constants-in-python/?in=user-97991
#
class const:
    class ConstError(TypeError): pass
    def __setattr__(self,name,value):
        if self.__dict__.has_key(name):
            raise self.ConstError( "Can't rebind const(%s)"%name)
        self.__dict__[name]=value



#
# smart_open allows a `with` statement to handle
# opening a file by name or returning a handle to
# stdout if no name is passed in or the name "-" is
# used.
#
# See https://stackoverflow.com/a/17603000/12711
#     https://stackoverflow.com/a/54073813/12711
#
@contextlib.contextmanager
def smart_open(filename, *args, **kwargs):
    if filename and filename != '-':
        fh = open(filename, *args, **kwargs)
    else:
        fh = os.fdopen(sys.stdout.fileno(), *args, **kwargs)

    try:
        yield fh
    finally:
        fh.close()


# global constants
const.default_bufsize = 256 * 1024
const.default_filelen = 256 * 1024

# global variables
debug = False
verbose = False
using_stdout = False



# a regular expression and function to decode arbitrary
# escape sequensces in strings.
#
# See: https://stackoverflow.com/a/24519338/12711

ESCAPE_SEQUENCE_RE = re.compile(r'''
    ( \\U........      # 8-digit hex escapes
    | \\u....          # 4-digit hex escapes
    | \\x..            # 2-digit hex escapes
    | \\[0-7]{1,3}     # Octal escapes
    | \\N\{[^}]+\}     # Unicode characters by name
    | \\[\\'"abfnrtv]  # Single-character escapes
    )''', re.UNICODE | re.VERBOSE)

def decode_escapes(s):
    def decode_match(match):
        return codecs.decode(match.group(0), 'unicode-escape')

    return ESCAPE_SEQUENCE_RE.sub(decode_match, s)


def printe(*args, **kwargs):
    logging.error( *args,  **kwargs)
    return print( *args, file=sys.stderr, **kwargs)



def printv(*args, **kwargs):
    # pass all arguments on to the built-in print() if we're in verbose mode
    global verbose

    # send verbose messags to info log regardless of if
    # the user wants to see them  on stdout
    logging.info( *args, **kwargs)

    if not verbose:
        return

    global using_stdout
    outfile = sys.stdout
    if using_stdout:
        # if the generated file is being sent to stdout, don't
        # send informational messages there too
        outfile = sys.stderr

    return print( *args, file=outfile, **kwargs)


def printi(*args, **kwargs):
    # pass all arguments on to the built-in print() if we're in verbose mode

    # send info messags to info log regardless of if
    # the user wants to see them  on stdout
    logging.info( *args, **kwargs)

    global using_stdout
    outfile = sys.stdout
    if using_stdout:
        # if the generated file is being sent to stdout, don't
        # send informational messages there too
        outfile = sys.stderr

    return print( *args, file=outfile, **kwargs)




# generate a stream of andom bytes
#
# From: https://stackoverflow.com/a/7044367/12711
#
# Note: Python 3.9 has random.randbytes(), but defining it here lets us run under
#   earlier versions of python
#
def randbytes(n):
    for _ in range(n):
        yield random.getrandbits(8)



def get_rand_data( sz):
    return bytearray( randbytes(sz))




def generate_file( fname,  length, pattern=None):
    global using_stdout
    if fname == "-":
        # generated filedata will be sent to stdout, tell the
        # messaging functions so they don't also send to stdout
        using_stdout = True

    if using_stdout:
        printi( f"Generating data of length {length} to stdout")
    else:
        printi( f"Generating {fname} of length {length}")

    bytes_remaining = length

    # create a buffer of data to write to the file
    if pattern:
        logging.debug( f"pattern is \"{pattern}\"")
        logging.debug( f"pattern is {len(pattern)} bytes")

        # interpret escape sequences
        pattern = decode_escapes(pattern)
        logging.debug( f"pattern is {len(pattern)} bytes (unescaped)")

        # convert the text pattern into an aray of bytes
        # using UTF-8 in case there are some non-ASCII characters
        # in there
        buf = bytearray( pattern, "utf-8")

        # if the pattern begins with  "0x" see if it is a
        # string of hex-encoding (whitespace is permitted)
        if pattern.startswith( "0x") or pattern.startswith( "0X"):
            try:
                logging.debug("pattern: attempting \"0x\" conversion")
                buf = bytearray.fromhex(pattern[2:])
            except:
                printe("pattern: 0x conversion failed, interpreting pattern as string")

        # double the buffer until it is at least as long as the lesser
        # of lenth or 1MB
        while len(buf) < length and len(buf) < const.default_bufsize:
            buf = buf + buf

        # make sure buf isn't longer than length
        del buf[length:]

    else:
        # no pattern was given - generate random data
        buf = get_rand_data( min( const.default_bufsize, bytes_remaining))

    try:
        with smart_open( fname,  "wb") as file:
            while bytes_remaining > 0:
                file.write( buf)
                bytes_remaining -= len(buf)
                bytes_remaining = max( bytes_remaining, 0)
                if pattern:
                    # make sure the buffer isn't longer than we want
                    del buf[bytes_remaining:]
                else:
                    # writing random data, so we need new random data
                    buf = get_rand_data( min( const.default_bufsize, bytes_remaining))


    except IOError:
        printe( f"error opening file \"{fname}\"")

    return 0



def parse_command_line(argv):
    parser = argparse.ArgumentParser(
                    description='generate arbitrary/random files',
                    formatter_class=argparse.RawTextHelpFormatter,
                    allow_abbrev=False)

    parser.add_argument( 'output_file', metavar='FILENAME', type=str,
                         action='store', default=argparse.SUPPRESS,
                         help="output filename (use '-' for stdout)")

    # the type argument would normally be int, but then hex constants like "0x1000" aren't accepted.
    # so here I'm using a funky lambda that will accept and interpret constants of any base.
    #
    # See https://stackoverflow.com/a/48950906/12711 for details
    #
    parser.add_argument( '-l', '--length', metavar="LEN",
                         type=functools.wraps(int)(lambda x: int(x,0)),
                         default=const.default_filelen,
                         help=f"length of file (default = {const.default_filelen})")

    parser.add_argument( '-p', '--pattern', metavar='PATTERN', type=str,
                         help="fill pattern (instead of random data)")

    parser.add_argument( '--verbose', action='store_true',
                         help="enable verbose output")

    parser.add_argument( '--debug', action='store_true',
                         help="enable debug logging")

    args = parser.parse_args(argv)

    return args



def main(args):
    global verbose

    # default logging is for CRITICAL only
    logging.getLogger().setLevel(logging.CRITICAL)

    # error messages get printed to stderr and logged at level ERROR
    #
    # verbose messages get logged at level INFO and will also
    #   be sent to stdout if the `--verbose` option is given
    #   if the generated file data is being directed to stdout,
    #   then verbose messages that are ususlly sent to stdout are
    #   sent to stderr instead (to avoid corrupting generated data)
    #
    # info messages get logged at logging level INFO and
    #   and are normally written to stdout, but if the generated
    #   file is being sent to stdout those message are written
    #   to stderr instead so they dnt' interfere with the file data

    if args.verbose:
        verbose = True

    if args.debug:
        debug = True
        logging.getLogger().setLevel(logging.DEBUG)

    import pprint
    logging.debug("List of command line arguments:\n")
    logging.debug(pprint.PrettyPrinter().pformat(vars(args)))

    result = generate_file(args.output_file, args.length, args.pattern)

    return result





if __name__ == '__main__':
    if sys.version_info < (3,):
        printe("Python version: " + sys.version)
        printe("This script requires Python 3 or later")
        sys.exit(1)
    sys.exit(main(parse_command_line(sys.argv[1:])))


