@echo off
::
:: RefreshEnv.cmd
::
:: Batch file to read environment variables from registry and
:: set session variables to these values.
::
:: With this batch file, there should be no need to reload command
:: environment every time you want environment changes to propagate
::
:: Originally from: https://github.com/chocolatey/choco/blob/master/src/chocolatey.resources/redirects/RefreshEnv.cmd

::echo "RefreshEnv.cmd only works from cmd.exe, please install the Chocolatey Profile to take advantage of refreshenv from PowerShell"


goto main

:DoSet
    if "%refreshenv_verbose%" == "true" (
        echo/set %1
    )
    if "%refreshenv_noset%" == "" (
        set %1
    )

    goto :eof

:: Set one environment variable from registry key
:SetFromReg
    :: See https://stackoverflow.com/a/56591836/12711 for the reason there is an extra
    ::   set of double-quotes surrounding the command string in this `for` loop
    ::
    :: BUG: the following doesn't properly parse out registry value names
    ::    that contain spaces
    ::
    ::    It is rare that something will create environment variables with
    ::    space in the name, but Windows allows it and PyCharm does it, even
    ::    though it's not a very nice thing to do.
    ::
    ::    For example, PyCharm has the value "HKCU\Environment\PyCharm Community Edition",
    ::    and puts %PyCharm Community Edition% in HKCU\Environment\Path.
    ::    That results in this code putting " Community Edition" in the path
    ::
    :: echo/DEBUG: setting %1 %2 %3
    for /f "usebackq skip=2 tokens=2,*" %%I IN (`cmd /c ""%WinDir%\System32\Reg" QUERY "%~1" /v "%~2" 2> nul"`) do (
        call :DoSet "%~3=%%J"
    )
    goto :EOF

:: Get a list of environment variables from registry
:GetRegEnv
    if "%refreshenv_verbose%" == "true" (
        echo/
        echo/Processing "%~1"
        echo/
    )

    ::
    :: BUG: the following doesn't properly parse out registry value names
    ::    that contain spaces
    ::
    for /f "usebackq skip=2" %%I IN (`cmd /c ""%WinDir%\System32\Reg" QUERY "%~1""`) do (
        rem PATH is handled specially
        rem COMSPEC should only be set by the command processor
        rem USERNAME in the environment is "System"
        rem PROCESSOR_ARCHITECTURE is skipped in case running a 32-bit cmd
        rem TMP and TEMP are skipped because setting them screws up tcc
        rem echo/DEBUG: "%~1" "%%~I" "%%~I"
        if /I not "%%~I"=="Path" (
        if /I not "%%~I"=="COMSPEC" (
        if /I not "%%~I"=="USERNAME" (
        if /I not "%%~I"=="PROCESSOR_ARCHITECTURE" (
        if /I not "%%~I"=="TMP" (
        if /I not "%%~I"=="TEMP" (
            call :SetFromReg "%~1" "%%~I" "%%~I"
        ))))))
    )
    goto :EOF

:params
    call :cleanup

    :params_loop
        if "%1" == ""           goto :params_end
        if "%1" == "-v"         set "refreshenv_verbose=true"
        if "%1" == "--verbose"  set "refreshenv_verbose=true"
        if "%1" == "--noset"    set "refreshenv_noset=true"
        if "%1" == "-h"         set "refreshenv_help=true"
        if "%1" == "--help"     set "refreshenv_help=true"

        shift
        goto :params_loop

    :params_end

    goto :eof


:cleanup
    set "refreshenv_verbose="
    set "refreshenv_noset="
    set "refreshenv_help="
    goto :eof


:usage
    echo. %~nx0 - read environment variables from registry into current environment
    echo.
    echo.      -v, --verbose    verbose output
    echo.          --noset      do not actually change environment (for debugging)
    echo.
    echo.      -h, --help       display this help message
    echo.

    call :cleanup

    exit /b
    goto :eof



:main
    call :params %*

    if "%refreshenv_help%"=="true" goto :usage

    echo Refreshing environment variables from registry for cmd.exe. Please wait...

    call :GetRegEnv "HKLM\System\CurrentControlSet\Control\Session Manager\Environment"
    call :GetRegEnv "HKCU\Environment"

    :: Special handling for PATH - mix both User and System
    if "%refreshenv_verbose%" == "true" (
        echo/
        echo/Processing PATH
        echo/
    )
    call :SetFromReg "HKLM\System\CurrentControlSet\Control\Session Manager\Environment" Path Path_HKLM
    if "%refreshenv_verbose%" == "true" echo/
    call :SetFromReg "HKCU\Environment" Path Path_HKCU
    if "%refreshenv_verbose%" == "true" echo/

    call :DoSet "PATH=%Path_HKLM%;%Path_HKCU%"

    echo Finished.
