#! /usr/bin/perl

# from https://unix.stackexchange.com/a/4529/7084

use strict;
use warnings;

while (<>) {
  s/\e\[?.*?[\@-~]//g; # Strip ANSI escape codes
  print;
}

