#! /bin/bash




# this script needs to be run as root (using sudo generally) - a requirement
# of `openvpn`.
#
# IMPORTANT NOTE: if the following suggestion has a problem and creates
#  a bad sudoers policy file then sudo will no longer work and you'll have
#  to logon as root or into recovery mode to fix the problem (many Linux
#  distros do not enable root logon by default, so recovery mode might be
#  your only option).  It is possible that 'su' might get you a root
#  erminal session depending on how your distro is set up.
#
#  If you are running in a virtual machine, take a snapshot before updating
#  the sudoer policies so that you can undo any problems easily by rolling
#  back to the snapshot.
#
#  Another alternative to help recover from problems is to have a separate
#  terminal window in a "sudo bash" session so you have a root command line
#  already open to fix problems.

# If you copy/paste the following lines (between the "===== %< ======" lines)
# to a terminal, a sudoer policy file will be created in /etc/sudoers.d/ - but
# it will have an extension of ".pending" so it won't be used by sudo.
#
# Examine the resulting "pending" policy file carefully.  If it is not valid
# syntax and is made "active" by removing the extension, sudo will no longer
# work (see the above "IMPORTANT NOTE").
#
# If the "pending" sudoers policy is correct, you can rename it (sudo required)
# so it doesn't have an extension and it will become active.
# if it looks correct, move  set up so you won't need to enter
# your password to sudo:

true <<'EXAMPLE'
#===== %< ===========================================
fname="/etc/sudoers.d/start-openvpn.pending"
sudo bash -c "cat  >$fname <<EOF
# no need to give a password to start the VPN

$USER ALL = NOPASSWD:$(realpath start-openvpn.sh)
EOF
chmod 0440 $fname"
#===== %< ===========================================
EXAMPLE


function loge()
{
    >&2 echo "$@"
}


function die()
{
    loge "$@"
    exit 1
}


help ()
{
	cat <<HELP_TEXT
Starts an OpenVPN session

This script needs to run as root (an OpenVPN requirement).

This script depends on two packages being installed:

    - expect
    - oathtool

The script requires 2 files exist:

    - ~/.vpn-xdc/client.ovpn    This is OpenVPN's client configuration file

    - ~/.vpn-xdc/ovpn.creds     A file with credentials for OpenVPN and the OTP

The credentials file has the following format:

    - line 1 is the OpenVPN userid
    - line 2 is the OpenVPN password
    - Line 3 is the OTP "secret" used by the 'oathtool' utility to generate
        an OTP (one time password) token for the OpenVPN connection. The OTP
        secret should be in "Base32" format.

You can obtain the OTP secret by decoding the QR code you were given when your
VPN acount was set up.  The QR code will decode into au otpauth:// URL that
looks like:

    otpauth://totp/OpenVPN:michael.burr@openvpnxdc.precor.com?secret=ABCD1234EFGH5678&issuer=OpenVPN
                                                                     ^^^^^^^^^^^^^^^^

In this example the OTP secret is ABCD1234EFGH5678 which would be placed in
the 3rd line of the ovpn.creds file


You can specify different paths for the OpenVPN client.ovpn file and
the ovpn.creds file using the following environement variables (the value
of these variables should be the full pathname of the files):

    - VPN_OVPN_CLIENT for the full pathname of the client.ovpn file
    - VPN_OVPN_CREDS  for the full pathname of the ovpn.creds file

HELP_TEXT

	return 0
}





# an embedded expect/tcl script:

expect_script=$(cat <<'EXPECT_SCRIPT_EOF'
# an expect script that will start an OpenVPN session without prompting for
# credentials.
#
# This script depends on:
#
#	- expect
#	- oathtool
#
# The OpenVPN credentials are provided in a file passed to the `openvpn`
# program with the `--auth-user-pass` option.  This file is a simply text
# file with the userid on line 1 and password on line 2.
#
# The 3rd line of the OpenVPN credentials file is used to provide the
# OTP 'secret' that is passed to `oathtool` to get the one-time-password.
# since `openvpn` only cares about the first 2 lines of the credentials
# file, we piggyback the OTP secret in the 3rd line.
#
# The OTP secret comes from the "otpauth://" URL provided in the QR code
# that OpenVPN (or the OpenVPN admin) gives you.
#
# For example, if the otpauth:// URL looks like:
#
#	otpauth://totp/OpenVPN:michael.burr@openvpnxdc.precor.com?secret=ABCD1234EFGH5678&issuer=OpenVPN
#                                                                    ^^^^^^^^^^^^^^^^
#
# Then the 3rd line of the creds file would simply be: ABCD1234EFGH5678
#
# Thanks to Pete Truog forthe working expect script that I could expand on.

set timeout 30

# configuration file paths
#
#	vpnclientfilename:	OpenVPN's client.ovpn file
#	credsfilename:		credentials for OpenVPN, line 1: userid, line 2: password
#						the 3rd line contains the OTP secret passed to `oathtool`

set home /home/user
if { [info exists ::env(HOME)] } {
	set home $::env(HOME)
}


set vpnclientfilename "${home}/.vpn-xdc/client.ovpn"
set credsfilename     "${home}/.vpn-xdc/ovpn.creds"

# see if any of the file locations are overridden by environment variables

if { [info exists ::env(VPN_OVPN_CLIENT)] } {
	set vpnclientfilename $::env(VPN_OVPN_CLIENT)
}
if { [info exists ::env(VPN_OVPN_CREDS)] } {
	set credsfilename $::env(VPN_OVPN_CREDS)
}

if { ![file exists $vpnclientfilename] } {
	puts "Cannot find OpenVPN client file: \"$vpnclientfilename\""
	puts "Use the --help option for more details."
	exit 1
}

if { ![file exists $credsfilename] } {
	puts "Cannot find OpenVPN credentials file: \"$credsfilename\""
	puts "Use the --help option for more details."
	exit 1
}

spawn bash -c "oathtool -b --totp \$(head -n 3 $credsfilename | tail -n 1)"
expect -re \\d+
set otp $expect_out(0,string)

spawn openvpn --config "$vpnclientfilename" --auth-user-pass "$credsfilename" --auth-retry interact

expect "CHALLENGE: Enter Authenticator Code"
send "$otp\n"

interact

EXPECT_SCRIPT_EOF
) # end of the embedded expect script



main ()
{
	# check if a help screen is asked for...
	# Currently no other command line options are  supported

	while [ -n "$1" ]; do
		case "$1" in
			"-h" | "--help")
				help
				exit 0
				;;
		esac

		shift
	done

	# check if we're running as root
	[ $EUID -eq 0 ] || die "Please run as root"

	# execute the expect script

	/usr/bin/expect -f <(echo "${expect_script}")
}


main "$@"
