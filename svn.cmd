@echo off
rem invoke the svn-log wrapper instead of svn.exe so that oneline enhancements take effect
rem
rem the wrapper will pass on all appropriate svn commands and options so it should work
rem transparently
rem
rem NOTE: currently svn-log.py *is* the wrapper when it's name svn.py
rem
rem I have found that confusing, so in the future let's create a sepatate
rem wrapper program that searches for command wrappers of the name "svn-xxx.py"
rem or "svn-xxx.exe" (or whatever executable file that starts a root name "svn-xxx")
rem where "xxx" is the svn command.
rem
rem this will make it easier to enhance existing svn commands with a wrapper script
rem (like we're doing here with "svn log") and will also allow seamlessly adding
rem new svn commands similar to how git handles extensions.

rem first announce what this batch file is doing *if* the --debug option is specified
for %%x in (%*) do (
    call :handle_debug %%x
)

svn.py %* 
goto :eof

:handle_debug
if "%1" == "--debug" echo svn.cmd executing svn.py wrapper...
goto :eof
