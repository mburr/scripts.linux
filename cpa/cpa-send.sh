default_cpaport=/dev/ttycpa
if ! [ -c $default_cpaport ]; then
    default_cpaport=/dev/ttyS0
fi

cpaport=${CPAPORT:-$default_cpaport}

cpacmd="$@"

case "$1" in
    --reset) 
        cpacmd="<ctrl>RESET</ctrl>"
        ;;
        
    --version)
        cpacmd="<ver>SOFTWARE</ver>"
        ;;
        
    --ctrl)
        cpacmd="<ctrl>$2</ctrl>"
        ;;
esac

echo "$cpacmd" >$cpaport & timeout 5 tr "\015" "\012" <$cpaport

