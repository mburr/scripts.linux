#
# The first group of settings is taken from flash_cpa.sh 
# (legacy precor script to flash the CPA)
#
# The second group are settings that change the defaults to disable them.
# They undefine several editing characters and turn off uppercase to lower
# case conversion on input (I'm surprised that was the default setting) 
# and translating cr to crlf on output.
#
#
# The third group just ensures that the remaining settings are at the default
#
stty -F /dev/ttyHS1   115200                                                      \
       intr undef  quit undef  start undef  stop undef  susp undef  min 1       time 5      \
       -icrnl      -ixon       -ixoff       -opost      -isig       -icanon     -iexten      \
       -echo       -echoe      -echok       -echonl                                          \
       \
       erase undef kill undef  eof undef   rprnt undef werase undef lnext undef discard undef \
       -iuclc      -onlcr                                                                    \
       \
       eol undef   eol2 undef  swtch undef -parenb      cs8         hupcl      -cstopb      \
       cread       clocal     -crtscts     -ignbrk     -brkint     -ignpar     -parmrk      \
       -inpck      -istrip     -inlcr       -igncr      -ixany       imaxbel     iutf8       \
       -olcuc      -ocrnl      -onocr       -onlret     -ofill      -ofdel       nl0

