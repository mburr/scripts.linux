#! /bin/bash

args=( "$@" )



# getdriver() gettop() and m() are dependencies
#    stolen from AOSP's ./build/envsetup.sh
#
getdriver () 
{ 
    local T="$1";
    test "$WITH_STATIC_ANALYZER" = "0" && unset WITH_STATIC_ANALYZER;
    if [ -n "$WITH_STATIC_ANALYZER" ]; then
        echo "$T/prebuilts/misc/linux-x86/analyzer/tools/scan-build/scan-build --use-analyzer $T/prebuilts/misc/linux-x86/analyzer/bin/analyzer --status-bugs --top=$T";
    fi
}


gettop () 
{ 
    local TOPFILE=build/core/envsetup.mk;
    if [ -n "$TOP" -a -f "$TOP/$TOPFILE" ]; then
        ( cd $TOP;
        PWD= /bin/pwd );
    else
        if [ -f $TOPFILE ]; then
            PWD= /bin/pwd;
        else
            local HERE=$PWD;
            T=;
            while [ \( ! \( -f $TOPFILE \) \) -a \( $PWD != "/" \) ]; do
                \cd ..;
                T=`PWD= /bin/pwd -P`;
            done;
            \cd $HERE;
            if [ -f "$T/$TOPFILE" ]; then
                echo $T;
            fi;
        fi;
    fi
}


m () 
{ 
    local T=$(gettop);
    local DRV=$(getdriver $T);
    if [ "$T" ]; then
        $DRV make -C $T -f build/core/main.mk $@;
    else
        echo "Couldn't locate the top of the tree.  Try setting TOP.";
        return 1;
    fi
}


# useful targets:
#
#    showcommands - make displays each command executed
#    dist  - build all artifacts associated with a release

logfile="${HOME}/temp/build-logs/$(basename $WORKDIR)-${TARGET_BUILD_VARIANT}-$(date +%Y.%m.%d-%H%M)-make.log"

echo -e "logfile: ${logfile}" "\n"
time (m  -j$CPUS "${args[@]}" 2>&1) | tee "${logfile}"
echo -e "\n" "logfile: ${logfile}"
