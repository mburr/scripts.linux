#!/bin/bash

#
# commit-missing-analysis.sh - display commits in a source branch that are not in the target branch
#
#	This script can be used to determine whether a release branch's commits have also been 
#	added to the "precor" master branch.
#
#	This script finds commits that exist in the source branch but not the target, but since
#	cherry-picked commmits can be in both branches with different commit-ids, this script 
#	uses `git patch-id` to identify whether one commit matches another.
#
#	(Note: a gerrit-style  "Change-Id" trailer in a commit message is no longer needed)
#
#	TODO:
#
#	  - add actual support for the --repo option
#
#	  - allow commit IDs to be passed in instead of forcing the list
#			to be the difference between two branches?
#
#	  - if one (or both) branches involved doesn't have a local checkout, 
#			but does have a remote, then push the current branch
#			stashing if necessary), checkout the remote locally, then
#			restore the original branch (and unstash)
#
#	  - sensible --verbose output?
#

# NOTES:
#
#	a better way to get a list of commit ids than parsing "git ls":
#
#		git rev-list ^precor v8.0.3
#
#		see: https://git-scm.com/docs/git-rev-list
#
#
#	a potentialy better way to find matching cherry-picks:
#
#		PATCH_ID=$((git show $commit-id-to-match | git patch-id))
#		git log -p branch-name | git patch-id | grep $PATCH_ID
#
#		It uses git's algorithm for determining if one commit is the same as another - the
#		patch-id is a hash of the commit's diff, more or less.  I'm not sure if the patch-id
#		includes the commit comment as well or other attributes like author or date.
#
#		see: https://stackoverflow.com/a/47288713/12711
#
#
#	To find the point where a branch was created from another branch:
#
#		bash -c 'diff --old-line-format='' --new-line-format='' <(git rev-list --first-parent "${1:-master}") <(git rev-list --first-parent "${2:-HEAD}") | head -1'
#
#		see: https://stackoverflow.com/a/4991675/12711
#


scriptname="${0/#$HOME/\~}"

# utility functions
# check if we want to use the "canonical" *.shlib files (which
#    introduces a dependency, or use versions of the library functions
#    that are inline (no external dependecies)
if [ ! -z "$SHLIB" ] ; then
	# a .shlib directory is provided, use it to source the libraries
	
	source "$SHLIB/bash-utils.shlib"
	source "$SHLIB/getopts_long.shlib"
else
	# use inline versions of the functions

	#
	# truncate_ellipsis()
	#
	# truncate a string to a maximum length, putting "..." at the end
	#
	#   - if truncation occurs. If truncation occurs the trucated string 
	#     plus the "..." will results in the specified length string
	#
	#   - the length must be greater than 3 or you'll get weird results
	#
	#   - the result is printed to stdout
	#
	#	The first argument is the string to truncate
	#	The second argument is the max length of the result (optional, default=80)
	#
	#	TODO:
	#		- trim whitespace from before the ellispsis (whitespace there looks odd)
	#		- trim from the middle and put ellipsis there
	#
	#	from: https://askubuntu.com/a/1048443
	#

	truncate_ellipsis() {
		local string="$1"
		local len="${2:-80}"

		echo "${string}" | \
		awk -v len="${len}" '{ if (length($0) > len) print substr($0, 1, len-3) "..."; else print; }'
	}



	# Trim leading and trailing whitespace from a variable 
	#
	#	from: 
	#		https://stackoverflow.com/a/7486606
	#		http://mywiki.wooledge.org/glob
	#
	#
	trim() {
		# Determine if 'extglob' is currently on.
		local extglobWasOff=1
		shopt extglob >/dev/null && extglobWasOff=0 
		(( extglobWasOff )) && shopt -s extglob # Turn 'extglob' on, if currently turned off.
		# Trim leading and trailing whitespace
		local var=$1
		var=${var##+([[:space:]])}
		var=${var%%+([[:space:]])}
		(( extglobWasOff )) && shopt -u extglob # If 'extglob' was off before, turn it back off.
		echo -n "$var"  # Output trimmed string.
	}




	# getopts_long -- POSIX shell getopts with GNU-style long option support
	#
	# Copyright 2005-2009 Stephane Chazelas <stephane_chazelas@yahoo.fr>
	# 
	# Permission to use, copy, modify, distribute, and sell this software and
	# its documentation for any purpose is hereby granted without fee, provided
	# that the above copyright notice appear in all copies and that both that
	# copyright notice and this permission notice appear in supporting
	# documentation.  No representations are made about the suitability of this
	# software for any purpose.  It is provided "as is" without express or
	# implied warranty.

	# History:
	# 2005 ?? - 1.0
	#   first version
	# 2009-08-12 - 1.1
	#   thanks to ujqm8360@netwtc.net for helping fix a few bugs:
	#     - -ab x where -b accepts arguments wasn't handled properly. Also,
	#       $OPTLIND wasn't set properly (at least not the same way as most
	#       getopts implementation do).
	#     - The handling of ambiguous long options was incorrect.

	getopts_long() {
	  # args: shortopts, var, [name, type]*, "", "$@"
	  #
	  # getopts_long parses command line arguments. It works like the
	  # getopts shell built-in command except that it also recognises long
	  # options a la GNU.
	  #
	  # You must provide getopts_long with the list of supported single
	  # letter options in the same format as getopts', followed by the
	  # variable name you want getopts_long to return the current processed
	  # option in, followed by the list of long option names (without the
	  # leading "--") and types (0 or no_argument, 1 or required_argument,
	  # 2 or optional_argument). The end of the long option specification
	  # is indicated by an empty argument. Then follows the list of
	  # arguments to be parsed.
	  #
	  # The $OPTLIND variable must be set to 1 before the first call of the
	  # getopts_long function to process a given list of arguments.
	  #
	  # getopts_long returns the value of the current option in the variable
	  # whose name is provided as its second argument (be careful to avoid
	  # variables that have a special signification to getopts_long or the
	  # shell or any other tool you may call from your script). If the
	  # current option is a single letter option, then it is returned
	  # without the leading "-". If it's a long option (possibly
	  # abbreviated), then the full name of the option (without the leading
	  # "--") is returned. If the option has an argument, then it is stored
	  # in the $OPTLARG variable. If the current option is not recognised,
	  # or if it is provided with an argument while it is not expecting one
	  # (as in --opt=value) or if it is not provided with an argument while
	  # it is expecting one, or if the option is so abbreviated that it is
	  # impossible to identify the option uniquely, then:
	  #   - if the short option specifications begin with ":", getopts_long
	  #     returns ":" in the output variable and $OPTLARG contains the
	  #     faulty option name (in full except in the case of the ambiguous
	  #     or bad option) and $OPTLERR contains the error message.
	  #   - if not, then getopts_long behaves the same as above except that
	  #     it returns "?" instead of ":", leaves $OPTLARG unset and
	  #     displays the error message on stderr.
	  #
	  # The exit status of getopts_long is 0 unless the end of options is
	  # reached or an error is encountered in the syntax of the getopts_long
	  # call.
	  #
	  # After getopts_long has finished processing the options, $OPTLIND
	  # contains the index of the first non-option argument or $# + 1 if
	  # there's no non-option argument.
	  #
	  # The "=" character is not allowed in a long option name. Any other
	  # character is. "-" and ":" are not allowed as short option names. Any
	  # other character is. If a short option appears more than once in the
	  # specification, the one with the greatest number of ":"s following it
	  # is retained. If a long option name is provided more than once, only
	  # the first one is taken into account. Note that if you have both a -a
	  # and --a option, there's no way to differentiate them. Beside the
	  # $OPTLIND, $OPTLARG, and $OPTLERR, getopts_long uses the $OPTLPENDING
	  # variable to hold the remaining options to be processed for arguments
	  # with several one-letter options. That variable shouldn't be used
	  # anywhere else in your script. Those 4 variables are the only ones
	  # getopts_long may modify.
	  #
	  # Dependency: only POSIX utilities are called by that function. They
	  # are "set", "unset", "shift", "break", "return", "eval", "command",
	  # ":", "printf" and "[". Those are generally built in the POSIX
	  # shells. Only "printf" has been known not to be in some old versions
	  # of bash, zsh or ash based shells.
	  #
	  # Differences with the POSIX getopts:
	  #  - if an error is detected during the parsing of command line
	  #    arguments, the error message is stored in the $OPTLERR variable
	  #    and if the first character of optstring is ':', ':' is returned in
	  #    any case.
	  #  - in the single-letter option specification, if a letter is
	  #    followed by 2 colons ("::"), then the option can have an optional
	  #    argument as in GNU getopt(3). In that case, the argument must
	  #    directly follow the option as in -oarg (not -o arg).
	  #  - there must be an empty argument to mark the end of the option
	  #    specification.
	  #  - long options starting with "--" are supported.
	  #
	  # Differences with GNU getopt_long(3):
	  #  - getopts_long doesn't allow options to be interspersed with other
	  #    arguments (as if POSIXLY_CORRECT was set for GNU getopt_long(3))
	  #  - there's no linkage of any sort between the short and long
	  #    options. The caller is responsible of that (see example below).
	  #
	  # Compatibility:
	  #  getopts_long code is (hopefully) POSIX.2/SUSv3 compliant. It won't
	  #  work with the Bourne/SystemV shell. Use /usr/xpg4/bin/sh or ksh or
	  #  bash on Solaris.
	  #  It has been tested successfully with:
	  #    - bash 3.0 (patch level 16) on Cygwin
	  #    - zsh 4.2.4 on Solaris 2.7
	  #    - /usr/xpg4/bin/sh (same as /usr/bin/ksh) (ksh88i) on Solaris 2.7
	  #    - /usr/dt/bin/dtksh (ksh93d) on Solaris 2.7
	  #    - /usr/bin/ksh (pdksh 5.2.14) on Linux
	  #    - zsh 3.0.6 on Solaris 2.8
	  #    - bash 2.0.3 on Solaris 2.8
	  #    - dash 0.5.2 on Linux
	  #    - bash 2.05b (patch level 0) on Linux
	  #    - ksh93p and ksh93q on Linux (ksh93t+ crashes)
	  #
	  #  It is known to fail with those non-POSIX compliant shells:
	  #    - /bin/sh on Solaris
	  #    - /usr/bin/sh on Cygwin
	  #    - bash 1.x
	  #
	  # Bugs:
	  #  please report them to <stephane_chazelas@yahoo.fr>
	  #
	  # Example:
	  #
	  # verbose=false opt_bar=false bar=default_bar foo=default_foo
	  # opt_s=false opt_long=false
	  # OPTLIND=1
	  # while getopts_long :sf:b::vh opt \
	  #   long 0 \
	  #   foo required_argument \
	  #   bar 2 \
	  #   verbose no_argument \
	  #   help 0 "" "$@"
	  # do
	  #   case "$opt" in
	  #     s) opt_s=true;;
	  #     long) opt_long=true;;
	  #     v|verbose) verbose=true;;
	  #     h|help) usage; exit 0;;
	  #     f|foo) foo=$OPTLARG;;
	  #     b|bar) bar=${OPTLARG-$bar};;
	  #     :) printf >&2 '%s: %s\n' "${0##*/}" "$OPTLERR"
	  #        usage
	  #        exit 1;;
	  #   esac
	  # done
	  # shift "$(($OPTLIND - 1))"
	  # # process the remaining arguments

	  [ -n "${ZSH_VERSION+z}" ] && emulate -L sh

	  unset OPTLERR OPTLARG || :

	  case "$OPTLIND" in
		"" | 0 | *[!0-9]*)
		  # First time in the loop. Initialise the parameters.
		  OPTLIND=1
		  OPTLPENDING=
		  ;;
	  esac

	  if [ "$#" -lt 2 ]; then
		printf >&2 'getopts_long: not enough arguments\n'
		return 1
	  fi

	  # validate variable name. Need to fix locale for character ranges.
	  LC_ALL=C command eval '
		case "$2" in
		  *[!a-zA-Z_0-9]*|""|[0-9]*)
		printf >&2 "getopts_long: invalid variable name: \`%s'\''\n" "$2"
		return 1
		;;
		esac'

	  # validate short option specification
	  case "$1" in
		::*|*:::*|*-*)
		  printf >&2 "getopts_long: invalid option specification: \`%s'\n" "$1"
		  return 1
		  ;;
	  esac

	  # validate long option specifications

	  # POSIX shells only have $1, $2... as local variables, hence the
	  # extensive use of "set" in that function.

	  set 4 "$@"
	  while :; do
		if
		  [ "$1" -gt "$#" ] || {
		eval 'set -- "${'"$1"'}" "$@"'
		[ -n "$1" ] || break
		[ "$(($2 + 2))" -gt "$#" ]
		  }
		then
		  printf >&2 "getopts_long: long option specifications must end in an empty argument\n"
		  return 1
		fi
		eval 'set -- "${'"$(($2 + 2))"'}" "$@"'
		# $1 = type, $2 = name, $3 = $@
		case "$2" in
		  *=*)
		printf >&2 "getopts_long: invalid long option name: \`%s'\n" "$2"
		return 1
		;;
		esac
		case "$1" in
		  0 | no_argument) ;;
		  1 | required_argument) ;;
		  2 | optional_argument) ;;
		  *)
		printf >&2 "getopts_long: invalid long option type: \`%s'\n" "$1"
		return 1
		;;
		esac
		eval "shift 3; set $(($3 + 2))"' "$@"'
	  done
	  shift

	  eval "shift; set $(($1 + $OPTLIND))"' "$@"'

	  # unless there are pending short options to be processed (in
	  # $OPTLPENDING), the current option is now in ${$1}

	  if [ -z "$OPTLPENDING" ]; then
		[ "$1" -le "$#" ] || return 1
		eval 'set -- "${'"$1"'}" "$@"'

		case "$1" in
		  --)
			OPTLIND=$(($OPTLIND + 1))
		return 1
		;;
		  --*)
			OPTLIND=$(($OPTLIND + 1))
			;;
		  -?*)
			OPTLPENDING="${1#-}"
		shift
		;;
		  *)
			return 1
		;;
		esac
	  fi

	  if [ -n "$OPTLPENDING" ]; then
		# WA for zsh and bash 2.03 bugs:
		OPTLARG=${OPTLPENDING%"${OPTLPENDING#?}"}
		set -- "$OPTLARG" "$@"
		OPTLPENDING="${OPTLPENDING#?}"
		unset OPTLARG

		# $1 = current option = ${$2+1}, $3 = $@

		[ -n "$OPTLPENDING" ] ||
		  OPTLIND=$(($OPTLIND + 1))

		case "$1" in
		  [-:])
		OPTLERR="bad option: \`-$1'"
		case "$3" in
		  :*)
			eval "$4=:"
			OPTLARG="$1"
			;;
		  *)
			printf >&2 '%s\n' "$OPTLERR"
			eval "$4='?'"
			;;
		esac
		;;

		  *)
		case "$3" in
		  *"$1"::*) # optional argument
			eval "$4=\"\$1\""
			if [ -n "$OPTLPENDING" ]; then
			  # take the argument from $OPTLPENDING if any
			  OPTLARG="$OPTLPENDING"
			  OPTLPENDING=
			  OPTLIND=$(($OPTLIND + 1))
			fi
			;;

		  *"$1":*) # required argument
			if [ -n "$OPTLPENDING" ]; then
			  # take the argument from $OPTLPENDING if any
			  OPTLARG="$OPTLPENDING"
			  eval "$4=\"\$1\""
			  OPTLPENDING=
			  OPTLIND=$(($OPTLIND + 1))
			else
			  # take the argument from the next argument
			  if [ "$(($2 + 2))" -gt "$#" ]; then
			OPTLERR="option \`-$1' requires an argument"
			case "$3" in
			  :*)
				eval "$4=:"
				OPTLARG="$1"
				;;
			  *)
				printf >&2 '%s\n' "$OPTLERR"
				eval "$4='?'"
				;;
			esac
			  else
			OPTLIND=$(($OPTLIND + 1))
			eval "OPTLARG=\"\${$(($2 + 2))}\""
			eval "$4=\"\$1\""
			  fi
			fi
			;;

		  *"$1"*) # no argument
			eval "$4=\"\$1\""
			;;
		  *)
			OPTLERR="bad option: \`-$1'"
			case "$3" in
			  :*)
			eval "$4=:"
			OPTLARG="$1"
			;;
			  *)
			printf >&2 '%s\n' "$OPTLERR"
			eval "$4='?'"
			;;
			esac
			;;
		esac
		;;
		esac
	  else # long option

		# remove the leading "--"
		OPTLPENDING="$1"
		shift
		set 6 "${OPTLPENDING#--}" "$@"
		OPTLPENDING=

		while
		  eval 'set -- "${'"$1"'}" "$@"'
		  [ -n "$1" ]
		do
		  # $1 = option name = ${$2+1}, $3 => given option = ${$4+3}, $5 = $@

		  case "${3%%=*}" in
		"$1")
		  OPTLPENDING=EXACT
		  break;;
		  esac

		  # try to see if the current option can be seen as an abbreviation.
		  case "$1" in
		"${3%%=*}"*)
		  if [ -n "$OPTLPENDING" ]; then
			[ "$OPTLPENDING" = AMBIGUOUS ] || eval '[ "${'"$(($OPTLPENDING + 1))"'}" = "$1" ]' ||
			  OPTLPENDING=AMBIGUOUS
			  # there was another different option matching the current
			  # option. The eval thing is in case one option is provided
			  # twice in the specifications which is OK as per the
			  # documentation above
		  else
			OPTLPENDING="$2"
		  fi
		  ;;
		  esac
		  eval "shift 2; set $(($2 + 2)) "'"$@"'
		done

		case "$OPTLPENDING" in
		  AMBIGUOUS)
		OPTLERR="option \`--${3%%=*}' is ambiguous"
		case "$5" in
		  :*)
			eval "$6=:"
			OPTLARG="${3%%=*}"
			;;
		  *)
			printf >&2 '%s\n' "$OPTLERR"
			eval "$6='?'"
			;;
		esac
		OPTLPENDING=
		return 0
		;;
		  EXACT)
			eval 'set "${'"$(($2 + 2))"'}" "$@"'
		;;
		  "")
		OPTLERR="bad option: \`--${3%%=*}'"
		case "$5" in
		  :*)
			eval "$6=:"
			OPTLARG="${3%%=*}"
			;;
		  *)
			printf >&2 '%s\n' "$OPTLERR"
			eval "$6='?'"
			;;
		esac
		OPTLPENDING=
		return 0
		;;
		  *)
			# we've got an abbreviated long option.
		shift
			eval 'set "${'"$(($OPTLPENDING + 1))"'}" "${'"$OPTLPENDING"'}" "$@"'
		;;
		esac

		OPTLPENDING=

		# $1 = option type, $2 = option name, $3 unused,
		# $4 = given option = ${$5+4}, $6 = $@

		case "$4" in
		  *=*)
		case "$1" in
		  1 | required_argument | 2 | optional_argument)
			eval "$7=\"\$2\""
			OPTLARG="${4#*=}"
			;;
		  *)
			OPTLERR="option \`--$2' doesn't allow an argument"
			case "$6" in
			  :*)
			eval "$7=:"
			OPTLARG="$2"
			;;
			  *)
			printf >&2 '%s\n' "$OPTLERR"
			eval "$7='?'"
			;;
			esac
			;;
		esac
		;;

		  *)
			case "$1" in
		  1 | required_argument)
			if [ "$(($5 + 5))" -gt "$#" ]; then
			  OPTLERR="option \`--$2' requires an argument"
			  case "$6" in
			:*)
			  eval "$7=:"
			  OPTLARG="$2"
			  ;;
			*)
			  printf >&2 '%s\n' "$OPTLERR"
			  eval "$7='?'"
			  ;;
			  esac
			else
			  OPTLIND=$(($OPTLIND + 1))
			  eval "OPTLARG=\"\${$(($5 + 5))}\""
			  eval "$7=\"\$2\""
			fi
			;;
		  *)
			# optional argument (but obviously not provided) or no
			# argument
			eval "$7=\"\$2\""
			;;
		esac
		;;
		esac
	  fi
	  return 0
	}

fi



function die()
{
	>&2 echo "$@"
	exit 1
}


function logv()
{
	if [ "$opt_verbose" == true ]; then
		echo "$@"
	fi
}

function logd()
{
	if [ "$opt_debug" == true ]; then
		echo "$@"
	fi
}


function usage()
{
echo "Usage: $scriptname"
cat <<EOF

For each commit in a source branch that is not in the target 
branch, make sure that the commit has been cherry-picked into 
the target branch.  if not, display the commit so that
situation can be verified as correct.

By default this script considers commits with the same Change-Id
trailer to be the same, even if the commits have different diff
contents.  You can disable this behavior with --no-change-id

This script assumes that the source branch is the current branch.

Alternatively the source branch can be passed in the command line.

The following command line options are supported:

    -s, --source=NAME   source branch for commits (default: current branch)
    -t, --target=NAME   target branch for commits
        --swap          swwap the source and target branches

    -r, --repo=PATH     path to the repository (current dir by default)

        --missing-only  only display commits missing in the target (default)
        --no-missing-only display both missing and matching commits
        --show-all      synonym for --no-missing-only

        --no-change-id  don't consider Change-Id trailer as a match (only
                            use the actual contents of the diff)

    -v, --verbose       verbose messages (ie., headers)
        --debug         debug messages
    -h, --help          this help message

EOF
}


function parse_options()
{
	opt_change_id=true
	opt_missing_only=true
	opt_verbose=false
	opt_debug=false
	opt_source=""
	opt_target=""
	opt_swap="false"
	opt_repo=""
	OPTLIND=1
	while getopts_long :c:p:s:t:r:vh opt \
	source required_argument \
	target required_argument \
	child required_argument \
	parent required_argument \
	swap no_argument \
	repo required_argument \
	missing-only no_argument \
	no-missing-only no_argument \
	show-all no_argument \
	no-change-id no_argument \
	verbose no_argument \
	debug no_argument \
	help 0 "" "$@"
	do
	case "$opt" in
		# handle obsolete arguments
		c|child)
			>&2 echo "The --child (-c) option is obsolete. Use --source instead"
			opt_source="$OPTLARG"
			;;
		p|parent)
			>&2 echo "The --parent (-p) option is obsolete. Use --target instead"
			opt_target="$OPTLARG"
			;;
		v|verbose) opt_verbose=true;;
		debug) opt_debug=true;;
		h|help) usage; exit 0;;

		missing-only)    opt_missing_only=true;;
		no-missing-only) opt_missing_only=false;;
		show-all)        opt_missing_only=false;;

		no-change-id) opt_change_id=false;;
		s|source) opt_source="$OPTLARG";;
		t|target) opt_target="$OPTLARG";;
		swap) opt_swap=true;;

# 		r|repo)   opt_repo="$OPTLARG";;

		:) printf >&2 '%s: %s\n' "${0##*/}" "$OPTLERR"
		usage
		exit 1;;
	esac
	done
	shift "$(($OPTLIND - 1))"
	# process the remaining arguments
}


function validate_options()
{
	# target must be specified
	[ -z "$opt_target" ] &&  die "target branch must be specified (-t or --target)"

	[ -z "$opt_source" ] && opt_source="$(git symbolic-ref --short HEAD)"
	[ -z "$opt_source" ] && die "cannot find source branch"

	[ -z "$opt_repo" ] && opt_repo="$(git rev-parse --show-toplevel)"
}




# the git commands used:

function git_l()
{
	git log -n 1 --date=short-local --pretty=format:"$delimiter %<(8)%h $delimiter %Cgreen%<(12,trunc)%an  $delimiter  %ad  $delimiter %s%Creset" "$@"

	printf "\n" ""
}




function main()
{
	parse_options "$@"
	validate_options

	logd "opt_change_id = $opt_change_id"
	logd "opt_missing_only = $opt_missing_only"
	logd "opt_source = $opt_source"
	logd "opt_target = $opt_target"
	logd "opt_swap = $opt_swap"
	logd "opt_repo = $opt_repo"
	logd "opt_verbose = $opt_verbose"

	# swap the source and target branches if asked to
	if [ "$opt_swap" = "true" ] ; then
        logd "*** swapping source and target"

        local tmp="$opt_target"
        opt_target="$opt_source"
        opt_source="$tmp"

        logd "after_swap: opt_source = $opt_source"
        logd "after_swap: opt_target = $opt_target"
	fi

	# figure out a reasonable truncated set of branch names for display
	field_len=18
	trunc_target="$opt_target"
	trunc_source="$opt_source"
	if [ ${#trunc_source} -gt $field_len ] || [ ${#trunc_target} -gt $field_len ]; then
		# a branch name (or both) is longer than the maxiumum field size
		# so truncate them (with ellipsis on the truncated ones)
		trunc_target=$(truncate_ellipsis "$opt_target" $field_len)
		trunc_source=$(truncate_ellipsis "$opt_source" $field_len)
	else
		# both branch names will fit in the max field size, so adjust it down
		# so the field length is exactly the size of the longer branch name
		field_len=$(( ${#trunc_source} > ${#trunc_target} ? ${#trunc_source} : ${#trunc_target} ))
	fi

	logd "trunc_source = $trunc_source"
	logd "trunc_target = $trunc_target"

	local OPT_PROCESS_BINARY_FILES=true
	local BINARY_DIFF
	if [ "$OPT_PROCESS_BINARY_FILES" == "false" ] ; then
		BINARY_DIFF=""
	else
		BINARY_DIFF="--binary"
	fi
	logd "OPT_PROCESS_BINARY_FILES=$OPT_PROCESS_BINARY_FILES"
	logd "BINARY_DIFF=$BINARY_DIFF"


	# get a list of patch ID's for the commits in the target branch that aren't in the source
	local PATCH_IDS_TARGET=$(git log --patch $BINARY_DIFF --no-merges $opt_target ^$opt_source | git patch-id)
	logd "PATCH_IDS_TARGET=$PATCH_IDS_TARGET"
	logd ""
	logd ""

	# get a list of Change-Id's for the commits in the target branch that aren't in the source
	#
	# each line in the list is a commitid/change-id - only for commits with a Change-Id
	#   (separated by a literal '/' character)
	#
	# an example:
	#
	#   ae1151e0b1f2beb8419444aaf2c2b211eb92c3ef/Ic9ee102e0bba0e3e63de969b8dd7a3efae39eb64
	#
	local CHANGE_IDS_TARGET=$(git log --pretty=format:"%H/%(trailers:key=Change-Id,only=true,valueonly=true)" --no-merges $opt_target ^$opt_source)
	logd "CHANGE_IDS_TARGET=$CHANGE_IDS_TARGET"
	logd ""
	logd ""

	if [ "$opt_verbose" = "true" ]; then
        if [ "$opt_missing_only" = "true" ]; then
            printf "commits in %s not in %s\n" "$opt_source" "$opt_target"
            printf -- "----------------\n"
        else
            printf "commits in %s that should be in %s\n" "$opt_source" "$opt_target"
            printf -- "----------------\n"
        fi
    fi

	# generate a list of commits that are in source branch, but not the target

	git rev-list --no-merges "^$opt_target" "$opt_source" | \
		while IFS= read -r commit_id || [ -n "$commit_id" ]
		do
		if [ -n "$commit_id" ]; then
			local CHANGE_ID=
			local PATCH_ID=
			local MATCHED_COMMIT_ID=

			logd "     commit_id=$commit_id"

			CHANGE_ID=$(git log "$commit_id" | grep "Change-Id:" 2>/dev/null | head -1 | xargs -n 2 echo | cut --delim=" " -f 2)

			if [ "$opt_change_id" != "false"  ]; then
				# check if there is a matching Change-Id among the target commits
				# if so look up the corresponding commit-id
				MATCHED_COMMIT_ID=$(grep "$CHANGE_ID" <<< "$CHANGE_IDS_TARGET" 2>/dev/null| head -1 | cut --delim="/" -f 1)

				if [ -n "$MATCHED_COMMIT_ID" ]; then
					logd "     MATCHED_COMMIT_ID=$MATCHED_COMMIT_ID  (via Change-Id: $CHANGE_ID)"
				else
					logd "     (no commit found with Change-Id: $Change_Id)"
				fi
			fi

			if [ -z "$MATCHED_COMMIT_ID" ]; then
				logd "     Looking for matching patch..."
				PATCH_ID=$(git show $commit_id $BINARY_DIFF | git patch-id | head -1 | cut --delim=" " -f 1)
				MATCHED_COMMIT_ID=$(grep $PATCH_ID <<< "$PATCH_IDS_TARGET"| cut --delim=" " -f 2)
			fi

			if [ "$OPT_PROCESS_BINARY_FILES" == "false" ]; then
				# we are not processing commits with binary files
				# if a match is found but the patch includes a binary file difference, then
				# the match is unreliable because `git patch-id` doesn't get information about
				# *what* changed in the binary file - so assume there is no match

				if git show "$commit_id" | grep -q "^Binary files.*differ$" ; then
					logd "     Removing match because of binary files: $MATCHED_COMMIT_ID"
					MATCHED_COMMIT_ID=""
				fi
			fi

			logd "     PATCH_ID=$PATCH_ID"
			logd "     MATCHED_COMMIT_ID=$MATCHED_COMMIT_ID"

			if [ ! -z "$MATCHED_COMMIT_ID" ]; then
				# found a match
				if [ "$opt_missing_only" != "true" ]; then
					printf "%-${field_len}.${field_len}s: %s\n" "$trunc_source" "$(trim "$(git_l $commit_id)")"
					printf "%-${field_len}.${field_len}s: %s\n" "$trunc_target" "$(trim "$(git_l $MATCHED_COMMIT_ID)")"
					printf "\n" ""
				fi
			else
				# no match...
				if [ "$opt_missing_only" != "true" ]; then
					printf "\n" ""		       # extra blank line to make no-matches stand out better
					printf "%s" "missing: "
				fi

				local binary_differs=""
				if [ "$OPT_PROCESS_BINARY_FILES" = "false" ]; then
					binary_differs=" (Binary files differ - not checking for match)"
				fi

				local indent="    "
				printf "${indent}%s%s\n" "$(trim "$(git_l $commit_id)")" "$binary_differs"
				# printf "%-${field_len}.${field_len}s: %s%s\n" "$trunc_source" "$(trim "$(git_l $commit_id)")" "$binary_differs"
				# printf "%-${field_len}.${field_len}s: %s\n" "$trunc_target" "*** NO MATCH ***"
				# printf "\n" ""
			fi
		fi
		done 
}



main "$@"
