#! /bin/bash

# install some basic configuration and scripts to a Px2 console

#! /bin/bash

# debug can contain one or more of the following tokens to enable
# certain debugging operations:
#
#   log,  keeptmp

# set some defaults that can be overridden via environment variable settings
opt_debug=${opt_debug:-""}
opt_verbose=${opt_verbose:-"true"}

script="$(basename "$0")"
scriptdir="$(dirname "$0")"

# IP address or DNS name of console to install the configuration
dest_ip=${1:-"burr-p82"}

# srcdir:  directory to find the files that will be installed on the console
#           default is the directory this script is running from
srcdir="${srcdir:-$scriptdir}"


function die()
{
    >&2 echo "$@"
    exit 1
}


function logv()
{
    if [ "$opt_verbose" == true ]; then
            echo "$@"
    fi
}

function logd()
{
    if [[ "$opt_debug" =~ log ]]; then
            echo "$@"
    fi
}


function logd_var()
{
    local var_name="$1"

    logd "$var_name:" "${!var_name}"
}



logd_var scriptdir
logd_var dest_ip
logd_var srcdir
logd ""


do_ssh()
{
    local cmd="$1"

    logv ssh root@"$dest_ip" "$cmd"
    2>/dev/null ssh -o StrictHostKeyChecking=no root@"$dest_ip" "$cmd"
}


do_scp()
{
    local srcfile="$1"
    local destfile="$2"

    logv scp "${srcfile}" "root@${dest_ip}:${destfile}"
    2>/dev/null scp -o StrictHostKeyChecking=no "${srcfile}" "root@${dest_ip}:${destfile}"
}


file_list="mkshrc.extra ctap-disable.sh ctap-enable.sh ctap-query.sh p82-key.sh"
link_list="ctap-enable.sh ctap-query.sh"

do_ssh 'mkdir -p /data/home/mburr /data/bin'

# copy only items that are files, but not symlinks
for fname in $file_list ; do
    [ -f "$srcdir/$fname" ] &&  ! [ -L "$srcdir/$fname" ] && do_scp "$srcdir/$fname" "/data/bin/"
done

# now create  symlink for those items that are symlinks
for fname in $link_list; do
    [ -L "srcdir/$fname" ] && do_ssh "rm /data/bin/$fname 2>/dev/null ; ln -s ctap-disable.sh /data/bin/$fname"
done

