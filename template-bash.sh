#!/usr/bin/env bash

########
#
# a basic bash script template taken from https://dev.to/thiht/shell-scripts-matter
#
# some other reslurces for script template ideas:
# 
# Blogs about shell script tamplates that have useful info:
# 
#   - [Shell script templates](https://stackoverflow.com/q/430078/12711)
#   - [Shell script common template](https://stackoverflow.com/q/14008125/12711)
#   - [Shell Scripts Matter: Thibaut Rousseau](https://dev.to/thiht/shell-scripts-matter)
#   - [Improve your bash scripts with this boilerplate template](https://www.nicksantamaria.net/post/boilerplate-bash-script/)


#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'


#/ Usage:
#/ Description:
#/ Examples:
#/ Options:
#/   --help: Display this help message
usage() { grep '^#/' "$0" | cut -c4- ; exit 0 ; }
expr "$*" : ".*--help" > /dev/null && usage

readonly LOG_FILE="/tmp/$(basename "$0").log"
info()    { echo "[INFO]    $*" | tee -a "$LOG_FILE" >&2 ; }
warning() { echo "[WARNING] $*" | tee -a "$LOG_FILE" >&2 ; }
error()   { echo "[ERROR]   $*" | tee -a "$LOG_FILE" >&2 ; }
fatal()   { echo "[FATAL]   $*" | tee -a "$LOG_FILE" >&2 ; exit 1 ; }

cleanup() {
    # Remove temporary files
    # Restart services
    # ...
}

if [[ "${BASH_SOURCE[0]}" = "$0" ]]; then
    trap cleanup EXIT
    # Script goes here
    # ...
fi

