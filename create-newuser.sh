# create a new user account with some decent defaults

userid="$1"


loge()
{
    1>&2 echo "$@"
}


if [ "$userid" = "" ] ; then
    loge "error: must provide a user name"
    exit 1
fi

if id "$userid" >/dev/null 2>&1 ; then
    loge "error: $userid already exists"
    exit 1
fi

default_passwd=$(openssl passwd -6 "password")
default_groups="dialout,sudo"
default_shell="/bin/bash"


# find get the next UID/GID in the range 1000-1999
#   this can be used to help keep a user's UID and default GID the same
#   taken from: https://www.kevinguay.com/posts/next-uid-gid/

uid=$(cat /etc/group /etc/passwd | cut -d ":" -f 3 | grep "^1...$" | sort -n | tail -n 1 | awk '{ print $1+1 }')

# add the group with that gid (if the group already exists, the
#   `-f` option supresses and error and leave the group's
#   gid unchanged)
sudo groupadd -g "$uid" "$userid"
gid=$(getent group "$userid" | cut -d: -f3)     # getent returns an entry like "userid:x:1234:"



sudo useradd -m $userid --uid="$uid" --gid="$gid" -p "$default_passwd" --groups "$default_groups" --shell "$default_shell"

# suppress the message about using sudo to run things as root
no_sudo_msg="/home/$userid/.sudo_as_admin_successful"
sudo su -c "touch $no_sudo_msg" "$userid"
