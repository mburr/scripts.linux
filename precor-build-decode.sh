#! /bin/bash

# The P82/P62 build number (which is basically a timestamp) is created in:
#
#       ./device/precor/var-som/mx6q/versions.mk:
#
#
#           # Determine the revision assigned to all the component versions which are built at this time
#           PRECOR_BUILD_VERSION := $(shell echo "0 k $(shell date --utc +%s) 1388534400 - 60 / p" | dc)
#

PRECOR_TIME=$1

# 1388534400  is the Unix timestamp for 1 Jan 2014 (UTC)
#
# The precor epoch will change to 1 Jan 2020 (UTC) for nextgen: 1577836800
#
# TODO: make this script handle either precor epoch:
#
# 		  - sensible default with option to override?
#		  - option to display both?
#
# the general algorithm is to:
#
#    - $PRECOR_TIME * 60        // convert the Precor timestamp from minutes to seconds
#    - add 1388534400 to convert from minutes since 1 Jan 2014 to 1 Jan 1970 (convert
#        back to the Unix epoch)
#
# note: the '@' sign here isn't anything to do with a bash operator; it indicates
#       the `date -d` argument is a Unix epoch decimal number.

EPOCH_2014=1388534400
EPOCH_2021=1577836800

printf "epoch 2014: %s\n" "$(date -d "@$(( PRECOR_TIME * 60 + EPOCH_2014 ))")"
printf "epoch 2021: %s\n" "$(date -d "@$(( PRECOR_TIME * 60 + EPOCH_2021 ))")"
