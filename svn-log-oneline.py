import datetime
import io
import zoneinfo
import sys
import xml.etree.ElementTree as ET
import xml.parsers.expat.errors as expat_errors

import tzdata       # <== needs to be installed using `pip install tzdata`
LOCAL_TZ = datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo


def notNone( exp):
    return exp is not None


# class IgnoreBrokenPipe helps solve a
# problem where python will give an
# OSError: [Errno 22] Invalid argument
#
# when a pipe being used is closed unexpectedly
#
# See https://stackoverflow.com/a/66874837/12711
# for details, becuase I sure as hell don't
# understand what's going on...

class IgnoreBrokenPipe(object):
    def __init__(self, stream):
        self.stream = stream
        def ignore_einval(fn):
            from functools import wraps
            @wraps(fn)
            def wrapper(*args, **kwargs):
                try:
                    return fn(*args, **kwargs)
                except OSError as exc:
                    if exc.errno != 22:
                        raise exc
                    else:  # mimicking the default SIGPIPE behavior on Windows
                        pass
                        # sys.exit(1)
            return wrapper

        self.write = ignore_einval(lambda data: self.stream.write(data))
        self.flush = ignore_einval(lambda: self.stream.flush())


sys.stdout = IgnoreBrokenPipe(sys.stdout)





def truncate_with_ellipsis(str, max_len, ellipsis="..."):
    orig_len = len(str)

    # truncate the string
    str = str[:max_len]
    
    if max_len <= len(ellipsis):
        # max_len is too short to handle ellipsis
        return str

    if orig_len > max_len:
        # replace the last few characters with the ellipsis
        str = str[:-len(ellipsis)] + ellipsis

    return str


def process_logentry(logentry, indent_level):
    # logentry is expected to be an object of type
    # `xml.etree.ElementTree.Element` that corresponds
    # to the `<logentry>` XML element produced by subversion's
    # `svn log --xml` command
    #
    # An example of that XML element:
    #
    # <logentry revision="36662">
    #     <author>jsmith</author>
    #     <date>2022-02-05T09:15:02.131865Z</date>
    #     <paths>
    #          <path  kind="file" action="M">/atlas/CPA_Boot/trunk/CPA_Boot/rakefile_helper.rb</path>
    #     </paths>
    #     <msg>Some subversion commit message text</msg>
    # </logentry>
    #
    # (the `<paths>` element will only exist if the `--verbose` option
    # is given to `svn log --xml`


    elt_author   = logentry.find('author')
    elt_date     = logentry.find('date')
    elt_msg      = logentry.find('msg')
    elt_logentry = logentry

    # use Python's "conditional expressions" to protect against missing attributes, elements, or element text
    revision = elt_logentry.attrib['revision']  or "(none)"
    author   = elt_author.text                 if (notNone(elt_author)  and notNone(elt_author.text)) else "(none)"
    xmldate  = elt_date.text                   if (notNone(elt_date)    and notNone(elt_date.text))   else "(none)"
    msg      = elt_msg.text.splitlines()[0]    if (notNone(elt_msg)     and notNone(elt_msg.text))    else "(none)"
    msg = "(none)"
    if (notNone(elt_msg) and notNone(elt_msg.text)):
        for msg_line in elt_msg.text.splitlines():
            msg_line = msg_line.strip()
            if (len(msg_line) != 0):
                msg = msg_line
                break

    
    # author is padded then truncated to 12 characters
    author = truncate_with_ellipsis(author.ljust(12), 12, "..")
    
    # SVN prints the date in XML as a ISO UTC value with a "Z" on the end
    # python's datetime.fromisoformat() doesn't like the trailing Z
    date = datetime.datetime.fromisoformat( xmldate.rstrip("Z"))
    
    # the date object is in UTC time, so tell it so
    date = date.replace(tzinfo=datetime.timezone.utc)
    
    # truncate msg if necessary
    msg = truncate_with_ellipsis(msg, 72)
    
    indent = " " * (indent_level - 1)
    line = f'{indent}r{revision} {author} {date.astimezone(LOCAL_TZ).strftime("%Y-%m-%d")}  {msg}'
    # print( f'DEBUG: output {revision}') #burr-debug
    return line
    



# This function processes the <logentry> XML elemnts
# written by svn as they come instead of waiting for the
# entire XML document to be written.
#
# That means that output will start to show before svn
# has written the entire XML log, so it's more "responsive"
# especially with logs that have a large number of commits.
#
# However, when piping output to the `head` command, this
# program will exit before svn has written all of the XML
# data because the `head` command will close the pipe
# it is reading from as soon as it reached the number of
# it has been asked to output (usually 10).
#
# Since that causes this program to exit, the pipe that
# svn is writing to will also close, which causes svn to
# display these error messages:
#
#   svn: E720232: Write error: The pipe is being closed.
#   svn: E200042: Additional errors:
#   svn: E135007: Write error in pipe
#
# Those messages are actually a bug in svn, which has been
# fixed on Linux, but apparently not on Windows (see
# https://issues.apache.org/jira/browse/SVN-3014?issueNumber=3014)
#
def process_with_events(infile):

    # XML exceptions need to be caught and handled by the caller
    xml_iter = ET.iterparse(infile, events=('start', 'end'))

    # logentry_level will track when we are handling 
    # nested logentry's (which represent merged commits)
    #
    # this will allow us to properly display merged
    # commits when --use-merge-history is specified
    
    output_strings = []     # a stack of strings - each level gets all the lines of output for it's level
    logentry_level = 0      # "start" events increment
                            # "end" events decrement
                            # When an "end" event results 
                            # in loglevel_entry == 0, then 
                            # it's time to pop & print the
                            # output_stack


    
    for event, elem in xml_iter:
        if elem.tag != 'logentry':
            # we're only interested in logevent elements
            continue
            
        if event == 'start':
            logentry_level += 1
            # print( f'DEBUG: start logentry (level = {logentry_level})') #burr-debug
            if len( output_strings) < logentry_level:
                output_strings.append( [logentry_level, ""] )
            # print( f'DEBUG: items on output_strings list: {len(output_strings)}') #burr-debug            
            
        if event == 'end':
            # print( f'DEBUG: end logentry (level = {logentry_level})') #burr-debug
            # print( f'DEBUG: items on output_strings list: {len(output_strings)}') #burr-debug
            output = process_logentry(elem, logentry_level)

            curr_out_level = output_strings.pop()

            if logentry_level != curr_out_level[0]:
                # popped an output level higher than this one
                # concatenate it to the new line of output and
                # add it to the current level's output
                output = output  + curr_out_level[1]
                curr_out_level = output_strings.pop()

            # writing a commit entry that is at the same "merge level"
            # append this to current output and push back
            new_string = output
            if logentry_level > 1:
                new_string = curr_out_level[1] + "\n" + output

            output_strings.append( [logentry_level, new_string] )
                
            # print( "\n", "DEBUG: new_string = ", new_string) #burr-debug
            # print( "DEBUG: output_strings = ", output_strings) #burr-debug
            
            logentry_level -= 1
            if logentry_level == 0:
                line = output_strings.pop()
                print( line[1])
            










# This function first reads all of the svn XML output
# then processes it.  That causes a potentially large
# delay before seeing any output at all.
#
# However, it plays nicer when piping to the `head` command
# because svn is done writing everything to stdout (this
# program's stdin) before `head` gets anything, so when
# this program terminates because `head` has closed it's pipe,
# svn won't complain about it's pipe getting closed too.
#
# So there's no messages of this kind:
#
#   svn: E720232: Write error: The pipe is being closed.
#   svn: E200042: Additional errors:
#   svn: E135007: Write error in pipe
#
def process_with_full_doc(infile):

    ### NOTE: this function has not been updated to support --use-merge-history yet!!

    # parse the `svn log --xml` output
    # XML exceptions need to be caught and handled by the caller
    et  = ET.parse(infile)
    log = et.getroot()

    for elt_logentry in log:
        process_logentry( elt_logentry)






def main():
    try:
        # interpret stdin as UTF-8 ( which is what svn log --xml outputs) instead of
        #    ANSI (which is what sys.stdin claims it is)
        stdin_utf8 = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')

        if True:
            process_with_events(stdin_utf8)     # process each <logentry> element as it gets read
        else:
            process_with_full_doc(stdin_utf8)   # read entire XML doc before starting to preocess <logentry> elements

    except ET.ParseError as parse_error:
        if (parse_error.code == 3):
            # the stdin stream was empty (there was no XML document), so don't output anything
            exit(1)

        print( "XML parsing failed - the input stream isn't well-formed XML")
        print( "It might be corrupt, non-existant, or you might need to use the svn log --xml option")

        (line, col) = parse_error.position
        print( f'error details:')
        print( f'    expat_error {parse_error.code}: {expat_errors.messages[parse_error.code]}')
        print( f'    xml.etree.ElementTree.ParseError on line:col {line}:{col} (expat error code: {parse_error.code})')
        exit( 1)

    except BrokenPipeError as pipe_error:
        # this happens when the output is being piped to something that
        # isn't interested in all of the output.  For example, the
        # `head` command will close the pipe after it gets 10 lines (or
        # however many it is supposed to display)
        #
        # This situation isn't really an error (or it's an error in
        # whatever is reading the pipe)
        pass

    exit(0)


main()
