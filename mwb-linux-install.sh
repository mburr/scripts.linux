#! /bin/bash




is_debug()
{
    if [[ -z "$opt_debug" || "$opt_debug" == "0" || "$opt_debug" == "false" ]]; then
        return 1
    fi
    
    return 0
}


loge()
{
    # write to stderr
    1>&2 echo "$@"
}


logd()
{
    if is_debug; then
        loge "$@"
    fi
    
    return 0
}

die()
{
    loge "$@"
    exit 1
}


get_ubuntu_codename()
{
    # get the Ubuntu codename for this Linux install - even if it's
    #   Linux Mint (based on Ubuntu)

    # find out which Ubuntu release codename this install is using
    local UBUNTU_CODENAME
    UBUNTU_CODENAME=$(lsb_release -cs)

    # if we are on Linux Mint, the above gave us Mint's codename, not the
    #   codename for the Ubuntu it is based on.
    #   Look in /etc/os-release for the Ubuntu codename if we're on Linux Mint
    if (2>/dev/null lsb_release --id |grep -q Linuxmint); then
        # it's Linux Mint, get the equivalent Ubuntu release from /etc/so-release
        logd "Mint codename=$UBUNTU_CODENAME"

        UBUNTU_CODENAME="$(grep -Po  '(?<=^UBUNTU_CODENAME=).*$' /etc/os-release)"
    fi

    logd "UBUNTU_CODENAME=$UBUNTU_CODENAME"
    
    echo "$UBUNTU_CODENAME"
}



install_gedit()
{
    sudo apt install gedit -y
}


install_kate()
{
    sudo apt install kate -y
}



install_git()
{
    sudo apt install git -y
}

install_svn()
{
    sudo apt install subversion -y
}



install_docker()
{
    # much of this stuff comes from https://docs.docker.com/engine/install/ubuntu/
    
    # install docker pre-requisites:
    sudo apt-get install -y \
         aufs-tools \
         ca-certificates \
         curl \
         gnupg \
         lsb-release
         
    # add docker's GPG key
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor --yes -o /usr/share/keyrings/docker-archive-keyring.gpg
    
    # add docker's stable dpkg repository
    
    local UBUNTU_CODENAME="$(get_ubuntu_codename)"
    
    echo \
      "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
      $UBUNTU_CODENAME stable" | sudo tee /etc/apt/sources.list.d/docker.list
      
      #  >/dev/null
    
    # and finally install docker
    sudo apt update
    sudo apt install docker-ce docker-ce-cli containerd.io
    
    # group exist?
    [[ 0 -eq $(grep -c docker /etc/group) ]] && sudo groupadd docker

    # am I in the group?
    if [[ 0 -eq $(groups | grep -c docker) ]]; then
        sudo usermod -aG docker $USER
        
        # try to make the group active without logoff/logon
        su - $USER
    fi
        

    # add insecure registries for precor's situation
    # I can't remember why I need the "storage-driver": "aufs" config
    if [[ 0 -eq $(sudo grep -c insecure-registries /etc/docker/daemon.json) ]]; then
        sudo cp /etc/docker/daemon.json /etc/docker/daemon.json.bak 2>/dev/null
        echo '{
    "storage-driver": "aufs",
    "insecure-registries" : ["192.168.250.41:5000","192.168.250.41:5001"]
}'      | sudo tee -a /etc/docker/daemon.json
    fi

    logd Pausing for a bit becuase stopping/restarting docker is finicky
    sleep 3

    logd stopping docker...
    sudo systemctl stop docker

    logd Pausing for a bit again becuase stopping/restarting docker is finicky
    sleep 5
    logd starting docker...
    sudo systemctl start docker
}


install_open-vm-tools()
{
    sudo apt install open-vm-tools open-vm-tools-desktop -y
}



main()
{
    apps="$@"

    for app in $apps; do
        case "$app" in
            gedit)
                echo Installing "$app"
                install_gedit
                ;;

            kate)
                echo Installing "$app"
                install_kate
                ;;

            svn|subversion)
                echo Installing "$app"
                install_svn
                ;;

            git)
                echo Installing "$app"
                install_git
                ;;

            open-vm-tools|vmware-tools|vm-tools)
                install_open-vm-tools
                ;;
                
            docker)
                install_docker
                ;;
                
            *)
                echo Unknown app: "$app"
                ;;
        esac
    done

}




main "$@"

