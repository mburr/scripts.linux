#!/bin/bash

# script to generate a QR code from a string argument.
# Sal posted this in the slack #dev-tool-discussions channel
#
#   https://precor-xdc.slack.com/archives/C011NSUJVPZ/p1587256549010200
#
# needs imagemagik package installed

[[ $# -eq 0 ]] && {
  echo "usage: $(basename $0) <STRING>"
  exit
}

DOT_SIZE=10

[[ "$(xrandr | grep '*' | awk {'print $1'})" =~ ([0-9]+)x([0-9]+) ]] &&
  EXTENT=$(( ${BASH_REMATCH[2]} / 2 )) || EXTENT=640

SCALING=$(gsettings get org.gnome.desktop.interface scaling-factor | cut -d' ' -f2)

[[ "$SCALING" =~ [2-4]+ ]] && DOT_SIZE=$(( $DOT_SIZE * $SCALING ))

for s in "$@"; do
  qrencode -s $DOT_SIZE -o - $s | convert -background white -gravity center -extent ${EXTENT}x${EXTENT} - miff:-
done | convert miff:- +append - | display
