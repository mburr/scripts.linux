#! /bin/bash

# script to list  git repositories in a directory tree
#
# refer to https://unix.stackexchange.com/a/333918/7084
#
# TODOs:
#
#    - exclude current directory?  Especially for any future prune option?
#    - currently only works for current directory - allow a directory to be specified on command line
#    - add an option to "prune" - stop searching deeper when a repo is found (use find's `-prune` option)
#    - the usual verbose/debug logging teplate stuff?

find . -type d -exec test -e '{}/.git' ';' -print
