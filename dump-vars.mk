# GNU Make Makefile to dump all make vars
#
# Usage: make -f Makefile -f dump-vars.mak dump-vars
#
# from: https://www.cmcrossroads.com/article/dumping-every-makefile-variable
# by:   John Graham-Cumming

.PHONY: dump-vars
dump-vars:
	@$(foreach V,$(sort $(.VARIABLES)),
		$(if $(filter-out environment% default automatic,
		$(origin $V)),$(warning $V=$($V) ($(value $V)))))

