#! /bin/bash


script=$(basename "$0")


# if a ctap archive is specified, a temporary directory
# will be  used for extraction.  The name of that temp dir will
# be stored in this variable so it can be cleaned up at the 
# end of the script
tmpdir=""


usage()
{
cat <<EOF
${script}:  Unpack ctag.zip and copy the files to specified console

This script also enables ctap by setting CPA_PROXY_ADDRESS=localhost in
mpaqtapp's environment.txt.

    --target        IP address (or resolvable name) of the console to
                    install ctap.  Default is "burr-p82".

    --ctap          Path to a directory or archive containing the ctap
                    distribution.  If this specifies a directory, it's
                    assumed to be the unarchived ctap distribution.

                    If it points to a .zip archive, the archive will be
                    extracted to a temporary directory for processing.
                    The temporary directory will be removed whenthe script
                    completes (unless the --keep-temp option is used)

                    Default is "~/ctap".

    -n, --no-enable Do not enable ctap when finished

    -v, --verbose   extra logging
    --debug         debug logging
    -h, --help      print this help screen

DEPRECATED:
For legacy purposes, some options can be set with environment variables:

Use the following environment variables to change the defaults:

  ctap_zip: the full path to the ctap.zip file

  dest:     the IP address or DNS name of the console to install
            the ctap file to
EOF
}


function parse_options()
{
    opt_target="${dest:-burr-p82}"
    opt_ctap="${ctap_zip:-$HOME/ctap}"
    opt_keep_temp="0"
    opt_enable_ctap="1"
    opt_verbose="0"
    opt_debug="0"

    OPTLIND=1
    while getopts_long :nvdh opt      \
        target    required_argument \
        ctap      required_argument \
        keep-temp no_argument \
        no-enable no_argument \
        verbose   no_argument \
        debug     no_argument \
        help      no_argument \
        "" \
        "$@"

        do
        case "$opt" in
            target)
                opt_target="$OPTLARG"
                ;;
            ctap)
                opt_ctap="$OPTLARG"
                ;;
            keep-temp)
                opt_keep_temp="1"
                ;;
            no-enable)
                opt_enable_ctap="0"
                ;;
            v|verbose) opt_verbose=true;;
            debug) opt_debug=true;;
            h|help) usage; exit 0;;

            :) printf >&2 '%s: %s\n' "${0##*/}" "$OPTLERR"
            usage
            exit 1;;
        esac
    done

    shift "$(($OPTLIND - 1))"
    # process the remaining arguments
}


function validate_options()
{
    [ -z "$opt_target" ] &&  die "target console must be specified"
    [ -z "$opt_ctap" ]   &&  die "location of ctap distribution must be specified"
}




#
# getopts_long -- POSIX shell getopts with GNU-style long option support
#
# Copyright 2005-2009 Stephane Chazelas <stephane_chazelas@yahoo.fr>
#
# Permission to use, copy, modify, distribute, and sell this software and
# its documentation for any purpose is hereby granted without fee, provided
# that the above copyright notice appear in all copies and that both that
# copyright notice and this permission notice appear in supporting
# documentation.  No representations are made about the suitability of this
# software for any purpose.  It is provided "as is" without express or
# implied warranty.

# History:
# 2005 ?? - 1.0
#   first version
# 2009-08-12 - 1.1
#   thanks to ujqm8360@netwtc.net for helping fix a few bugs:
#     - -ab x where -b accepts arguments wasn't handled properly. Also,
#       $OPTLIND wasn't set properly (at least not the same way as most
#       getopts implementation do).
#     - The handling of ambiguous long options was incorrect.

getopts_long() {
  # args: shortopts, var, [name, type]*, "", "$@"
  #
  # getopts_long parses command line arguments. It works like the
  # getopts shell built-in command except that it also recognises long
  # options a la GNU.
  #
  # You must provide getopts_long with the list of supported single
  # letter options in the same format as getopts', followed by the
  # variable name you want getopts_long to return the current processed
  # option in, followed by the list of long option names (without the
  # leading "--") and types (0 or no_argument, 1 or required_argument,
  # 2 or optional_argument). The end of the long option specification
  # is indicated by an empty argument. Then follows the list of
  # arguments to be parsed.
  #
  # The $OPTLIND variable must be set to 1 before the first call of the
  # getopts_long function to process a given list of arguments.
  #
  # getopts_long returns the value of the current option in the variable
  # whose name is provided as its second argument (be careful to avoid
  # variables that have a special signification to getopts_long or the
  # shell or any other tool you may call from your script). If the
  # current option is a single letter option, then it is returned
  # without the leading "-". If it's a long option (possibly
  # abbreviated), then the full name of the option (without the leading
  # "--") is returned. If the option has an argument, then it is stored
  # in the $OPTLARG variable. If the current option is not recognised,
  # or if it is provided with an argument while it is not expecting one
  # (as in --opt=value) or if it is not provided with an argument while
  # it is expecting one, or if the option is so abbreviated that it is
  # impossible to identify the option uniquely, then:
  #   - if the short option specifications begin with ":", getopts_long
  #     returns ":" in the output variable and $OPTLARG contains the
  #     faulty option name (in full except in the case of the ambiguous
  #     or bad option) and $OPTLERR contains the error message.
  #   - if not, then getopts_long behaves the same as above except that
  #     it returns "?" instead of ":", leaves $OPTLARG unset and
  #     displays the error message on stderr.
  #
  # The exit status of getopts_long is 0 unless the end of options is
  # reached or an error is encountered in the syntax of the getopts_long
  # call.
  #
  # After getopts_long has finished processing the options, $OPTLIND
  # contains the index of the first non-option argument or $# + 1 if
  # there's no non-option argument.
  #
  # The "=" character is not allowed in a long option name. Any other
  # character is. "-" and ":" are not allowed as short option names. Any
  # other character is. If a short option appears more than once in the
  # specification, the one with the greatest number of ":"s following it
  # is retained. If a long option name is provided more than once, only
  # the first one is taken into account. Note that if you have both a -a
  # and --a option, there's no way to differentiate them. Beside the
  # $OPTLIND, $OPTLARG, and $OPTLERR, getopts_long uses the $OPTLPENDING
  # variable to hold the remaining options to be processed for arguments
  # with several one-letter options. That variable shouldn't be used
  # anywhere else in your script. Those 4 variables are the only ones
  # getopts_long may modify.
  #
  # Dependency: only POSIX utilities are called by that function. They
  # are "set", "unset", "shift", "break", "return", "eval", "command",
  # ":", "printf" and "[". Those are generally built in the POSIX
  # shells. Only "printf" has been known not to be in some old versions
  # of bash, zsh or ash based shells.
  #
  # Differences with the POSIX getopts:
  #  - if an error is detected during the parsing of command line
  #    arguments, the error message is stored in the $OPTLERR variable
  #    and if the first character of optstring is ':', ':' is returned in
  #    any case.
  #  - in the single-letter option specification, if a letter is
  #    followed by 2 colons ("::"), then the option can have an optional
  #    argument as in GNU getopt(3). In that case, the argument must
  #    directly follow the option as in -oarg (not -o arg).
  #  - there must be an empty argument to mark the end of the option
  #    specification.
  #  - long options starting with "--" are supported.
  #
  # Differences with GNU getopt_long(3):
  #  - getopts_long doesn't allow options to be interspersed with other
  #    arguments (as if POSIXLY_CORRECT was set for GNU getopt_long(3))
  #  - there's no linkage of any sort between the short and long
  #    options. The caller is responsible of that (see example below).
  #
  # Compatibility:
  #  getopts_long code is (hopefully) POSIX.2/SUSv3 compliant. It won't
  #  work with the Bourne/SystemV shell. Use /usr/xpg4/bin/sh or ksh or
  #  bash on Solaris.
  #  It has been tested successfully with:
  #    - bash 3.0 (patch level 16) on Cygwin
  #    - zsh 4.2.4 on Solaris 2.7
  #    - /usr/xpg4/bin/sh (same as /usr/bin/ksh) (ksh88i) on Solaris 2.7
  #    - /usr/dt/bin/dtksh (ksh93d) on Solaris 2.7
  #    - /usr/bin/ksh (pdksh 5.2.14) on Linux
  #    - zsh 3.0.6 on Solaris 2.8
  #    - bash 2.0.3 on Solaris 2.8
  #    - dash 0.5.2 on Linux
  #    - bash 2.05b (patch level 0) on Linux
  #    - ksh93p and ksh93q on Linux (ksh93t+ crashes)
  #
  #  It is known to fail with those non-POSIX compliant shells:
  #    - /bin/sh on Solaris
  #    - /usr/bin/sh on Cygwin
  #    - bash 1.x
  #
  # Bugs:
  #  please report them to <stephane_chazelas@yahoo.fr>
  #
  # Example:
  #
  # verbose=false opt_bar=false bar=default_bar foo=default_foo
  # opt_s=false opt_long=false
  # OPTLIND=1
  # while getopts_long :sf:b::vh opt \
  #   long 0 \
  #   foo required_argument \
  #   bar 2 \
  #   verbose no_argument \
  #   help 0 "" "$@"
  # do
  #   case "$opt" in
  #     s) opt_s=true;;
  #     long) opt_long=true;;
  #     v|verbose) verbose=true;;
  #     h|help) usage; exit 0;;
  #     f|foo) foo=$OPTLARG;;
  #     b|bar) bar=${OPTLARG-$bar};;
  #     :) printf >&2 '%s: %s\n' "${0##*/}" "$OPTLERR"
  #        usage
  #        exit 1;;
  #   esac
  # done
  # shift "$(($OPTLIND - 1))"
  # # process the remaining arguments

  [ -n "${ZSH_VERSION+z}" ] && emulate -L sh

  unset OPTLERR OPTLARG || :

  case "$OPTLIND" in
    "" | 0 | *[!0-9]*)
      # First time in the loop. Initialise the parameters.
      OPTLIND=1
      OPTLPENDING=
      ;;
  esac

  if [ "$#" -lt 2 ]; then
    printf >&2 'getopts_long: not enough arguments\n'
    return 1
  fi

  # validate variable name. Need to fix locale for character ranges.
  LC_ALL=C command eval '
    case "$2" in
      *[!a-zA-Z_0-9]*|""|[0-9]*)
    printf >&2 "getopts_long: invalid variable name: \`%s'\''\n" "$2"
    return 1
    ;;
    esac'

  # validate short option specification
  case "$1" in
    ::*|*:::*|*-*)
      printf >&2 "getopts_long: invalid option specification: \`%s'\n" "$1"
      return 1
      ;;
  esac

  # validate long option specifications

  # POSIX shells only have $1, $2... as local variables, hence the
  # extensive use of "set" in that function.

  set 4 "$@"
  while :; do
    if
      [ "$1" -gt "$#" ] || {
    eval 'set -- "${'"$1"'}" "$@"'
    [ -n "$1" ] || break
    [ "$(($2 + 2))" -gt "$#" ]
      }
    then
      printf >&2 "getopts_long: long option specifications must end in an empty argument\n"
      return 1
    fi
    eval 'set -- "${'"$(($2 + 2))"'}" "$@"'
    # $1 = type, $2 = name, $3 = $@
    case "$2" in
      *=*)
    printf >&2 "getopts_long: invalid long option name: \`%s'\n" "$2"
    return 1
    ;;
    esac
    case "$1" in
      0 | no_argument) ;;
      1 | required_argument) ;;
      2 | optional_argument) ;;
      *)
    printf >&2 "getopts_long: invalid long option type: \`%s'\n" "$1"
    return 1
    ;;
    esac
    eval "shift 3; set $(($3 + 2))"' "$@"'
  done
  shift

  eval "shift; set $(($1 + $OPTLIND))"' "$@"'

  # unless there are pending short options to be processed (in
  # $OPTLPENDING), the current option is now in ${$1}

  if [ -z "$OPTLPENDING" ]; then
    [ "$1" -le "$#" ] || return 1
    eval 'set -- "${'"$1"'}" "$@"'

    case "$1" in
      --)
        OPTLIND=$(($OPTLIND + 1))
    return 1
    ;;
      --*)
        OPTLIND=$(($OPTLIND + 1))
        ;;
      -?*)
        OPTLPENDING="${1#-}"
    shift
    ;;
      *)
        return 1
    ;;
    esac
  fi

  if [ -n "$OPTLPENDING" ]; then
    # WA for zsh and bash 2.03 bugs:
    OPTLARG=${OPTLPENDING%"${OPTLPENDING#?}"}
    set -- "$OPTLARG" "$@"
    OPTLPENDING="${OPTLPENDING#?}"
    unset OPTLARG

    # $1 = current option = ${$2+1}, $3 = $@

    [ -n "$OPTLPENDING" ] ||
      OPTLIND=$(($OPTLIND + 1))

    case "$1" in
      [-:])
    OPTLERR="bad option: \`-$1'"
    case "$3" in
      :*)
        eval "$4=:"
        OPTLARG="$1"
        ;;
      *)
        printf >&2 '%s\n' "$OPTLERR"
        eval "$4='?'"
        ;;
    esac
    ;;

      *)
    case "$3" in
      *"$1"::*) # optional argument
        eval "$4=\"\$1\""
        if [ -n "$OPTLPENDING" ]; then
          # take the argument from $OPTLPENDING if any
          OPTLARG="$OPTLPENDING"
          OPTLPENDING=
          OPTLIND=$(($OPTLIND + 1))
        fi
        ;;

      *"$1":*) # required argument
        if [ -n "$OPTLPENDING" ]; then
          # take the argument from $OPTLPENDING if any
          OPTLARG="$OPTLPENDING"
          eval "$4=\"\$1\""
          OPTLPENDING=
          OPTLIND=$(($OPTLIND + 1))
        else
          # take the argument from the next argument
          if [ "$(($2 + 2))" -gt "$#" ]; then
        OPTLERR="option \`-$1' requires an argument"
        case "$3" in
          :*)
            eval "$4=:"
            OPTLARG="$1"
            ;;
          *)
            printf >&2 '%s\n' "$OPTLERR"
            eval "$4='?'"
            ;;
        esac
          else
        OPTLIND=$(($OPTLIND + 1))
        eval "OPTLARG=\"\${$(($2 + 2))}\""
        eval "$4=\"\$1\""
          fi
        fi
        ;;

      *"$1"*) # no argument
        eval "$4=\"\$1\""
        ;;
      *)
        OPTLERR="bad option: \`-$1'"
        case "$3" in
          :*)
        eval "$4=:"
        OPTLARG="$1"
        ;;
          *)
        printf >&2 '%s\n' "$OPTLERR"
        eval "$4='?'"
        ;;
        esac
        ;;
    esac
    ;;
    esac
  else # long option

    # remove the leading "--"
    OPTLPENDING="$1"
    shift
    set 6 "${OPTLPENDING#--}" "$@"
    OPTLPENDING=

    while
      eval 'set -- "${'"$1"'}" "$@"'
      [ -n "$1" ]
    do
      # $1 = option name = ${$2+1}, $3 => given option = ${$4+3}, $5 = $@

      case "${3%%=*}" in
    "$1")
      OPTLPENDING=EXACT
      break;;
      esac

      # try to see if the current option can be seen as an abbreviation.
      case "$1" in
    "${3%%=*}"*)
      if [ -n "$OPTLPENDING" ]; then
        [ "$OPTLPENDING" = AMBIGUOUS ] || eval '[ "${'"$(($OPTLPENDING + 1))"'}" = "$1" ]' ||
          OPTLPENDING=AMBIGUOUS
          # there was another different option matching the current
          # option. The eval thing is in case one option is provided
          # twice in the specifications which is OK as per the
          # documentation above
      else
        OPTLPENDING="$2"
      fi
      ;;
      esac
      eval "shift 2; set $(($2 + 2)) "'"$@"'
    done

    case "$OPTLPENDING" in
      AMBIGUOUS)
    OPTLERR="option \`--${3%%=*}' is ambiguous"
    case "$5" in
      :*)
        eval "$6=:"
        OPTLARG="${3%%=*}"
        ;;
      *)
        printf >&2 '%s\n' "$OPTLERR"
        eval "$6='?'"
        ;;
    esac
    OPTLPENDING=
    return 0
    ;;
      EXACT)
        eval 'set "${'"$(($2 + 2))"'}" "$@"'
    ;;
      "")
    OPTLERR="bad option: \`--${3%%=*}'"
    case "$5" in
      :*)
        eval "$6=:"
        OPTLARG="${3%%=*}"
        ;;
      *)
        printf >&2 '%s\n' "$OPTLERR"
        eval "$6='?'"
        ;;
    esac
    OPTLPENDING=
    return 0
    ;;
      *)
        # we've got an abbreviated long option.
    shift
        eval 'set "${'"$(($OPTLPENDING + 1))"'}" "${'"$OPTLPENDING"'}" "$@"'
    ;;
    esac

    OPTLPENDING=

    # $1 = option type, $2 = option name, $3 unused,
    # $4 = given option = ${$5+4}, $6 = $@

    case "$4" in
      *=*)
    case "$1" in
      1 | required_argument | 2 | optional_argument)
        eval "$7=\"\$2\""
        OPTLARG="${4#*=}"
        ;;
      *)
        OPTLERR="option \`--$2' doesn't allow an argument"
        case "$6" in
          :*)
        eval "$7=:"
        OPTLARG="$2"
        ;;
          *)
        printf >&2 '%s\n' "$OPTLERR"
        eval "$7='?'"
        ;;
        esac
        ;;
    esac
    ;;

      *)
        case "$1" in
      1 | required_argument)
        if [ "$(($5 + 5))" -gt "$#" ]; then
          OPTLERR="option \`--$2' requires an argument"
          case "$6" in
        :*)
          eval "$7=:"
          OPTLARG="$2"
          ;;
        *)
          printf >&2 '%s\n' "$OPTLERR"
          eval "$7='?'"
          ;;
          esac
        else
          OPTLIND=$(($OPTLIND + 1))
          eval "OPTLARG=\"\${$(($5 + 5))}\""
          eval "$7=\"\$2\""
        fi
        ;;
      *)
        # optional argument (but obviously not provided) or no
        # argument
        eval "$7=\"\$2\""
        ;;
    esac
    ;;
    esac
  fi
  return 0
}





function die()
{
    >&2 echo "$@"
    exit 1
}


function logv()
{
    if [ "$opt_verbose" == true ]; then
            echo "$@"
    fi
}

function logd()
{
    if [[ "$opt_debug" != 0 ]]; then
            echo "$@"
    fi
}


function logd_var()
{
    local var_name="$1"

    logd "$var_name:" "${!var_name}"
}


function is_dir()
{
    [ -d "$1" ]
    return $?
}


function is_zip()
{
    local result=1
    local f="$1"

    ftype=$(file --brief --mime-type "$f")

    [ "$ftype" = "application/zip" ]
    result=$?

    # logd "is_zip(\"$f\"): $ftype"
    # logd "is_zip(\"$f\"): result=$result"

    return $result
}



function is_tarball()
{
    local result=1
    local f="$1"

    # application/x-tar:    regular tar file
    #
    # compressed files recognized:
    #   application/gzip    (.gz, .tgz)
    #   application/x-bzip2 (.bz2, .tbz2, .tbz)
    #   application/x-xz    (.xz)

    ftype=$(file --brief --mime-type "$f")
    logd "is_tarball(\"$f\"): $ftype"

    case "$ftype" in
        application/x-tar)
            # it's a tarball
            logd "is_tarball(\"$f\"): is a tarball (result=0)"

            result=0
            ;;

        application/gzip)       ;& # fall through
        application/x-bzip2)    ;& # fall through
        application/x-xz)
            # check if it's a compressed tarball
            ftype=$(file --brief --mime-type --uncompress "$f")
            logd "is_tarball(\"$f\"): uncompressed type: $ftype"
            if [ "$ftype" = "application/x-tar" ]; then
                logd "is_tarball(\"$f\"): is a compressed tarball (result=0)"

                result=0
            fi
            ;;
    esac

    [ "$result" = "1" ] && logd "is_tarball(\"$f\"): is not a tarball (result=1)"

    return $result
}


do_ssh()
{
    local cmd="$1"

    logd ssh root@"$opt_target" "$cmd"
    2>/dev/null ssh -o StrictHostKeyChecking=no root@"$opt_target" "$cmd"
}


do_scp()
{
    local srcfile="$1"
    local destfile="$2"

    logd scp "${srcfile}" "root@${opt_target}:${destfile}"
    2>/dev/null scp -o StrictHostKeyChecking=no ${srcfile} "root@${opt_target}:${destfile}"
}



install_ctap()
{
    # determine exactly what $opt_ctap is
    if is_dir "$opt_ctap"; then
        # $opt_ctap is a directory
        logd "$opt_ctap is a directory"

    elif is_zip "$opt_ctap"; then
        # $opt_ctap is azip file

        logd "$opt_ctap is a zip file"

        # create a tmpdir and unzip the archive
        tmpdir=$(mktemp -d --tmpdir "$script.XXXX")
        unzip "$opt_ctap" -d "$tmpdir"
    elif is_tarball "$opt_ctap"; then 
        # $opt_ctap is a tarball (possibly compressed)

        logd "$opt_ctap is a tarball"

        # create a tmpdir and unzip the archive
        tmpdir=$(mktemp -d --tmpdir "$script.XXXX")
        tar -xvf "$opt_ctap" -C "$tmpdir"
    else
        # an unsupported filetype
        die "\#$opt_ctap\" must be a directory, zip file, or tarball"
    fi

    # $ctapdir points to the directory where uncompressed files are
    local ctapdir="$opt_ctap"
    if [ -n "$tmpdir" ]; then
        ctapdir="$tmpdir"
    fi

    # do some very basic sanity checks
    [ ! -d "${ctapdir}/ctap" ] && die "no ${ctapdir}/ctap directory. Exiting..."
    [ ! -d "${ctapdir}/ctap/ctap-plugins" ] && die "no ${ctapdir}/ctap/ctap-plugins directory. Exiting..."
    [ ! -f "${ctapdir}/ctap/ctap" ] && die "no ${ctapdir}/ctap/ctap script. Exiting..."
    [ ! -f "${ctapdir}/ctap/socat" ] && die "no ${ctapdir}/ctap/socat file. Exiting..."


    # copy files to the console
    do_ssh 'mkdir -p /data/bin/ctap-plugins'

    for f in $ctapdir/ctap/* ; do
        if [ -f "$f" ]; then
            do_scp "$f" "/data/bin/"
            do_ssh "chmod 0777 /data/bin/$(basename "$f")"
        fi
    done

    # now copy the ctap-plugins over
    do_scp "$ctapdir/ctap/ctap-plugins/*" "/data/bin/ctap-plugins/"
    do_ssh "chmod 0777 /data/bin/ctap-plugins/*"


    #  rename xmllint.23 special case
    do_ssh 'cp /data/bin/xml.23 /data/bin/xml; chmod 0777 /data/bin/xml'

    if [[ "$opt_debug" != 0 ]]; then
        logd "Contents of ${opt_target}://data/bin/"
        logd ""
        do_ssh 'ls -l /data/bin/*'
    fi

    if [ "$opt_enable_ctap" = "1" ]; then
        logd "enabling ctap by editing environment.txt"
        # configure CPA_PROXY_ADDRESS=localhost if neccessary
        local environment_txt="/data/data/com.preva.mpaqtapp/files/support_files/resources/environment.txt"

        # some fancy quoting needed here so that single quotes
        #   are embedded and ${environment_txt} gets expanded...
        # do_ssh 'grep -q '\''^CPA_PROXY_ADDRESS=localhost$'\'' '"${environment_txt}"' || echo '\''CPA_PROXY_ADDRESS=localhost'\'' >>'"${environment_txt}"''

        # uncomment the CPA_PROXY_ADDRESS configuration
        do_ssh "busybox sed -i 's/^# CPA_PROXY_ADDRESS=localhost$/CPA_PROXY_ADDRESS=localhost/g' ${environment_txt}"

        # to undo the above edit:
        # do_ssh "busybox sed -i 's/^CPA_PROXY_ADDRESS=localhost$/# CPA_PROXY_ADDRESS=localhost/g' ${environment_txt}"
    else
        logd "not enabling ctap (--no-enable)"
    fi
}



cleanup()
{
    if [ -z "$tmpdir" ]; then
        logd "cleanup(): not using a temporary directory, nothing to clean up"
        return 0
    fi

    if [ "$opt_keep_temp" = "1" ]; then
        logd "cleanup(): --keep-temp specified. Leaving \"$tmpdir\""
        return 0
    fi

    logd "Cleaning up \$tmpdir: $tmpdir"
    rm -r "$tmpdir"

    return $?
}




main()
{
    parse_options "$@"

    logd_var opt_target
    logd_var opt_ctap
    logd_var opt_keep_temp
    logd_var opt_enable_ctap
    logd_var opt_verbose
    logd_var opt_debug
    logd ""

    validate_options
 
    install_ctap
    cleanup
}


main "$@"
