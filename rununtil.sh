#! /bin/bash
#
# rununtil - run a connand until a specified time (HH:MM)
#
# rununtil HH:MM [command [args...]]
#
#     HH:MM - time specified in 24 hour form
#
#     command [args...] - command to run along with any arguments
#
# if no command is provided, the duration until HH:MM in seconds is 
#   displayed
#
# Note: at the moment, rununtil is not smart enough to schedule a stop
#    time more than one day in the future.
#
# see https://stackoverflow.com/a/19067658/12711 for notes on the 
#    duration calculation
#

scriptname="${0/#$HOME/\~}"

# some standard variables for logging/debugging
#
# turn them on via options in `parse_options()` or
# set them on the command line as environment variables:
#
#     opt_debug=true myscript arg1 arg2
#
opt_verbose="${opt_verbose:-false}"
opt_debug="${opt_debug:-false}"


# include any "library" functions
#
# general utility functions:
#   . ~/devtrees/scripts/bash-utils.shlib
# 
# getopts_long() function:
#   . ~/devtrees/scripts/getopts_long.shlib


function die()
{
    >&2 echo "$@"
    exit 1
}


function logv()
{
    if [ "$opt_verbose" == true ]; then
            echo "$@"
    fi
}

function logd()
{
    if [ "$opt_debug" == true ]; then
            echo "$@"
    fi
}

function logd_var()
{
    local var_name="$1"

    logd "$var_name:" "${!var_name}"
}



function usage()
{
echo "Usage: $scriptname" HH:MM [command [args...]]
cat <<EOF

    HH:MM - time specified in 24 hour form

    command [args...] - command to run along with any arguments

if no command is provided, the duration until HH:MM in seconds is 
  displayed

Note: at the moment, rununtil is not smart enough to schedule a stop
   time more than one day in the future.
EOF
}



function parse_options()
{
    # add any command line option parsing here...
    #
    # I strongly suggest using 
    #
    #    . ~/devtrees/scripts/getopts_long.shlib
    # 
    # to get a `getopts_long()` function

    return 0
}




function main()
{
    parse_options "$@"
    
    if [ "$#" -eq 0 ]; then 
        usage
        exit 0
    fi

    deadline="$1"
    shift


    command="$1"
    shift
    args=("$@")

    logd deadline: ${deadline}
    logd command: ${command}
    logd arguments: 
    if [ "$opt_debug" != "false" ]; then
        echoargs.sh "${args[@]}"
    fi


    # see https://stackoverflow.com/a/19067658/12711 for duration calculation
    #
    #    the original, obtuse duration calculation:
    #
    #   duration=$((($(date -f - +%s- <<<${deadline}$' tomorrow\nnow')0)%86400))

    secs_now=$(date --date=now  +%s)
    secs_deadline=$(date --date="${deadline} tomorrow" +%s)
    secs_until_deadline=$(( (secs_deadline - secs_now) % 86400 ))

    logd_var secs_now
    logd_var secs_deadline
    logd_var secs_until_deadline

    logv running until: $(date +%H:%M:%S --date="+${secs_until_deadline} seconds") "($((secs_until_deadline / 60)) minutes $(( secs_until_deadline % 60)) seconds)"

    logd "running: "timeout ${secs_until_deadline} "${command}"  "${args[@]}"
    timeout ${secs_until_deadline} "${command}"  "${args[@]}"
}


# call the main function
main "$@"

