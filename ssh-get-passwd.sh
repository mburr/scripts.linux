#!/bin/bash

# get a password stored in ~/.ssh/config for a host

# ssh doesn't support storing a plain old password in the ~/.ssh/config file
#
# The usual wasy to handle authentication with ssh is public/private keys. But
# there are times when you are given a login for an ssh site (perhaps for sftp)
# that only has a userid and password.  This function will let you store and
# retrieve that password in an ssh config entry for the host.
#
# Most people consider this a bad practice, but if you must use passwork auth
# (ie., you can't convince the admin to use key pairs) then this will let you
# automate authentication using the files at ssh usually uses
#
# This script prints the password token specified by the "#InsecurePassword"
# setting for the given Host to stdout and hte script return 0.
#
# If there is no password #InsecurePassword string , nothign is printed to 
# stdout and the script returns an error code 1.


# How it works:
#
# this script parses ~/.ssh/config looking for a matching Host configuration
# that also has a "comment" line that starts with "#InsecurePassword" (which
# can be preceded by whitespace).   An example:
#
# Host contoso.com
#     PreferredAuthentications password
#     User jane-smith
#     #InsecurePassword P@$sw0Rd!
#
# Note that the "#InsecurePassword" 'setting' starts with a '#' character,
# so as far as ssh is concerned it's just a comment.
#
# The ~/.ssh/config file is searched for a string of text that starts with
# "Host" and ends just before another "Host" (or EOF).  grep is used to 
# perform the search.  The following options are used:
#
#   -P  (Perl-style regex)
#   -z  (a line of text is terminated with '\0' instead of newline)
#   -o  (only output the text that matches the regex)
#
# The -z option means that the who config file is a single line as far as
# grep is concerned. That allows grep to look for a match that involves text
# across multiple actual lines.
#
# If a password is used for a host, the ~./ssh/config for that host should
# probably also have the "PreferredAuthentications=password" config option
# specified.  Otherwise all of the uses public keys will be tried, which
# might result in a connection being refused due to "too manu auth failures"
# (at least that's what happened with Harman's FTP server)
#
# see https://serverfault.com/a/974972 for the original inspriation for this script
#

function ssh-get-passwd()
{
    local debug=false
    local LASTARG="${!#}"

    local HOST
    local INSECUREPASS
    local SSHPASS

    HOST="$LASTARG"
    HOST="${HOST/#*@/}"
    HOST="${HOST/%:*}"

    $debug && printf "DEBUG: HOST=%s\n" "$HOST"

    # look for "#InsecurePassword" inside of a multi-line string that starts
    #   with "Host $HOST" up to (but not including) the next "Host" token (or EOF)
    #   also get rid of the trailing null character

    INSECUREPASS=$(grep -Pzo "Host $HOST"'\s*\n((?!Host).*\n)*[ \t]*#InsecurePassword[ \t]+(\N+)\n' ~/.ssh/config | tr -d '\000')

    $debug && printf "DEBUG: INSECUREPASS=%s\n" "$INSECUREPASS"

    # extract the password token that follows "#InsecurePassword"
    # SSHPASS=$(printf "%s" "$INSECUREPASS" | tail -n 2 | head -n 1 | sed -E 's/[ \t]*#InsecurePassword[ \t]+//')
    SSHPASS=$(printf "%s" "$INSECUREPASS" | tail -n 1 | sed -E 's/[ \t]*#InsecurePassword[ \t]+//')

    $debug && printf "DEBUG: SSHPASS=%s\n" "$SSHPASS"

    if [ -n "$SSHPASS" ]; then
        printf "%s\n" "$SSHPASS"
        return 0
    fi

    return 1
}

# stop if this file is being sourced to define the function
return 2>/dev/null

# if this is being run in a subshell (ie., not sourced), run the function
ssh-get-passwd "$@"
