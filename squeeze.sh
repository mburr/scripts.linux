# squeeze.sh
#
# reduce runs of blank lines in a file to a single blank line
#
# A line with only whitespace (spaces or tabs) is considered blank
#
# A filename of "-" will read from stdin
#
# NOTE: trailing whitespace is also removed for all lines!
#
# TODO:
#
#   - add help
#   - add option to remove blank lines entirely
#   - add option to treat lines with whitespace as non-blank
#   - add option to reduces runs of blank lines to 1, 3, or n
#       blank lines instead of 1
#   - add option to remove all blank lines from start/end

squeeze()
{
	local infile="$1"

	# pass "-" to sed as the input filename is stdin should be used	
	if [ "$infile" = "" ] ; then infile="-"; fi
	
    sed 's/[ \t]*$//' "$infile" | cat --squeeze-blank
}

squeeze "$1"


<<TEST
This is an area that squeeze.sh can use to test itself

The original has 4 blank lines following this one




And these three lines have embedded spaces or tabs
     
    	    	
	
Some indented lines to make sure leading spaces aren't messed up
      space indented line should be left alone
	tab indented line should also be left alone
A line with 5 trailing spaces:     
end of test...
TEST
