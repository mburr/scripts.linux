#! /bin/bash

# debug can contain one or more of the following tokens to enable
# certain debugging operations:
#
#   log,  keeptmp
opt_debug="log "

script=$(basename "$0")


# set defaults (can override with environment variables)
platform=${platform:-"unknown"}
console=${console:-"unknown"}
logfile=${logfile:-"${HOME}/temp/build-logs/$(basename $WORKDIR)-${TARGET_PRODUCT}-${TARGET_BUILD_VARIANT}-$(date +%Y.%m.%d-%H%M)-make.log"}



function die()
{
    >&2 echo "$@"
    exit 1
}


function logv()
{
    if [ "$opt_verbose" == true ]; then
            echo "$@"
    fi
}

function logd()
{
    if [[ "$opt_debug" =~ log ]]; then
            echo "$@"
    fi
}


function logd_var()
{
    local var_name="$1"

    logd "$var_name:" "${!var_name}"
}

# get all those  Android build environment shell functions
source build/envsetup.sh >/dev/null

platform_version="$(get_build_var PLATFORM_VERSION)"
if [ "$platform" = "unknown" ]; then
    case "$platform_version" in
        4*) platform=jellybean
            ;;
        6*) platform=marshmallow
            ;;
    esac
fi


if [ "$console" = "unknown" ]; then
    case "$TARGET_PRODUCT" in
        *p82*)  console=p82
                ;;
        var_som_mx6q)   console=p82
                        ;;
        *p62*)  console=p62
                ;;
    esac
fi

logd "DEBUG info ======================"
logd_var platform_version
logd_var TARGET_PRODUCT
logd_var TARGET_BUILD_VARIANT
logd_var platform
logd_var console
logd_var logfile
logd "DEBUG info end==================="
logd ""



if [ "$platform" = "unknown" ]; then
    die "platform unknown - must be jellybean or marshmallow"
fi

if [ "$console" = "unknown" ]; then
    die "console unknown - must be p62 or p82"
fi

printf "logfile: %s\n" "${logfile}"                         |  tee    "${logfile}"
date +"Build started %Y-%m-%d at %-I:%M%P (%R)"             |  tee -a "${logfile}"
printf "\n"                                                 |  tee -a "${logfile}"
(time make_me --android $platform --console $console 2>&1)  |& tee -a "${logfile}"
printf "\nlogfile: %s\n" "${logfile}"                       |  tee -a "${logfile}"
