scriptName=$(basename "$0")

# Print usage
usage() {
  >&2 echo "${scriptName} [OTA file] [Console address]"
}



if [ "$#" -ne 2 ]; then
    >&2 echo "Illegal number of parameters"
    usage
    exit 1
fi

OTA_FILE="$1"
TARGET_IP="$2"

if [ ! -f "${OTA_FILE}" ]; then
    >&2 echo "${OTA_FILE} does not exist."
    exit 1
fi

echo "removing /cache/*.ota..."
ssh -o StrictHostKeyChecking=no root@${TARGET_IP} "rm /cache/*.ota"


echo "copying ${OTA_FILE} to ${TARGET_IP}..."
scp -o StrictHostKeyChecking=no "${OTA_FILE}" root@${TARGET_IP}:/cache/

echo "initiating the reflash..."
ssh -o StrictHostKeyChecking=no root@${TARGET_IP} '(OTA_FILE=$(find /cache -name "*.ota" | sort | tail -n 1);cd /mnt/obb;unzip "${OTA_FILE}" recovery.img;cat /dev/zero > /dev/block/mmcblk0p2 2>/dev/null;cat /mnt/obb/recovery.img > /dev/block/mmcblk0p2;mkdir -p /cache/recovery;echo "USB" > /cache/recovery/last_update_source;echo "--update_package=${OTA_FILE}" > /cache/recovery/command;sync;sleep 5;echo "rebooting...";reboot recovery)'

