#!/bin/bash

scriptname="${0/#$HOME/\~}"

# some standard variables for logging/debugging
#
# turn them on via options in `parse_options()` or
# set them on the command line as environment variables:
#
#     opt_debug=true myscript arg1 arg2
#
opt_verbose="${opt_verbose:-false}"
opt_debug="${opt_debug:-false}"


# include any "library" functions
#
# general utility functions:
#   . ~/devtrees/scripts/bash-utils.shlib
# 
# getopts_long() function:
#   . ~/devtrees/scripts/getopts_long.shlib

function loge()
{
    >&2 echo "$@"
}


function die()
{
    loge "$@"
    exit 1
}


function logv()
{
    if [ "$opt_verbose" == true ]; then
            loge "$@"
    fi
}

function logd()
{
    if [ "$opt_debug" == true ]; then
            loge "$@"
    fi
}



function usage()
{
echo "Usage: $scriptname"
cat <<EOF

Description of the program...


The following command line options are supported:

    -f, --file=FILE                read the commit list from FILE instead
                                       of stdin
    -v, --verbose                  verbose messages
        --debug                    debug messages
    -h, --help                     this help message

EOF
}



function parse_options()
{
    # add any command line option parsing here...
    #
    # I strongly suggest using
    #
    #    . ~/devtrees/scripts/getopts_long.shlib
    #
    # to get a `getopts_long()` function

    return 0
}




function main()
{
    parse_options "$@"

    # the reset of the script...
}


# call the main function
main "$@"

