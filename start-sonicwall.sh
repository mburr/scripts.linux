#! /bin/bash


# file that contains userid (line 1) and password (line 2)
# default to "~/.vpn-xdc/creds.sonicwall"
#
# Can be overridden by setting environment variable VPN_SONICWALL_CREDS
#
creds="${VPN_SONICWALL_CREDS:-${HOME}/.vpn-xdc/creds.sonicwall}"


pid=''

sonicwall ()
{
	local userid=$(head -n 1 $creds)
	local passwd=$(head -n 2 $creds | tail -n 1)

	echo "About to start sonicwall"

	# echo "Y" to get past the warning about self-signed certs
	echo "Y" | netExtender --auto-reconnect --username="$userid"  --domain=LocalDomain --dns-only-local precorsw.precor.com:4433 -p "$passwd" &
	pid=$!

	echo "netExtender pid: $pid"
}

dns_add ()
{
	echo Adding XDC nameserver...

	# add XDC's nameserver
	printf "%s\n%s\n" "nameserver 192.168.248.51" "search xdc.precor.int" | sudo resolvconf -a sonicwall.vpn

	echo Done adding XDC nameserver.
}


dns_remove ()
{
	echo Removing XDC nameserver...

	# remove XDC's DNS
	sudo resolvconf	-d sonicwall.vpn
	echo Done removing XDC nameserver.
}


cleanup ()
{
	kill -9 $pid
	wait $pid
	dns_remove
	exit 0
}

main ()
{
	trap cleanup EXIT

	sonicwall

	# wait for sonicwall to startup...
	sleep 8

	dns_add

	echo "Press Ctrl-C to exit..."
	# idle waiting for abort from user (see https://stackoverflow.com/a/36991974/12711)
	read -r -d '' _ </dev/tty
}

main "$@"
