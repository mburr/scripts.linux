#!python3

# weird hack to force a "readable" syntax error when this is run under
#   python2 instead of python3
#
# see https://stackoverflow.com/a/65407535/12711
#
# This takes advantage of a particular syntax that is new to Python 3,
#   "PEP 3132 -- Extended Iterable Unpacking"
#   https://www.python.org/dev/peps/pep-3132/
#
Python_2_is_running, *Python_3_is_needed="python 3 is needed" # this script needs to be run with python 3



import logging
import os
import shlex
import shutil
import subprocess
import sys

# globals
def get_version():
    prog_ver = "1.0"
    (prog_path, prog_name) = os.path.split( sys.argv[0])

    return (prog_path, prog_name, prog_ver)



def get_version_string(include_py_ver = False):
    (prog_path, prog_name, prog_ver) = get_version()

    result = f'{prog_name} wrapper {prog_ver}  ({prog_path}/{prog_name})'
    if (include_py_ver):
        py_ver_string = "Python version: " + sys.version
        result += "\n" + py_ver_string

    return result



class Options:
    def __init__(self):
        self.opt_debug      = False
        self.opt_help       = False
        self.opt_oneline    = False

OPTIONS = Options()

def get_opt_debug():
    return OPTIONS.opt_debug

def get_opt_help():
    return OPTIONS.opt_help
    
def get_opt_oneline():
    return OPTIONS.opt_oneline
    
def set_opt_debug(val):
    OPTIONS.opt_debug = val
    return OPTIONS.opt_debug 

def set_opt_help(val):
    OPTIONS.opt_help = val
    return OPTIONS.opt_help
    
def set_opt_oneline(val):
    OPTIONS.opt_oneline = val
    return OPTIONS.opt_oneline
    

custom_help='''
Custom options:
  -<number>                : limit number of commits output (or searched)
  -n <number>
  --max-count=<number>

  --oneline                : output the log entries in a one-line format

  --debug                  : print debug output for wrapper script
'''




def logged_call_build_msg(cmd, **kwargs):
    # put cmd into the msg, joining if it's a collection of arguments
    msg = "running: \n========\n{cmd}\n"
    if type(cmd) is str:
        msg = msg.format(cmd=cmd)
    else:
        msg = msg.format(cmd = " ".join(cmd))

    # list ant keyword arguments, one per line
    if kwargs:
        # list the keyword arguments passed in
        msg += "\nkeyword args:\n"
        for arg in kwargs:
            msg += "    {arg} = {val}\n".format(arg=arg, val=kwargs[arg])
    msg += "========"
    return msg


def logged_call(cmd, **kwargs):
    # special case when called with a string - split it into a
    #   collection of args - but **only** if `shell==False`
    logging.debug( f"cmd: {cmd}")
    logging.debug( f"kwargs: {kwargs}");

    if type(cmd) is str and not kwargs.get("shell"):
        cmd = shlex.split(cmd)
        logging.debug( f"split cmd: {cmd}")

    logging.debug( logged_call_build_msg(cmd, **kwargs))

    return subprocess.call(cmd, **kwargs)


def logged_check_output(cmd, **kwargs):
    # special case when called with a string - split it into a
    #   collection of args - but **only** if `shell==False`

    if type(cmd) is str and not kwargs.get("shell"):
        cmd = shlex.split(cmd)

    logging.debug( logged_call_build_msg(cmd, **kwargs))

    return subprocess.check_output(cmd, **kwargs)





def printd(*args, **kwargs):
    # pass all arguments on to the built-in print() if we're in debug mode
    opt_debug = get_opt_debug()
    
    if not opt_debug:
        return
        
    return print( *args, **kwargs)




def printe(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)





def log_entry():
    func = sys._getframe(1).f_code.co_name
    printd( f"----> entering {func}() <-----")
    return func





def do_help( args):
    func = log_entry()

    # call `svn log` with the passed in arguments so it
    #   call print the appropriate help or error

    ret = 0   #TODO:  needs t be: ret = call_svn_log( args)

    if ret == 0:
        global custom_help
        print( custom_help)

    return ret


def has_help( args):
    func = log_entry()

    return "--help" in args or "-h" in args
    
    
    

#
# handle the --debug option
def handle_debug( args):
    func = log_entry()

    args = args.copy()    # don't modify the passed in args list
    new_args = []

    while args:
        arg = args.pop(0)
        match arg:
            case "--":
                # "--" means nothing after this argument is an option
                #    so just move the remainder of args to the new_args list
                printd( func, f'found "--", appending remaining args')
                new_args.append(arg)
                new_args.extend(args)
                args.clear()

            case "--debug":
                # turn on debug output and remove the option (don't append to new_args)
                set_opt_debug( True)
                printd( func, f'found "--debug"')
                
            case _:
                printd( func, f'found "{arg}", appending it to new_args')
                new_args.append(arg)

    return new_args


#
# handle the --version option
#
def handle_version( args_):
    func = log_entry()
    
    args = args_.copy()      ## don't modify passed in args_
    new_args = []

    while args:
        arg = args.pop( 0)

        if arg == "--version":
            # print version info, but pass the option on so svn will too
            print( get_version_string("with_py_info"))
            new_args.append(arg)

        else:
            # not --version, just pass it along
            printd( func, f'passthru "{arg}" to new_args')
            new_args.append(arg)

    return new_args




#
#  handle the -123 option
#
def handle_number( args_):
    func = log_entry()

    args = args_.copy()      ## don't modify passed in args_
    new_args = []

    while args:
        arg = args.pop(0)

        # case: -123  ==> -l 123
        if arg.startswith( "-"):
            # parse whatever follows the -, if it's an integer
            #   then add '-l' and that integer to new_args as separate
            #   arguments
            # otherwise, add the arg to new_args and let svn deal with it
            suffix = arg.removeprefix( "-")
            if str.isdigit( suffix):
                printd( func, f'convert "{arg}" to -l {suffix}')
                new_args.append( "-l")
                new_args.append( suffix)
            else:
                printd( func, f'passthru "{arg}" to new_args')
                new_args.append( arg)

        else:
            # argument isn't related to -123, so just pass it along
            printd( func, f'passthru "{arg}" to new_args')
            new_args.append(arg)

    return new_args



#
# handle -n 123 and -n123:
#
#   -n 123
#   -n123
#
def handle_n( args_):
    func = log_entry()

    args = args_.copy()      ## don't modify passed in args_
    new_args = []

    while args:
        arg = args.pop(0)

        # case: -n 123  ==> -l 123
        if arg == "-n":
            # add '-l' and pop next argument add that arg to new_args
            new_args.append("-l")
            if args:
                new_args.append( args.pop(0))

        # case: -n123  ==> -l 123
        elif arg.startswith( "-n"):
            # parse whatever follows the -n, if it's an integer
            #   then add '-l' and that integer to new_args as separate
            #   arguments
            # otherwise, add the arg to new_args and let svn deal with it
            suffix = arg.removeprefix( "-n")
            if str.isdigit( suffix):
                printd( func, f'convert "-n {suffix}" to -l {suffix}')
                new_args.append( "-l")
                new_args.append( suffix)
            else:
                printd( func, f'passthru "{arg}" to new_args')
                new_args.append( arg)

        else:
            # argument isn't related to -n, just pass it along
            printd( func, f'passthru "{arg}" to new_args')
            new_args.append(arg)

    return new_args


#
# handle --max-count 123 and --max-count=123:
#
def handle_max_count( args_):
    func = log_entry()
    
    args = args_.copy()      ## don't modify passed in args_
    new_args = []

    while args:
        arg = args.pop( 0)

        # case --max-count=123  ==> --limit 123
        if arg.startswith( "--max-count="):
            # parse whatever follows the --match-count= then
            #   then add '--limit' and that following string to new_args
            #   (there's no need to determine if the suffix is an int - let
            #   svn deal with that error)
            suffix = arg.removeprefix( "--max-count=")
            printd( func, f'convert "{arg}" to --limit {suffix}')
            new_args.append( "--limit")
            new_args.append( suffix)

        # case --max-count 123  ==>  --limit 123
        elif arg == "--max-count":
            # add "--limit" and pop the next arg and add it to the new_args list
            num = args.pop(0)
            printd( func, f'convert "{arg} {num}" to --limit {num}')
            new_args.append( "--limit")
            new_args.append( num)

        else:
            # argument isn't related to --max-count, just pass it along
            printd( func, f'passthru "{arg}" to new_args')
            new_args.append(arg)

    return new_args


def handle_oneline( args_):
    func = log_entry()

    has_oneline = False
    
    args = args_.copy()      ## don't modify passed in args_
    new_args = []

    while args:
        arg = args.pop( 0)
        
        if arg == "--oneline":
            # --oneline implies --xml, so append that
            printd( func, f'convert "{arg}" to --xml')
            printd( func, f'set has_oneline flag')
            new_args.append( "--xml")
            has_oneline = True
            
        else: 
            # not --oneline, so pass it along
            printd( func, f'passthru "{arg}" to new_args')
            new_args.append( arg)
        
    return (has_oneline, new_args)




def run_svn_log(args, do_oneline):
    func = log_entry()
    
    if has_help(args):
        print_custom_help = True
        printd( f"calling 'svn log {args}' so it can print help")

        # args.pop(0)
        ret = 0
        try:
            cmd = [ "svn", "log" ]
            cmd.extend(args)
            output = logged_check_output( cmd)
        except subprocess.CalledProcessError as err:
            ret = err.returncode
            output = err.output
            print_custom_help = False

        output = output.decode()    # will this always use the correct encoding?
        print(output)
        
        if print_custom_help:
            global custom_help
            print(custom_help)
        exit( 0)                # if printing help, we're done, just exit

    printd( f'calling: svn log {args}')
    
    cmd = [ "svn", "log" ]
    cmd.extend( args)
    
    if do_oneline:
        # create a pipline between the "svn log" process and the "oneline.py" process
        #   see: https://docs.python.org/3/library/subprocess.html#replacing-shell-pipeline
        
        printd( func, f'wrapping "svn log --xml" in a pipeline to svn-log-oneline.pyexec to transform to oneline format')
        oneline_cmd = [ shutil.which("py"), shutil.which("svn-log-oneline.py") ]
        
        p1 = subprocess.Popen( cmd, stdout=subprocess.PIPE)
        p2 = subprocess.Popen( oneline_cmd, stdin=p1.stdout)  # , stdout=PIPE)
        p1.stdout.close()   # Allow p1 to receive a SIGPIPE if p2 exits
        # output = p2.communicate()[0]
        printd( func, f'waiting for the pipeline to complete')
        p2.wait()
    else:
        logged_call( cmd)
    
    return 0




def handle_svn_log( args_):
    func = log_entry()

    args = args_.copy()
    new_args = args.copy()
    
    # check for --debug
    new_args = handle_debug( new_args)
    printd( f'new_args: {new_args}')

    # display version info if --debug flag was set
    printd( get_version_string("with-py-ver"))

    new_args = handle_version( new_args)
    printd( f'new_args: {new_args}')

    # handle -<number>
    new_args = handle_number( new_args)
    printd( f'new_args: {new_args}')

    # handle -n <number>
    new_args = handle_n(new_args)
    printd( f'new_args: {new_args}')

    # handle --max-count
    new_args = handle_max_count( new_args)
    printd( f'new_args: {new_args}')

    # handle --oneline
    #  this function returns two things:
    #    - a flag indicating if --oneline was passed in
    #    - a new_args list with "--oneline" removed
    #
    (has_oneline, new_args) = handle_oneline( new_args)
    printd( f'new_args: {new_args}')

    ret = run_svn_log( new_args, has_oneline)

    return ret




def handle_svn( args_):
    func = log_entry()

    # convert "custom" args to those appropriate to "svn log"
    if args_[0] == "log":
        return handle_svn_log( args_[1:])


    # any other svn command  gets --debug handled then just gets passed through
    new_args = handle_debug( args_)

    # display version info if --debug flag was set
    printd( get_version_string("with-py-ver"))

    new_args = handle_version( new_args)

    svn_cmd = [ shutil.which("svn.exe") ]
    if not svn_cmd or not svn_cmd[0]:
        printe( 'error: could not find "svn.exe"')
        return 127
    
    svn_cmd.extend( new_args)
    printd( svn_cmd)
    
    try:
        retval = logged_call( svn_cmd)
    except FileNotFoundError:
        printe( 'error: could not find "svn.exe"')
        return 127

    return retval




def main(argv):
    func = log_entry()

    (_, prog_name, *_) = get_version()

    if prog_name.lower() == "svn-log.py":
        retval = handle_svn_log( argv[1:])
    else:
        retval = handle_svn( argv[1:])
        
    return retval




if __name__ == "__main__":
    if sys.version_info < (3,10):
        print("Python version: " + sys.version)
        print("This script requires Python 3.10 or later (for switch/match)")
        sys.exit(1)

    func = sys._getframe().f_code.co_name
    printd( f"entering {func}()")


    exit( main(sys.argv))
