#! /bin/bash

# create a Fireird PDF label for a given serial number

#
# the commands were derived from the following makefile
#
#         ./iot-mfg/mfg-server/components/qrcode/Makefile.projbuild
#
# (this script scales the final PDF to 73% of what the MFG Server
#  build does do the resulting QR fits on top of the ESP32 'can')
#
# dependencies:
#
#     sudo apt-get install qrencode
#     sudo apt-get install graphviz
#
# If you get an error like:
#
#     convert: not authorized `SERIAL_label.pdf' @ error/constitute.c/WriteImage/1028.
#
# That means that ImageMagick policies have disabled PDF creation
#
# Enable ImageMagick to generate PDF files by editing:
#
#     /etc/ImageMagick-6/policy.xml
#
# (requries root permission) and change the PDF policy from
# "none" to "read|write":
#
#     <policy domain="coder" rights="read|write" pattern="PDF" />
#
# See https://askubuntu.com/q/1081695/667744 for more info.
#


SERIAL="$1"
TMP1="~${SERIAL}-tmp1.png"
TMP2="~${SERIAL}-tmp2.png"
TMP3="~${SERIAL}-tmp3.png"


qrencode -l H -m 0 -t PNG -s 11  -o "${TMP1}" "${SERIAL}"

dot -Tpng   -o "${TMP2}" <<EOF
digraph qr {
sep=0
pad=0
rankdir=LR
margin=0
nodesep=0
ranksep=0
node [shape=none,label="",margin=0,width=0,height=0];
imgnode [image="${TMP1}"];
textnode [label="${SERIAL}",fontsize=28];
imgnode -> textnode [style=invis,arrowsize=0,len=0,minlen=0];
}
EOF

convert "${TMP2}" -density 300 "${TMP3}"

convert -scale 73% "${TMP3}"  "${SERIAL}_label.pdf"

rm "${TMP3}" "${TMP2}" "${TMP1}"
