#!/bin/bash

hexfile_fullname="$1"
echo "Splitting $hexfile_fullname"

hexfile_basename="$(basename --suffix=.hex "$hexfile_fullname")"

csplit -s --prefix="$hexfile_basename.partition-" --suffix-format="%1d.hex"  "$hexfile_fullname" /:00000001FF/+1

mv "$hexfile_basename.partition-0.hex" "$hexfile_basename.partition-a.hex"
mv "$hexfile_basename.partition-1.hex" "$hexfile_basename.partition-b.hex"
