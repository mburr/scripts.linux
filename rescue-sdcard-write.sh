#! /bin/bash
#
# Write a gzipped rescue sdcard image to an sdcard
#
# Notes:
#
#	- the image file can be raw or gzipped
#	- the script will not write to a device that is larger than 32GB
#	   (to prevent accidentally trashing an important partition)

SCRIPT_NAME="$(basename ${0})"
AUTHOR=mburr
VERSION=2.0.0


# options (some may be  only supported via environment variables, not
#   as command line options):
#
#   opt_dry_run     set to 1 to do everything except the actual write
#   opt_list_drives set to 1 to list drive (and do nothing else)
#   opt_gb_max      set to max number of GB drive that is OK to write
#   opt_debug:      set to 1 to get debug messages
#   opt_verbose:    set to 1 to get verbose output
#   opt_version     set to 1 to display the version of the script
#


# as a safety measure - don't write to a device that is larger than
#   $DEFAULT_GB_MAX Gigabytes. This can be overridden with the
#   $opt_gb_max option
DEFAULT_GB_MAX=64

opt_list_drives="${opt_list_drives:-0}"
opt_dry_run="${opt_dry_run:-0}"
opt_gb_max="${opt_gb_max:-$DEFAULT_GB_MAX}"
opt_debug="${opt_debug:-0}"
opt_verbose="${opt_verbose:-0}"
opt_help="${opt_help:-0}"
opt_version="${opt_version:-0}"

loge()
{
    1>&2 echo "$@"
}


logd()
{
    is_debug && echo "DEBUG:" "$@"
}

logd-positional-args()
{
    ! is_debug && return 0

    local arg_string=""

    for arg in "${positional_args[@]}"; do
        arg_string="$arg_string \"$arg\" "
    done

    logd "positional args: " "$arg_string"
}


logv()
{
    is_verbose && echo "$@"
}


log()
{
    echo "$@"
}



die()
{
    loge "$@"
    exit 1
}


is_truthy()
{
    # "${var,,}" converts the variable's value to lowercase  (bash 4+)
    local arg="${1,,}"
    
    [ "$arg" = "1" ] || [ "$arg" = "true" ] || [ "$arg" = "yes" ] || [ "$arg" = "y" ] || [ "$arg" = "on" ]
}

is_debug()
{
    is_truthy "$opt_debug"
}

is_verbose()
{
    is_truthy "$opt_verbose"
}


parse-args()
{
    logd "in parse-args()"

    local args=("${@: 1}")

    for arg in "${args[@]}"; do
        case "$arg" in

        --dry-run)
            logd "handling --dry-run"
            opt_dry_run=1
            ;;

        --list-drives)
            logd "handling --list_drives"
            opt_list_drives=1
            ;;

        --no-list-drives)
            logd "handling --no-list_drives"
            opt_list_drives=0
            ;;

        --verbose)
            logd "handling --verbose"
            opt_verbose=1
            ;;

        --version|-v)
            logd "handling --version|-v"
            opt_version=1
            ;;

        --debug)
            logd "handling --debug"
            opt_debug=1
            ;;

        --help|-h)
            logd "handling --help|-h"
            opt_help=1
            ;;

          *)
            # append this argument to the `positional_args` array
            logd "handling positional argument: $arg"
            positional_args+=( "$arg" )
            ;;
        esac
    done

    return 0
}




display-version()
{
    log "${SCRIPT_NAME} v$VERSION  author: $AUTHOR"
}


usage()
{
	echo -n "
usage: ${SCRIPT_NAME} IMAGE-FILE SDCARD-DEVICE

Write a raw or gzipped image to sdcard

    IMAGE-FILE       Name of raw or gzipped image to write
    SDCARD-DEVICE    Name of the sdcard to write to (ie. /dev/sdX)
                       (/dev/null can be specified for testing)

Options:

    --dry-run        Don't actually perform the write

    --list-drives    List the hot-pluggable drives on this machine
                       (an aid to figuring out which /dev/sdX is the sdcard)
                       (requires admin rights)

                       Note: if this option is given, the sdcard write
                         will *not* be performed

    --no-list-drives Cancel the effect of any *prior* --list-drives option

    --verbose         Display verbose output

    --version|-v      Display the script's version

    --debug           Display Debug output

    --help|-h         Show this usage screen

Note: as a safety measure, this script will only write to devices ${GB_MAX}GB or smaller
"
}


main()
{
    # set up an array to receive positional arguments
    # parse-args() will append an unhandled arguments to that array

    positional_args=()

    parse-args "${@: 1}"

    GB_MAX="$opt_gb_max"
    SIZE_MAX=$(($opt_gb_max * 1024 * 1024 * 1024))  # 64GB sdcard

    logd "$SCRIPT_NAME v$VERSION"
    logd "opt_help: $opt_help"
    logd "opt_version: $opt_version"
    logd "opt_dry_run: $opt_dry_run"
    logd "opt_list_drives: $opt_list_drives"
    logd "number of positional arguments: ${#positional_args[@]}"
    logd-positional-args

    if is_truthy "$opt_version" ; then
        display-version
    fi

    if is_truthy "$opt_help" ; then
        usage
        exit 0
    fi

    if is_truthy "$opt_list_drives" ; then
        # list hotpluggable drives installed and the size
        # if this option is used, do  not do any actual work
        #
        #TODO: list only drives that meet the $SIZE_MAX setting

        log "listing removable block devices:"
        log ""
        sudo fdisk -l | grep -e "^Disk\s/dev/" -e "^Disk model:" | awk -v n=2 '1; NR % n == 0 {print ""}'
        exit 0
    fi

    if [[ "${#positional_args[@]}" -eq 0 ]] ;then
        # only print the usage if the user didn't ask for the version
	    is_truthy "$opt_version" && exit 0

	    usage
	    exit 0
    fi

    if [[ "${#positional_args[@]}" -ne 2 ]] ;then
        loge "error: invalid number of positional arguments: ${#positional_args[@]}"
	    usage
	    exit 1
    fi



    IMAGE="${positional_args[0]}"
    SDCARD="${positional_args[1]}"
    SDCARD_BASE="$(basename "$SDCARD")"

    logd "image: $IMAGE"
    logd "SDCARD: $SDCARD"
    logd "SDCARD_BASE: $SDCARD_BASE"


    if [[ "$SDCARD" = "/dev/null" ]] ; then
        # /dev/null isn't technically a block device or sdcard, but it's safe and useful for testing
        device_count=1
    else
        device_count=$(lsblk --byte --raw --noheadings --output NAME,SIZE,TYPE | grep -c "^$SDCARD_BASE " 2>/dev/null)
    fi

    logv "Found $device_count block device(s) for" "$SDCARD_BASE"


    dd_conv_opts="nocreat"     # the target device might not support dd's `conv=fsync` option
    # validate that the SDCARD argument exists, is /dev/null or a block device and is unique

    if [[ "$SDCARD" = "/dev/null" ]] ; then
        # /dev/null is a fine destination (for testing), even though it's not a block device
        :  # nothing to do, but we need a 'null statement' for syntax reasons
    elif [[ ! -b "$SDCARD" ]] || [[ "$device_count" = "" ]] || [[ 0 -eq "$device_count" ]] ;then
        1>&2 echo error - block device not found: $SDCARD
        exit 1
    else
        # it's a block device that looks OK - use the `conv=fsync` option
        dd_conv_opts+=",fsync"
    fi

    if [[ $device_count -gt 1 ]] ; then
        1>&2 echo error - device name matches more than one device: "$SDCARD"
        exit 1
    fi


    # validate that the SDCARD argument is a sensible size (to avoid
    #    overwriting a device that we shouldn't)

    SIZE=$(lsblk --byte --raw --noheadings --output NAME,SIZE,TYPE | grep "^$SDCARD_BASE " 2>/dev/null | cut -d " " -f 2)
    logd "size of $SDCARD: $SIZE"

    if  [[ "$SIZE" -gt $SIZE_MAX ]] ; then
	    1>&2 echo "error - $SDCARD device is larger than expected" "($SIZE > $SIZE_MAX)"
	    exit 1
    fi


    # validate that there's a file to write to the image
    if [[ "$IMAGE" = "" ]] || [[ ! -f "$IMAGE" ]] ; then
	    1>&2 echo "error - \"$IMAGE\" does not exist"
	    exit 1
    fi

    # we have a valid sdcard device name and a file to write to it, so do the work

    # unmount anything on this device
    for i in {1..9} "" ; do
	    if  mount | grep -c -q "${SDCARD}${i} " ;then
	        echo unmounting "\"${SDCARD}${i}\""
	        sudo umount "${SDCARD}${i}"
	    fi
    done

    GZIPPED="$(file --brief "$IMAGE" 2>/dev/null | grep -c "^gzip compressed data")"

    echo Writing "\"$IMAGE\"" to "$SDCARD"

    if [ "$GZIPPED" = "1" ]; then
        logd "$IMAGE is gzipped"
        if is_truthy "$opt_dry_run" ; then
            log "dry run of:" \
                zcat "$IMAGE" "|" sudo dd bs=4M of="${SDCARD}" status=progress conv="${dd_conv_opts#,}"   # remove dangling comma from $dd_conv_opts
        else
                zcat "$IMAGE"  |  sudo dd bs=4M of="${SDCARD}" status=progress conv="${dd_conv_opts#,}"
        fi
    else
        logd "$IMAGE is a raw image file"
        if is_truthy "$opt_dry_run" ; then
            log "dry run of:" \
                sudo dd bs=4M if="${IMAGE}" of="${SDCARD}" status=progress conv="${dd_conv_opts#,}"
        else
                sudo dd bs=4M if="${IMAGE}" of="${SDCARD}" status=progress conv="${dd_conv_opts#,}"
        fi
    fi
}


main "$@"

