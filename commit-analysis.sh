#!/bin/bash

scriptname="${0/#$HOME/\~}"

# utility functions
# use the getopts_long() function:
. ~/devtrees/scripts/getopts_long.shlib


function die()
{
	>&2 echo "$@"
	exit 1
}


function logv()
{
	if [ "$opt_verbose" == true ]; then
		echo "$@"
	fi
}

function logd()
{
	if [ "$opt_debug" == true ]; then
		echo "$@"
	fi
}


function usage()
{
echo "Usage: $scriptname"
cat <<EOF

Read a list of git commits from stdin and list identical commits on
all branches even if they have a different commit SHA hash.

The input needs to be one commit per line with the first word
being the commit hash.  The remainder of each line is ignored 
except when displaying the input for reference. This means
that the result of "git log --oneline" can be piped into this 
script for analysis.

The following command line options are supported:

    -f, --file=FILE                read the commit list from FILE instead
                                       of stdin
    -b, --branch-pattern=PATTERN   regex used to filter branch names
    -v, --verbose                  verbose messages
        --debug                    debug messages
    -h, --help                     this help message

EOF
}


function parse_options()
{
	opt_verbose=false
	opt_debug=false
	opt_file=-
	opt_branch_pattern='*'
	OPTLIND=1
	while getopts_long :f:b:vh opt \
	file required_argument \
	branch-pattern required_argument \
	verbose no_argument \
	debug no_argument \
	help 0 "" "$@"
	do
	case "$opt" in
		v|verbose) opt_verbose=true;;
		debug) opt_debug=true;;
		h|help) usage; exit 0;;
		f|file) opt_file=$OPTLARG;;
		b|branch-pattern) opt_branch_pattern=$OPTLARG;;
		:) printf >&2 '%s: %s\n' "${0##*/}" "$OPTLERR"
		usage
		exit 1;;
	esac
	done
	shift "$(($OPTLIND - 1))"
	# process the remaining arguments
}




# the git commands used:

git_ls='git log --date=short-local --pretty=format:"%<(8)%h %Cgreen%<(12,trunc)%an  %ad  %s%Creset"'
git_showcommit='git show --name-status --abbrev-commit'
git_findall='git log --all --date=local --pretty=format:"%h%Cgreen%x09%an%x09%ad%x09%s%Creset" --grep'
git_contains='git branch --contains'


function run_prefixed()
{

	if [ $# -lt 2 ]; then return 1; fi

	local prefix="$1"
	local line
	
	# logd "$2"
	eval $2 | while IFS=$'\n' read -r line || [ -n "$line" ]
	do
		printf "%s%s\n" "${prefix}" "${line}"
	done
}



# the commits we're interested in analyzing
#
# I got these with this command: git ls precor ^cobra-6.2.4
#

# read -r -d '' commit_list VAR <<'EOF'


define(){ IFS='\n' read -r -d '' ${1} || true; }

define commit_list <<'EOF'
b369ac9  Michael Burr  2017-02-09  Revert "Fix Bug 12992 - Hardware watchdog reboots occur during workouts"
f7eb994  Michael Burr  2017-02-08  Fix Bug 12992 - Hardware watchdog reboots occur during workouts
bcac02c  Michael Burr  2017-02-03  Fix Bug 12984 - DCR: enable timestamps on the dmesg log
c07e21b  Michael Burr  2017-02-03  Fix Bug 13193 by reverting the GPUp13 Ver 5 patch
5b2588b  Jean-Bapti..  2016-01-05  Don't release page if not valid
f4ca5b9  Jean-Bapti..  2016-01-05  Fix build issue
b587683  Jean-Bapti..  2016-01-05  10939: Validate page before flushing
792a206  Jean-Bapti..  2016-01-05  GPU driver will cause kernel panic when allocate memory failed
523f2e3  Michael Burr  2017-01-17  ENGR00278452 PU: fix system hang if run Audio loop test
6bcec36  Xianzhong     2013-12-04  ENGR00290463 revert exact pages allocator to fix system reboot issue
f4d6398  Richard Liu   2014-05-12  ENGR00313001-2 disable CONFIG_ANDROID_RESERVED_MEMORY_ACCOUNT
22b41d3  Richard Liu   2014-05-12  ENGR00313001-1 add CONFIG_GPU_LOW_MEMORY_KILLER and default enable
53d76f5  Richard Liu   2015-09-15  upgrade GPU to latest p13 for JB4.2.2_1.1.0
bd62bb9  Michael Burr  2017-01-17  Prep for GPUp13_v5 - revert a75df21: Freescale patch: GPU upgrade to latest p13
c340847  Michael Burr  2017-01-17  Prep for GPUp13_v5 - revert b12cacc: GPUp13_v2 patch from Freescale.
f1c84a51 Michael Burr  2017-01-17  Prep for GPUp13_v5 - revert dad720e: ENGR00290463 revert exact pages allocator to fix system reboot issue
2d6ce91  Michael Burr  2017-01-17  Prep for GPUp13_v5 - revert 60cedcb: GPU driver will cause kernel panic when allocate memory failed
e08a972  Michael Burr  2017-01-17  Prep for GPUp13_v5 - revert 83e3eb0: Validate page before flushing
b67440e  Michael Burr  2017-01-17  Prep for GPUp13_v5 - revert 35a3bba: Fix build issue
87309a8  Michael Burr  2017-01-17  Prep for GPUp13_v5 - revert 21894a7: Don't release page if not valid
e3bc2c4  sam           2016-12-13  Make the default HDMI port number 3 which is the daughter board HDMI input on P82.
845bc18  Steven Kak..  2016-12-02  Merge branch 'precor' of http://xdc-scm/git/freescale/freescale/linux-2.6-imx into precor
cf6f8ef  sam           2016-12-01  Remove conditional configuration of GPIO pins for P62 alpha and beta boards. The GPIO pin assignment for both versions of hardware did not change.
641c5c2  Steven Kak..  2016-12-01  Merge branch 'precor' of http://xdc-scm/git/freescale/freescale/linux-2.6-imx into precor
b179d73  sam           2016-12-01  Change default GPIO settings for IR blaster (AtTiny) so that it's out of reset.
ab32196  Steven Kak..  2016-11-23  Merge branch 'searsp-aufs' into precor
cc868d6  Michael Burr  2016-11-11  Fix bug 12969 (#3):  Set 1.2GHz .cpu_voltage to 1300000
ee4afed  Michael Burr  2016-11-08  Fix bug 12969 (2nd try):  Set mx6q 1.2GHz .cpu_voltage back to 1275000
b45f402  Michael Burr  2016-11-04  Fix bug 12969: Update voltage values using Variscite's iMX6 4.1.15 device tree config
b789d42  Michael Burr  2016-11-01  Fix Bug 12968: new eMMC flash chips report a new CSD version
fea2bfd  Steven Kak..  2016-10-27  Added aufs support for P62  Reviewed_by:self
9e43a9f  Steven Kak..  2016-09-19  Merge branch 'precor' into searsp-aufs
e2ff5a4  Patrick Se..  2016-02-29  Merge branch 'precor' into aufs
32ad1bd  Patrick Se..  2016-02-17  Merge branch 'precor' into aufs
9847179  Patrick Se..  2016-02-16  Merge branch 'precor' into aufs
faf98e6  Patrick Se..  2016-02-10  Add AuFS support
0db2adf  Patrick Se..  2016-02-10  Add AuFS support
e317f06  Patrick Se..  2016-02-10  Add AuFS support
5efaa59  Patrick Se..  2016-02-10  Add AuFS support
EOF


function list_branches()
{
	local line
	local changeid=$1
	local branch_pattern="$2"
	
	if [ -z "$changeid" ]; then return 1; fi

	if [ -z "$branch_pattern" ]; then
		# nothing specified to match
		# so we'll match everything
		branch_pattern='*'
	fi

	# logd "list_branches: " ${changeid} ${branch_pattern}
	# logd ----debug----
	# if [ "$opt_debug" == "true ]; then eval ${git_findall} ${changeid} ; fi
	# logd ====debug====
	

	
	# find all commits that match the changeid
	
	# since the last line of the ${git_findall} command doesn't have a newline,
	# we need the '|| [ -n "$line" ]' at the end of the read command in order to
	# process that line.  bash can be a major pain
	#
	# the ${git_findall} command will find all commits in the repo that a given changeid is in
	eval ${git_findall} ${changeid} | while IFS=$'\n' read -r line || [ -n "$line" ]
	do
		# extract the first field from the line (delete everythign after a space or tab)
		# local commit_id="${line%%[$' \t\e']*}"
		local commit_id="${line%%[^0-9a-fA-F]*}"
		
		# ${git_contains} will list all branches the commit belongs to
		# we filter that list using the provided pattern
		#
		# the sed command adds spaces to the beginning of the lines of grep output
		echo "$line"
		eval ${git_contains} ${commit_id} | grep -E "${branch_pattern}" | sed 's/.*/    &/'
	done

	return 0
}



function main()
{
	parse_options "$@"
	
	logd "opt_file = $opt_file"
	logd "opt_branch_pattern = $opt_branch_pattern"
	logd "opt_verbose = $opt_verbose"

	# if a file is specified for the -f option, use it for stdin
	if [ "$opt_file" != "-" ] ; then 
		if [ ! -e "$opt_file" ] ; then 	die "error: File $opt_file does not exist"; fi
		exec < "$opt_file" 
	fi


	# go through the commit_list and find the Change-Id: value

	while IFS= read -r line || [ -n "$line" ]
	do
		if [ -n "$line" ]; then
			# extract the first field from the line (delete everything after a space or tab)
			local commit_id="${line%%[^0-9a-fA-F]*}"

			echo "${line}"

			# first check to see if there are any Change-Id's for this commit
			if ! ${git_showcommit} ${commit_id} | grep -c "Change-Id:" &> /dev/null ; then
				run_prefixed "    " "echo Commit ${commit_id} has no Change-Id"
				run_prefixed "    " "${git_ls} ${commit_id} -n 1"
				run_prefixed "        " "${git_contains} ${commit_id}"
			fi

			# now display branches for commits that contains any of the change ids found in the commit
			${git_showcommit} ${commit_id} | grep "Change-Id:" | while read header changeid
			do 
				# echo "    ""${changeid:0:8}"
				run_prefixed "    " "echo Change-Id: ${changeid:0:8}..."    
				
				if [ ${#changeid} ]; then
					# take care if changing the following that the quoting for the
					# opt_branch_pattern variable is correct.  It needs to have single
					# quotes *passed* to the function so the regex is always quoted.
					run_prefixed "    " "list_branches ${changeid} '""$opt_branch_pattern""'"
				fi
			done
			
			echo ""
		fi
	done
}



main "$@"
