ROOT=${ROOT:-${HOME}/devtrees/pingu}

cmd="$1"

if [ "${cmd}" = "" ] ; then 
    cmd=status
fi

for q in "LINUX/android/vendor/qcom/opensource/interfaces" \
         "LINUX/android/vendor/qcom/opensource/tools" \
         "LINUX/android/vendor/qcom/proprietary" \
         "qcs605/chipcode_proprietary"
do
    case "${cmd}" in
        status)
            git -C "${ROOT}/${q}" status --porcelain --branch -uno
            ;;
            
        restore)
            git -C "${ROOT}/${q}" restore .
            ;;

        *)
            >&2 printf "unknown command: %s\n" "${cmd}" 
            exit 1
            ;;
    esac
done
