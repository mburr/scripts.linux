#! /bin/bash

scriptname="${0/#$HOME/\~}"


function usage()
{
echo "Usage: $(basename $scriptname)" "enable|disable"
cat <<EOF

Help manage DNS configuration with the Sonicwall netExtender VPN

One of the following options muct be specified:

    enable  - add XDC DNS server to "ppp0.sonicwall" resolvconf configuration

    disable - remove the "ppp0.sonicwall" resolvconf configuration

EOF
}


main() {
    local op="$1"
    local dns_conf_name="ppp0.sonicwall"

    case "$op" in
    "enable" | "en" | "e" | "on")
        echo "enabling DNS for Sonicwall"
        echo -ne "search xdc.precor.int\nnameserver 192.168.248.51" | /sbin/resolvconf -a "$dns_conf_name"
        ;;
    "disable" | "d" | "off")
        echo "disabling DNS for Sonicwall"
        /sbin/resolvconf -d "$dns_conf_name"
        ;;
    *)
        usage
        ;;
    esac
}

main "$@"

